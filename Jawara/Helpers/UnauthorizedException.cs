﻿using System;

namespace Jawara.Helpers
{
    public class UnauthorizedException : Exception
    {
        public UnauthorizedException() : base("You are not allowed to access this function.") { }
        public UnauthorizedException(string message) : base(message) { }
    }
}
