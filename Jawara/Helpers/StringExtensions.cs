﻿namespace Jawara.Helpers
{
    public static class StringExtensions
    {
        public static string Left(this string text, int length)
        {
            if (text == null) text = "";

            if (text.Length > length)
                return text.Substring(0, length);

            return text.PadRight(length);
        }

        public static string Right(this string text, int length)
        {
            if (text == null) text = "";

            if (text.Length > length)
                return text.Substring(0, length);

            return text.PadLeft(length);
        }

        public static string NumRight(this decimal number, int length)
        {
            var text = number.ToString("#,#0.##");

            if (text == null) text = "";

            if (text.Length > length)
                return text.Substring(0, length);

            return text.PadLeft(length);
        }

        public static string Format(this decimal number)
        {
            var text = number.ToString("#,#0.##");
            return text;
        }
        public static string Format(this int number)
        {
            var text = number.ToString("#,#0.##");
            return text;
        }


        public static string Center(this string text, int length)
        {
            if (text == null) text = "";

            if (text.Length > length)
                return text.Substring(0, length);

            var remainder = length - text.Length;
            var leftLength = remainder / 2;
            var rightLength = remainder - leftLength;

            var left = Repeat(" ", leftLength);
            var right = Repeat(" ", rightLength);
            var result = left + text + right;

            return result;
        }

        public static string Repeat(this string text, int count)
        {
            var result = "";
            for (int i = 0; i < count; i++)
                result += text;

            return result;
        }

        public static string Space(int count)
        {
            return Repeat(" ", count);
        }
    }
}
