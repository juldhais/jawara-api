using Jawara.Helpers;
using Jawara.Models;
using Jawara.Services;
using Jawara.Repositories;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace Jawara
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DataContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DataContext")));

            services.AddScoped<SettlementService>();

            services.AddScoped<ConfigurationService>();

            services.AddScoped<CustomerCategoryService>();
            services.AddScoped<CustomerService>();

            services.AddScoped<SupplierCategoryService>();
            services.AddScoped<SupplierService>();

            services.AddScoped<ItemService>();
            services.AddScoped<ItemCategoryService>();
            services.AddScoped<ItemSubcategoryService>();
            services.AddScoped<ItemTagService>();
            services.AddScoped<BillOfMaterialService>();

            services.AddScoped<PriceControlCategoryService>();
            services.AddScoped<PriceControlService>();

            services.AddScoped<StockControlCategoryService>();
            services.AddScoped<StockControlService>();

            services.AddScoped<UnitService>();
            services.AddScoped<LocationService>();
            services.AddScoped<SectionService>();
            services.AddScoped<TaxService>();
            services.AddScoped<PaymentMethodService>();

            services.AddScoped<StockOrderService>();
            services.AddScoped<StockDeliveryService>();
            services.AddScoped<StockReceiptService>();
            services.AddScoped<StockAdjustmentService>();
            services.AddScoped<ManufacturingOrderService>();
            services.AddScoped<ManufacturingCompletionService>();

            services.AddScoped<PurchaseOrderService>();
            services.AddScoped<PurchaseReceiptService>();
            services.AddScoped<PurchaseReturnService>();
            services.AddScoped<PurchasePaymentService>();

            services.AddScoped<SalesOrderService>();
            services.AddScoped<SalesDeliveryService>();
            services.AddScoped<SalesReturnService>();
            services.AddScoped<SalesPaymentService>();

            services.AddScoped<UserService>();
            services.AddScoped<RoleService>();

            services.AddScoped<PurchaseOrderPrintService>();
            services.AddScoped<SalesDeliveryPrintService>();

            services.AddScoped<SalesReportService>();

            // authentication
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                    .AddJwtBearer(options =>
                    {
                        options.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidateIssuer = true,
                            ValidateAudience = true,
                            ValidateLifetime = true,
                            ValidateIssuerSigningKey = true,
                            ValidIssuer = "Jawara",
                            ValidAudience = "Jawara",
                            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["TokenKey"]))
                        };
                    });
            // end authentication

            //swagger
            services.AddSwaggerGen(config =>
            {
                config.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "Jawara API v1",
                    Version = "v1"
                });
                config.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = @"JWT Authorization header using the Bearer scheme. \r\n\r\n 
                      Enter 'Bearer' [space] and then your token in the text input below.
                      \r\n\r\nExample: 'Bearer 12345abcdef'",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });
                config.AddSecurityRequirement(new OpenApiSecurityRequirement()
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            },
                            Scheme = "Bearer",
                            Name = "Bearer",
                            In = ParameterLocation.Header,

                        },
                        new List<string>()
                    }
                });
            });
            // end swagger

            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader());
            });

            services.AddMvc(config => config.EnableEndpointRouting = false);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            using var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope();
            using var db = serviceScope.ServiceProvider.GetRequiredService<DataContext>();
            db.Database.EnsureCreated();
            if (!db.User.Any())
            {
                var location = new Location();
                location.Code = "HO";
                location.Description = "Head Office";
                location.Type = "Head Office";
                db.Create(location, null);

                var role = new Role();
                role.Code = "Administrator";
                role.IsAdministrator = true;
                db.Create(role, null);

                var user = new User();
                user.UserName = "boss";
                user.Description = "boss";
                user.Password = StringEncryptor.Encrypt("boss");
                user.RoleId = role.Id;
                user.DefaultLocationId = location.Id;
                db.Create(user, null);

                db.SaveChanges();
            }

            app.UseDeveloperExceptionPage();


            // exception middleware
            app.UseExceptionHandler(appError =>
            {
                appError.Run(async context =>
                {
                    var contextFeature = context.Features.Get<IExceptionHandlerFeature>();
                    if (contextFeature != null)
                    {
                        if (contextFeature.Error is BadRequestException)
                            context.Response.StatusCode = (int)HttpStatusCode.BadRequest;

                        else if (contextFeature.Error is NotFoundException)
                            context.Response.StatusCode = (int)HttpStatusCode.NotFound;

                        else if (contextFeature.Error is SecurityTokenException)
                            context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;

                        else if (contextFeature.Error is UnauthorizedException)
                            context.Response.StatusCode = (int)HttpStatusCode.Forbidden;

                        else context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;


                        context.Response.Headers.Add("Access-Control-Allow-Origin", "*");
                        context.Response.ContentType = "application/json";

                        var response = new
                        {
                            statusCode = context.Response.StatusCode,
                            message = contextFeature.Error.GetBaseException().Message
                        };

                        await context.Response.WriteAsync(JsonConvert.SerializeObject(response));
                    }
                });
            });

            app.UseCors("CorsPolicy");

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseAuthorization();

            // swagger
            app.UseSwagger(c =>
            {
                c.RouteTemplate = "swagger/{documentName}/swagger.json";
            });
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Jawara API");
            });

            app.UseRouting();

            app.UseMvc();
        }
    }
}
