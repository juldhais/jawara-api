﻿using System;

namespace Jawara.Models
{
    public class SettlementDetail : IModel
    {
        public Guid Id { get; set; }
        public Settlement Settlement { get; set; }
        public Guid? SettlementId { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
        public Guid? PaymentMethodId { get; set; }
        public decimal InputBalance { get; set; }
        public decimal SystemBalance { get; set; }
        public decimal Difference { get; set; }

        public bool IsDeleted { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public Guid? CreatedById { get; set; }
        public Guid? UpdatedById { get; set; }
    }
}
