﻿using System;

namespace Jawara.Models
{
    public class SalesPaymentDetail : IModel
    {
        public Guid Id { get; set; }

        public SalesPayment SalesPayment { get; set; }
        public Guid? SalesPaymentId { get; set; }

        public SalesDelivery SalesDelivery { get; set; }
        public Guid? SalesDeliveryId { get; set; }

        public decimal Balance { get; set; }
        public decimal Payment { get; set; }
        public decimal Remaining { get; set; }

        public string Remarks { get; set; }

        public bool IsDeleted { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public Guid? CreatedById { get; set; }
        public Guid? UpdatedById { get; set; }
    }
}
