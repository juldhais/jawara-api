﻿using System;

namespace Jawara.Models
{
    public class SalesPayment : IModel
    {
        public Guid Id { get; set; }

        public string DocumentNumber { get; set; }
        public DateTime DocumentDate { get; set; }
        public string ReferenceNumber { get; set; }
        public bool Direct { get; set; }

        public Location Location { get; set; }
        public Guid? LocationId { get; set; }

        public Customer Customer { get; set; }
        public Guid? CustomerId { get; set; }

        public decimal TotalPayment { get; set; }
        public decimal TotalReturn { get; set; }
        public decimal AmountPaid { get; set; }

        public PaymentMethod PaymentMethod { get; set; }
        public Guid? PaymentMethodId { get; set; }

        public string Remarks { get; set; }

        public bool IsDeleted { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public Guid? CreatedById { get; set; }
        public Guid? UpdatedById { get; set; }
    }
}
