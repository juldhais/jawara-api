﻿using System;

namespace Jawara.Models
{
    public class User : IModel
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Description { get; set; }

        public Role Role { get; set; }
        public Guid? RoleId { get; set; }

        public Location DefaultLocation { get; set; }
        public Guid? DefaultLocationId { get; set; }

        public bool IsDeleted { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public Guid? CreatedById { get; set; }
        public Guid? UpdatedById { get; set; }
    }
}
