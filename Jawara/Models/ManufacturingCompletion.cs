﻿using System;

namespace Jawara.Models
{
    public class ManufacturingCompletion : IModel
    {
        public Guid Id { get; set; }
        public string DocumentNumber { get; set; }
        public DateTime DocumentDate { get; set; }

        public ManufacturingOrder ManufacturingOrder { get; set; }
        public Guid? ManufacturingOrderId { get; set; }

        public Location Location { get; set; }
        public Guid? LocationId { get; set; }

        public Section Section { get; set; }
        public Guid? SectionId { get; set; }

        public BillOfMaterial BillOfMaterial { get; set; }
        public Guid? BillOfMaterialId { get; set; }

        public Item Parent { get; set; }
        public Guid? ParentId { get; set; }
        public decimal Quantity { get; set; }
        public string Unit { get; set; }
        public decimal Ratio { get; set; }
        public decimal Cost { get; set; }
        public decimal TotalCost { get; set; }
        public string Remarks { get; set; }

        public bool IsDeleted { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public Guid? CreatedById { get; set; }
        public Guid? UpdatedById { get; set; }
    }
}
