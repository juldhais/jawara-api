﻿using System;

namespace Jawara.Models
{
    public class StockReceiptDetail : IModel
    {
        public Guid Id { get; set; }

        public StockReceipt StockReceipt { get; set; }
        public Guid? StockReceiptId { get; set; }

        public int Sequence { get; set; }

        public Item Item { get; set; }
        public Guid? ItemId { get; set; }

        public string Unit { get; set; }
        public decimal Ratio { get; set; }

        public decimal Quantity { get; set; }
        public decimal Cost { get; set; }
        public decimal Subtotal { get; set; }
        public string Remarks { get; set; }

        public bool IsDeleted { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public Guid? CreatedById { get; set; }
        public Guid? UpdatedById { get; set; }
    }
}
