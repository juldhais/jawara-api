﻿using System;

namespace Jawara.Models
{
    public class SalesDelivery : IModel
    {
        public Guid Id { get; set; }

        public string DocumentNumber { get; set; }
        public DateTime DocumentDate { get; set; }
        public string ReferenceNumber { get; set; }
        public DateTime? DueDate { get; set; }
        public string Status { get; set; }

        public SalesOrder SalesOrder { get; set; }
        public Guid? SalesOrderId { get; set; }

        public Location Location { get; set; }
        public Guid? LocationId { get; set; }

        public Customer Customer { get; set; }
        public Guid? CustomerId { get; set; }

        public decimal TotalBeforeTax { get; set; }
        public decimal TotalValueAddedTax { get; set; }
        public decimal TotalIncomeTax { get; set; }
        public decimal TotalOtherTax { get; set; }
        public decimal TotalAfterTax { get; set; }

        public decimal TotalPayment { get; set; }
        public decimal Balance { get; set; }

        public string Remarks { get; set; }

        public bool IsDeleted { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public Guid? CreatedById { get; set; }
        public Guid? UpdatedById { get; set; }
    }
}
