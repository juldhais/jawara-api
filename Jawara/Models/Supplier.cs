﻿using System;

namespace Jawara.Models
{
    public class Supplier : IModel
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }

        public SupplierCategory SupplierCategory { get; set; }
        public Guid? SupplierCategoryId { get; set; }

        public string Address1 { get; set; }
        public string Phone1 { get; set; }
        public string Email1 { get; set; }
        public string Website1 { get; set; }
        public string Contact1 { get; set; }

        public string Address2 { get; set; }
        public string Phone2 { get; set; }
        public string Email2 { get; set; }
        public string Website2 { get; set; }
        public string Contact2 { get; set; }

        public string TaxNumber { get; set; }

        public string Image { get; set; }
        public string Remarks { get; set; }

        public bool IsDeleted { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public Guid? CreatedById { get; set; }
        public Guid? UpdatedById { get; set; }
    }
}
