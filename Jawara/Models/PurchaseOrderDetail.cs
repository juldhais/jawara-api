﻿using System;

namespace Jawara.Models
{
    public class PurchaseOrderDetail : IModel
    {
        public Guid Id { get; set; }

        public PurchaseOrder PurchaseOrder { get; set; }
        public Guid? PurchaseOrderId { get; set; }

        public int Sequence { get; set; }

        public Item Item { get; set; }
        public Guid? ItemId { get; set; }

        public string Unit { get; set; }
        public decimal Ratio { get; set; }

        public decimal Quantity { get; set; }
        public decimal QuantityReceived { get; set; }
        public decimal Price { get; set; }

        public decimal DiscountPercent1 { get; set; }
        public decimal DiscountPercent2 { get; set; }
        public decimal DiscountAmount { get; set; }
        public decimal SubtotalBeforeTax { get; set; }

        public Tax ValueAddedTax { get; set; }
        public Guid? ValueAddedTaxId { get; set; }
        public decimal ValueAddedTaxAmount { get; set; }

        public Tax IncomeTax { get; set; }
        public Guid? IncomeTaxId { get; set; }
        public decimal IncomeTaxAmount { get; set; }

        public Tax OtherTax { get; set; }
        public Guid? OtherTaxId { get; set; }
        public decimal OtherTaxAmount { get; set; }

        public decimal SubtotalAfterTax { get; set; }

        public string Remarks { get; set; }

        public bool IsDeleted { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public Guid? CreatedById { get; set; }
        public Guid? UpdatedById { get; set; }
    }
}
