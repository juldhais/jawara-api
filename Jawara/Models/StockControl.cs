﻿using System;

namespace Jawara.Models
{
    public class StockControl : IModel
    {
        public Guid Id { get; set; }
        public StockControlCategory StockControlCategory { get; set; }
        public Guid? StockControlCategoryId { get; set; }

        public Item Item { get; set; }
        public Guid? ItemId { get; set; }

        public decimal MinimumStock { get; set; }
        public decimal MaximumStock { get; set; }

        public bool IsDeleted { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public Guid? CreatedById { get; set; }
        public Guid? UpdatedById { get; set; }
    }
}
