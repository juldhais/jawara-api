﻿using System;

namespace Jawara.Models
{
    public class PurchasePaymentDetail : IModel
    {
        public Guid Id { get; set; }

        public PurchasePayment PurchasePayment { get; set; }
        public Guid? PurchasePaymentId { get; set; }

        public PurchaseReceipt PurchaseReceipt { get; set; }
        public Guid? PurchaseReceiptId { get; set; }

        public decimal Balance { get; set; }
        public decimal Payment { get; set; }
        public decimal Remaining { get; set; }

        public string Remarks { get; set; }

        public bool IsDeleted { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public Guid? CreatedById { get; set; }
        public Guid? UpdatedById { get; set; }
    }
}
