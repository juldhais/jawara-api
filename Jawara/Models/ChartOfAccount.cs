﻿using System;

namespace Jawara.Models
{
    public class ChartOfAccount : IModel
    {
        public Guid Id { get; set; }
        public Guid? ParentId { get; set; }
        public int Level { get; set; }
        public bool Postable { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Remarks { get; set; }

        public bool IsDeleted { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public Guid? CreatedById { get; set; }
        public Guid? UpdatedById { get; set; }
    }
}
