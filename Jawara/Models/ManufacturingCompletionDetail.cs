﻿using System;

namespace Jawara.Models
{
    public class ManufacturingCompletionDetail : IModel
    {
        public Guid Id { get; set; }
        public ManufacturingCompletion ManufacturingCompletion { get; set; }
        public Guid? ManufacturingCompletionId { get; set; }
        public int Sequence { get; set; }
        public Item Component { get; set; }
        public Guid? ComponentId { get; set; }
        public decimal Quantity { get; set; }
        public string Unit { get; set; }
        public decimal Ratio { get; set; }
        public decimal Cost { get; set; }
        public decimal SubtotalCost { get; set; }
        public string Remarks { get; set; }

        public bool IsDeleted { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public Guid? CreatedById { get; set; }
        public Guid? UpdatedById { get; set; }
    }
}
