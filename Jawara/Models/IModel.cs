﻿using System;

namespace Jawara.Models
{
    public interface IModel
    {
        Guid Id { get; set; }

        bool IsDeleted { get; set; }
        DateTime? DateCreated { get; set; }
        DateTime? DateUpdated { get; set; }
        Guid? CreatedById { get; set; }
        Guid? UpdatedById { get; set; }
    }
}
