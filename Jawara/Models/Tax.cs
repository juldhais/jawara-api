﻿using System;

namespace Jawara.Models
{
    public class Tax : IModel
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }

        public string Type { get; set; } //Value Addded Tax, Income Tax, Other Tax

        public decimal Rate { get; set; }
        public decimal Amount { get; set; }

        public string Remarks { get; set; }

        public bool IsDeleted { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public Guid? CreatedById { get; set; }
        public Guid? UpdatedById { get; set; }
    }
}
