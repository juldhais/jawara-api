﻿using System;

namespace Jawara.Models
{
    public class RolePrivilege : IModel
    {
        public Guid Id { get; set; }
        public Role Role { get; set; }
        public Guid? RoleId { get; set; }

        public string Privilege { get; set; }

        public bool IsDeleted { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public Guid? CreatedById { get; set; }
        public Guid? UpdatedById { get; set; }
    }
}
