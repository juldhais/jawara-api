﻿using System;

namespace Jawara.Models
{
    public class Location : IModel
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string Type { get; set; } // Head Office, Warehouse, Store

        public PriceControlCategory PriceControlCategory { get; set; }
        public Guid? PriceControlCategoryId { get; set; }

        public StockControlCategory StockControlCategory { get; set; }
        public Guid? StockControlCategoryId { get; set; }

        public string Address { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Contact { get; set; }

        public string Remarks { get; set; }

        public bool IsDeleted { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public Guid? CreatedById { get; set; }
        public Guid? UpdatedById { get; set; }
    }
}
