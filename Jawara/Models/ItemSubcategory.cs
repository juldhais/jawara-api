﻿using System;

namespace Jawara.Models
{
    public class ItemSubcategory : IModel
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }

        public bool IsDeleted { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public Guid? CreatedById { get; set; }
        public Guid? UpdatedById { get; set; }
    }
}
