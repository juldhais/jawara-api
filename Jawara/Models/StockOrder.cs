﻿using System;

namespace Jawara.Models
{
    public class StockOrder : IModel
    {
        public Guid Id { get; set; }

        public string DocumentNumber { get; set; }
        public DateTime DocumentDate { get; set; }

        public string Status { get; set; } // Open, Closed

        public Location DestinationLocation { get; set; }
        public Guid? DestinationLocationId { get; set; }

        public Location SourceLocation { get; set; }
        public Guid? SourceLocationId { get; set; }

        public string Remarks { get; set; }

        public bool IsDeleted { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public Guid? CreatedById { get; set; }
        public Guid? UpdatedById { get; set; }
    }
}
