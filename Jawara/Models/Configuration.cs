﻿using System;

namespace Jawara.Models
{
    public class Configuration : IModel
    {
        public Guid Id { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }

        public bool IsDeleted { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public Guid? CreatedById { get; set; }
        public Guid? UpdatedById { get; set; }
    }
}
