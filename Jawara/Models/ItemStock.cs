﻿using System;

namespace Jawara.Models
{
    public class ItemStock : IModel
    {
        public Guid Id { get; set; }
        public Item Item { get; set; }
        public Guid? ItemId { get; set; }

        public Location Location { get; set; }
        public Guid? LocationId { get; set; }
        public DateTime Date { get; set; }

        public decimal Stock { get; set; }
        public decimal Cost { get; set; }

        public bool IsDeleted { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public Guid? CreatedById { get; set; }
        public Guid? UpdatedById { get; set; }
    }
}
