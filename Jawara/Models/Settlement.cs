﻿using System;

namespace Jawara.Models
{
    public class Settlement : IModel
    {
        public Guid Id { get; set; }
        public string DocumentNumber { get; set; }
        public DateTime DocumentDate { get; set; }
        public Location Location { get; set; }
        public Guid? LocationId { get; set; }
        public User User { get; set; }
        public Guid? UserId { get; set; }

        public bool IsDeleted { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public Guid? CreatedById { get; set; }
        public Guid? UpdatedById { get; set; }
    }
}
