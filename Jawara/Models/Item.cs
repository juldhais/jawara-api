﻿using System;

namespace Jawara.Models
{
    public class Item : IModel
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string Barcode { get; set; }
        public string Unit { get; set; }
        public decimal PurchasePrice { get; set; }
        public decimal SalesPrice { get; set; }

        public bool Purchasable { get; set; }
        public bool Stockable { get; set; }
        public bool Manufacturable { get; set; }
        public bool Sellable { get; set; }

        public ItemCategory ItemCategory { get; set; }
        public Guid? ItemCategoryId { get; set; }

        public ItemSubcategory ItemSubcategory { get; set; }
        public Guid? ItemSubcategoryId { get; set; }


        public string Tag { get; set; }

        public Supplier Supplier { get; set; }
        public Guid? SupplierId { get; set; }
        public string SupplierItemCode { get; set; }

        public string Image { get; set; }
        public string Remarks { get; set; }

        public bool IsDeleted { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public Guid? CreatedById { get; set; }
        public Guid? UpdatedById { get; set; }
    }
}
