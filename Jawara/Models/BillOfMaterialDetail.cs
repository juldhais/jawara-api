﻿using System;

namespace Jawara.Models
{
    public class BillOfMaterialDetail : IModel
    {
        public Guid Id { get; set; }

        public BillOfMaterial BillOfMaterial { get; set; }
        public Guid? BillOfMaterialId { get; set; }

        public int Sequence { get; set; }

        public Item Component { get; set; }
        public Guid? ComponentId { get; set; }

        public decimal ComponentQuantity { get; set; }
        public string ComponentUnit { get; set; }
        public decimal ComponentRatio { get; set; }

        public bool IsDeleted { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public Guid? CreatedById { get; set; }
        public Guid? UpdatedById { get; set; }
    }
}
