﻿using System;

namespace Jawara.Models
{
    public class PriceControl : IModel
    {
        public Guid Id { get; set; }

        public PriceControlCategory PriceControlCategory { get; set; }
        public Guid? PriceControlCategoryId { get; set; }

        public Item Item { get; set; }
        public Guid? ItemId { get; set; }

        public string Unit { get; set; }
        public decimal Ratio { get; set; }
        public decimal Price { get; set; }
        public decimal MinimumQuantity { get; set; }

        public bool IsDeleted { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public Guid? CreatedById { get; set; }
        public Guid? UpdatedById { get; set; }
    }
}
