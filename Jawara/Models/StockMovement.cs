﻿using System;

namespace Jawara.Models
{
    public class StockMovement : IModel
    {
        public Guid Id { get; set; }

        public Guid? TransactionId { get; set; }

        public string DocumentNumber { get; set; }
        public DateTime DocumentDate { get; set; }
        public string TransactionType { get; set; }

        public ItemStock ItemStok { get; set; }
        public Guid? ItemStockId { get; set; }

        public decimal Quantity { get; set; }

        public bool IsDeleted { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public Guid? CreatedById { get; set; }
        public Guid? UpdatedById { get; set; }
    }
}
