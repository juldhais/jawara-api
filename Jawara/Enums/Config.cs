﻿namespace Jawara.Enums
{
    public class Config
    {
        private Config() { }

        public const string CompanyName = "CompanyName";
        public const string PrintHeader = "PrintHeader";
        public const string PrintSubheader = "PrintSubheader";
        public const string PrintFooter = "PrintFooter";
        public const string AutoCutter = "AutoCutter";
        public const string PrintType = "PrintType";

        public const string UseDiscount = "UseDiscount";
        public const string UseTax = "UseTax";

    }
}
