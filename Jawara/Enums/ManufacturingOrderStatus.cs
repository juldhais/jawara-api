﻿namespace Jawara.Enums
{
    public class ManufacturingOrderStatus
    {
        private ManufacturingOrderStatus() { }

        public const string Open = "Open";
        public const string Closed = "Closed";
    }
}
