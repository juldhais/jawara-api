﻿namespace Jawara.Enums
{
    public class PaymentStatus
    {
        private PaymentStatus() { }

        public const string Unpaid = "Unpaid";
        public const string Paid = "Paid";
    }
}
