﻿namespace Jawara.Enums
{
    public class PurchaseOrderStatus
    {
        private PurchaseOrderStatus() { }

        public const string Open = "Open";
        public const string Closed = "Closed";
    }
}
