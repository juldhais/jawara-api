﻿namespace Jawara.Enums
{
    public class StockDeliveryStatus
    {
        private StockDeliveryStatus() { }

        public const string Open = "Open";
        public const string Closed = "Closed";
    }
}
