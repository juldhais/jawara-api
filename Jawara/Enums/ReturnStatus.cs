﻿namespace Jawara.Enums
{
    public class ReturnStatus
    {
        private ReturnStatus() { }

        public const string Unused = "Unused";
        public const string Used = "Used";
    }
}
