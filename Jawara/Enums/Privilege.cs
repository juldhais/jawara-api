﻿namespace Jawara.Enums
{
    public class Privilege
    {
        private Privilege() { }

        public const string Item = "Item";
        public const string ItemCreate = "Item - Create";
        public const string ItemUpdate = "Item - Update";
        public const string ItemDelete = "Item - Delete";

        public const string ItemCategory = "Item Category";
        public const string ItemCategoryCreate = "Item Category - Create";
        public const string ItemCategoryUpdate = "Item Category - Update";
        public const string ItemCategoryDelete = "Item Category - Delete";

        public const string ItemSubcategory = "Item Subcategory";
        public const string ItemSubcategoryCreate = "Item Subcategory - Create";
        public const string ItemSubcategoryUpdate = "Item Subcategory - Update";
        public const string ItemSubcategoryDelete = "Item Subcategory - Delete";

        public const string ItemTag = "Item Tag";
        public const string ItemTagCreate = "Item Tag - Create";
        public const string ItemTagUpdate = "Item Tag - Update";
        public const string ItemTagDelete = "Item Tag - Delete";

        public const string PriceControl = "Item Sales Price";
        public const string PriceControlCreate = "Item Sales Price - Create";
        public const string PriceControlUpdate = "Item Sales Price - Update";
        public const string PriceControlDelete = "Item Sales Price - Delete";

        public const string StockControl = "Stock Control";
        public const string StockControlCreate = "Stock Control - Create";
        public const string StockControlUpdate = "Stock Control - Update";
        public const string StockControlDelete = "Stock Control - Delete";

        public const string BillOfMaterial = "Bill of Material";
        public const string BillOfMaterialCreate = "Bill of Material - Create";
        public const string BillOfMaterialUpdate = "Bill of Material - Update";
        public const string BillOfMaterialDelete = "Bill of Material - Delete";

        public const string Unit = "Unit";
        public const string UnitCreate = "Unit - Create";
        public const string UnitUpdate = "Unit - Update";
        public const string UnitDelete = "Unit - Delete";

        public const string Location = "Location";
        public const string LocationCreate = "Location - Create";
        public const string LocationUpdate = "Location - Update";
        public const string LocationDelete = "Location - Delete";

        public const string Section = "Section";
        public const string SectionCreate = "Section - Create";
        public const string SectionUpdate = "Section - Update";
        public const string SectionDelete = "Section - Delete";

        public const string Tax = "Tax";
        public const string TaxCreate = "Tax - Create";
        public const string TaxUpdate = "Tax - Update";
        public const string TaxDelete = "Tax - Delete";

        public const string PriceControlCategory = "Sales Price Category";
        public const string PriceControlCategoryCreate = "Sales Price Category - Create";
        public const string PriceControlCategoryUpdate = "Sales Price Category - Update";
        public const string PriceControlCategoryDelete = "Sales Price Category - Delete";

        public const string StockControlCategory = "Stock Control Category";
        public const string StockControlCategoryCreate = "Stock Control Category - Create";
        public const string StockControlCategoryUpdate = "Stock Control Category - Update";
        public const string StockControlCategoryDelete = "Stock Control Category - Delete";

        public const string Customer = "Customer";
        public const string CustomerCreate = "Customer - Create";
        public const string CustomerUpdate = "Customer - Update";
        public const string CustomerDelete = "Customer - Delete";

        public const string CustomerCategory = "Customer Category";
        public const string CustomerCategoryCreate = "Customer Category - Create";
        public const string CustomerCategoryUpdate = "Customer Category - Update";
        public const string CustomerCategoryDelete = "Customer Category - Delete";

        public const string Supplier = "Supplier";
        public const string SupplierCreate = "Supplier - Create";
        public const string SupplierUpdate = "Supplier - Update";
        public const string SupplierDelete = "Supplier - Delete";

        public const string SupplierCategory = "Supplier Category";
        public const string SupplierCategoryCreate = "Supplier Category - Create";
        public const string SupplierCategoryUpdate = "Supplier Category - Update";
        public const string SupplierCategoryDelete = "Supplier Category - Delete";

        public const string PaymentMethod = "Payment Method";
        public const string PaymentMethodCreate = "Payment Method - Create";
        public const string PaymentMethodUpdate = "Payment Method - Update";
        public const string PaymentMethodDelete = "Payment Method - Delete";

        public const string Role = "Role";
        public const string RoleCreate = "Role - Create";
        public const string RoleUpdate = "Role - Update";
        public const string RoleDelete = "Role - Delete";

        public const string User = "User";
        public const string UserCreate = "User - Create";
        public const string UserUpdate = "User - Update";
        public const string UserDelete = "User - Delete";

        public const string Configuration = "Configuration";

        public const string PurchaseOrder = "Purchase Order";
        public const string PurchaseOrderCreate = "Purchase Order - Create";
        public const string PurchaseOrderUpdate = "Purchase Order - Update";
        public const string PurchaseOrderDelete = "Purchase Order - Delete";
        public const string PurchaseOrderOpen = "Purchase Order - Open";
        public const string PurchaseOrderClose = "Purchase Order - Close";

        public const string PurchaseReceipt = "Purchase Receipt";
        public const string PurchaseReceiptCreate = "Purchase Receipt - Create";
        public const string PurchaseReceiptUpdate = "Purchase Receipt - Update";
        public const string PurchaseReceiptDelete = "Purchase Receipt - Delete";

        public const string PurchaseReturn = "Purchase Return";
        public const string PurchaseReturnCreate = "Purchase Return - Create";
        public const string PurchaseReturnUpdate = "Purchase Return - Update";
        public const string PurchaseReturnDelete = "Purchase Return - Delete";

        public const string PurchasePayment = "Purchase Payment";
        public const string PurchasePaymentCreate = "Purchase Payment - Create";
        public const string PurchasePaymentUpdate = "Purchase Payment - Update";
        public const string PurchasePaymentDelete = "Purchase Payment - Delete";

        public const string SalesOrder = "Sales Order";
        public const string SalesOrderCreate = "Sales Order - Create";
        public const string SalesOrderUpdate = "Sales Order - Update";
        public const string SalesOrderDelete = "Sales Order - Delete";
        public const string SalesOrderOpen = "Sales Order - Open";
        public const string SalesOrderClose = "Sales Order - Close";

        public const string SalesDelivery = "Sales Delivery";
        public const string SalesDeliveryCreate = "Sales Delivery - Create";
        public const string SalesDeliveryUpdate = "Sales Delivery - Update";
        public const string SalesDeliveryDelete = "Sales Delivery - Delete";

        public const string SalesReturn = "Sales Return";
        public const string SalesReturnCreate = "Sales Return - Create";
        public const string SalesReturnUpdate = "Sales Return - Update";
        public const string SalesReturnDelete = "Sales Return - Delete";

        public const string SalesPayment = "Sales Payment";
        public const string SalesPaymentCreate = "Sales Payment - Create";
        public const string SalesPaymentUpdate = "Sales Payment - Update";
        public const string SalesPaymentDelete = "Sales Payment - Delete";

        public const string StockList = "Stock List";
        public const string LowStockList = "Low Stock List";

        public const string ManufacturingOrder = "Manufacturing Order";
        public const string ManufacturingOrderCreate = "Manufacturing Order - Create";
        public const string ManufacturingOrderUpdate = "Manufacturing Order - Update";
        public const string ManufacturingOrderDelete = "Manufacturing Order - Delete";
        public const string ManufacturingOrderOpen = "Manufacturing Order - Open";
        public const string ManufacturingOrderClose = "Manufacturing Order - Close";

        public const string ManufacturingCompletion = "Manufacturing Completion";
        public const string ManufacturingCompletionCreate = "Manufacturing Completion - Create";
        public const string ManufacturingCompletionUpdate = "Manufacturing Completion - Update";
        public const string ManufacturingCompletionDelete = "Manufacturing Completion - Delete";

        public const string StockOrder = "Stock Order";
        public const string StockOrderCreate = "Stock Order - Create";
        public const string StockOrderUpdate = "Stock Order - Update";
        public const string StockOrderDelete = "Stock Order - Delete";
        public const string StockOrderOpen = "Stock Order - Open";
        public const string StockOrderClose = "Stock Order - Close";

        public const string StockDelivery = "Stock Delivery";
        public const string StockDeliveryCreate = "Stock Delivery - Create";
        public const string StockDeliveryUpdate = "Stock Delivery - Update";
        public const string StockDeliveryDelete = "Stock Delivery - Delete";
        public const string StockDeliveryOpen = "Stock Delivery - Open";
        public const string StockDeliveryClose = "Stock Delivery - Close";

        public const string StockReceipt = "Stock Receipt";
        public const string StockReceiptCreate = "Stock Receipt - Create";
        public const string StockReceiptUpdate = "Stock Receipt - Update";
        public const string StockReceiptDelete = "Stock Receipt - Delete";

        public const string StockAdjustment = "Stock Adjustment";
        public const string StockAdjustmentCreate = "Stock Adjustment - Create";
        public const string StockAdjustmentUpdate = "Stock Adjustment - Update";
        public const string StockAdjustmentDelete = "Stock Adjustment - Delete";

        public const string Settlement = "Settlement";
        public const string SettlementCreate = "Settlement - Create";
        public const string SettlementUpdate = "Settlement - Update";
        public const string SettlementDelete = "Settlement - Delete";

        public const string ChangeTransactionDate = "Change Transaction Date";
        public const string ChangeTransactionLocation = "Change Transaction Location";
        public const string ChangeSalesPrice = "Change Sales Price";
        public const string ChangePurchasePrice = "Change Purchase Price";

        public const string SalesReport = "Sales Report";
        public const string SalesProfitReport = "Sales Profit Report";
        public const string PurchaseReport = "Purchase Report";
        public const string StockReport = "Stock Report";
    }
}
