﻿namespace Jawara.Enums
{
    public class TransactionType
    {
        private TransactionType() { }

        public const string ManufacturingOrder = "Manufacturing Order";
        public const string ManufacturingCompletion = "Manufacturing Completion";

        public const string PurchaseOrder = "Purchase Order";
        public const string PurchaseReceipt = "Purchase Receipt";
        public const string PurchaseReturn = "Purchase Return";
        public const string PurchasePayment = "Purchase Payment";

        public const string SalesOrder = "Sales Order";
        public const string SalesDelivery = "Sales Delivery";
        public const string SalesReturn = "Sales Return";
        public const string SalesPayment = "Sales Payment";

        public const string StockOrder = "Stock Order";
        public const string StockDelivery = "Stock Delivery";
        public const string StockReceipt = "Stock Receipt";

        public const string StockAdjustment = "Stock Adjustment";

    }
}
