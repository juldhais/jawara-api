﻿namespace Jawara.Enums
{
    public class StockOrderStatus
    {
        private StockOrderStatus() { }

        public const string Open = "Open";
        public const string Closed = "Closed";
    }
}
