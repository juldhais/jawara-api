﻿namespace Jawara.Enums
{
    public class SalesOrderStatus
    {
        private SalesOrderStatus() { }

        public const string Open = "Open";
        public const string Closed = "Closed";
    }
}
