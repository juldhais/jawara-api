﻿namespace Jawara.Enums
{
    public class PrintType
    {
        private PrintType() { }

        public const string Receipt30 = "Receipt30";
        public const string Receipt40 = "Receipt40";
        public const string Receipt50 = "Receipt50";
        public const string Report = "Report";
    }
}
