﻿using System;

namespace Jawara.Resources
{
    public class TaxResource
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }

        public string Type { get; set; } //Value Addded Tax, Income Tax, Other Tax

        public decimal Rate { get; set; }
        public decimal Amount { get; set; }

        public string Remarks { get; set; }
    }
}
