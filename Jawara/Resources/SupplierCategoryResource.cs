﻿using System;

namespace Jawara.Resources
{
    public class SupplierCategoryResource
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }

        public SupplierCategoryResource()
        {
            Id = Guid.Empty;
        }
    }
}
