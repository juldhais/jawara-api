﻿using System;

namespace Jawara.Resources
{
    public class SalesPriceResource
    {
        public Guid? ItemId { get; set; }
        public Guid? LocationId { get; set; }
        public Guid? CustomerId { get; set; }
        public string Unit { get; set; }
        public decimal Ratio { get; set; }
        public decimal Quantity { get; set; }
    }
}
