﻿using System;

namespace Jawara.Resources
{
    public class DirectPaymentResource
    {
        public Guid? PaymentId { get; set; }
        public string PaymentMethodCode { get; set; }
        public Guid? PaymentMethodId { get; set; }
        public decimal Amount { get; set; }
        public string Remarks { get; set; }
    }
}
