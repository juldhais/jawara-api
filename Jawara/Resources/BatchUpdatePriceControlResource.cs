﻿using System;

namespace Jawara.Resources
{
    public class BatchUpdatePriceControlResource
    {
        public Guid? PriceControlCategoryId { get; set; }
        public Guid? ItemCategoryId { get; set; }
        public Guid? ItemSubcategoryId { get; set; }
        public string ItemTag { get; set; }
        public string Unit { get; set; }
        public decimal Ratio { get; set; }
        public decimal MinimumQuantity { get; set; }
        public decimal Price { get; set; }
    }
}
