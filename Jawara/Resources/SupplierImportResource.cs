﻿namespace Jawara.Resources
{
    public class SupplierImportResource
    {
        public string Code { get; set; }
        public string Description { get; set; }

        public string Category { get; set; }

        public string Address1 { get; set; }
        public string Phone1 { get; set; }
        public string Email1 { get; set; }
        public string Website1 { get; set; }
        public string Contact1 { get; set; }

        public string Address2 { get; set; }
        public string Phone2 { get; set; }
        public string Email2 { get; set; }
        public string Website2 { get; set; }
        public string Contact2 { get; set; }

        public string Remarks { get; set; }
    }
}
