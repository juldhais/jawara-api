﻿using System;

namespace Jawara.Resources
{
    public class ItemStockResource
    {
        public Guid? ItemId { get; set; }
        public Guid? LocationId { get; set; }
        public DateTime Date { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public string Subcategory { get; set; }
        public string Tag { get; set; }
        public string Location { get; set; }
        public string Unit { get; set; }
        public decimal Stock { get; set; }
        public string StockDetail { get; set; }
        public decimal Cost { get; set; }
        public decimal Subtotal { get; set; }
    }
}
