﻿using Jawara.Helpers;
using System;

namespace Jawara.Resources
{
    public class PurchasePaymentReturnResource
    {
        public Guid Id { get; set; }

        public bool Check { get; set; }

        public string DocumentNumber { get; set; }

        public DateTime DocumentDate { get; set; }

        [Mapping("Location.Code")]
        public string LocationCode { get; set; }

        public decimal TotalAfterTax { get; set; }

        public PurchasePaymentReturnResource()
        {
            Id = Guid.Empty;
        }
    }
}
