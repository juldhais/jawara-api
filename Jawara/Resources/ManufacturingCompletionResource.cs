﻿using Jawara.Helpers;
using System;
using System.Collections.Generic;

namespace Jawara.Resources
{
    public class ManufacturingCompletionResource
    {
        public Guid Id { get; set; }
        public string DocumentNumber { get; set; }
        public DateTime DocumentDate { get; set; }

        public Guid? ManufacturingOrderId { get; set; }

        [Mapping("Location.Code")]
        public string LocationCode { get; set; }
        public Guid? LocationId { get; set; }

        [Mapping("BillOfMaterial.Code")]
        public string BillOfMaterialCode { get; set; }
        public Guid? BillOfMaterialId { get; set; }

        [Mapping("Parent.Code")]
        public string ParentCode { get; set; }

        [Mapping("Parent.Description")]
        public string ParentDescription { get; set; }
        public Guid? ParentId { get; set; }
        public decimal Quantity { get; set; }
        public string Unit { get; set; }
        public decimal Ratio { get; set; }
        public decimal Cost { get; set; }
        public decimal TotalCost { get; set; }
        public string Remarks { get; set; }


        [NotMappedToModel]
        public DateTime? DateCreated { get; set; }

        [NotMappedToModel]
        public DateTime? DateUpdated { get; set; }

        [NotMappedToModel]
        public string CreatedBy { get; set; }

        [NotMappedToModel]
        public string UpdatedBy { get; set; }

        [NotMappedToModel]
        public Guid? CreatedById { get; set; }

        [NotMappedToModel]
        public Guid? UpdatedById { get; set; }

        public List<ManufacturingCompletionDetailResource> Details { get; set; }

        public ManufacturingCompletionResource()
        {
            Details = new List<ManufacturingCompletionDetailResource>();
        }
    }
}
