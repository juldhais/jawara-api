﻿using System;

namespace Jawara.Resources
{
    public class StockControlCategoryResource
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }

        public StockControlCategoryResource()
        {
            Id = Guid.Empty;
        }
    }
}
