﻿using Jawara.Helpers;
using System;

namespace Jawara.Resources
{
    public class BillOfMaterialDetailResource
    {
        public Guid Id { get; set; }

        public Guid? BillOfMaterialId { get; set; }

        public int Sequence { get; set; }

        [Mapping("Component.Code")]
        public string ComponentCode { get; set; }

        [Mapping("Component.Description")]
        public string ComponentDescription { get; set; }

        public Guid? ComponentId { get; set; }
        public decimal ComponentQuantity { get; set; }
        public string ComponentUnit { get; set; }
        public decimal ComponentRatio { get; set; }
    }
}
