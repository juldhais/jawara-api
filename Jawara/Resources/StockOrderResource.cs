﻿using Jawara.Helpers;
using System;
using System.Collections.Generic;

namespace Jawara.Resources
{
    public class StockOrderResource
    {
        public Guid Id { get; set; }

        public string DocumentNumber { get; set; }
        public DateTime DocumentDate { get; set; }

        public string Status { get; set; } // Open, Sent, Received // Closed, Canceled

        [Mapping("DestinationLocation.Code")]
        public string DestinationLocationCode { get; set; }
        public Guid? DestinationLocationId { get; set; }

        [Mapping("SourceLocation.Code")]
        public string SourceLocationCode { get; set; }
        public Guid? SourceLocationId { get; set; }

        public string Remarks { get; set; }

        [NotMappedToModel]
        public DateTime? DateCreated { get; set; }

        [NotMappedToModel]
        public DateTime? DateUpdated { get; set; }

        [NotMappedToModel]
        public string CreatedBy { get; set; }

        [NotMappedToModel]
        public string UpdatedBy { get; set; }

        [NotMappedToModel]
        public Guid? CreatedById { get; set; }

        [NotMappedToModel]
        public Guid? UpdatedById { get; set; }

        public List<StockOrderDetailResource> Details { get; set; }

        public StockOrderResource()
        {
            Id = Guid.Empty;
            Details = new List<StockOrderDetailResource>();
        }
    }
}
