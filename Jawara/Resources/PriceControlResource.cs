﻿using Jawara.Helpers;
using System;

namespace Jawara.Resources
{
    public class PriceControlResource
    {
        public Guid Id { get; set; }

        [Mapping("PriceControlCategory.Code")]
        public string PriceControlCategoryCode { get; set; }
        public Guid? PriceControlCategoryId { get; set; }

        [Mapping("Item.Code")]
        public string ItemCode { get; set; }

        [Mapping("Item.Description")]
        public string ItemDescription { get; set; }

        public Guid? ItemId { get; set; }

        public string Unit { get; set; }
        public decimal Ratio { get; set; }
        public decimal Price { get; set; }
        public decimal MinimumQuantity { get; set; }

        public PriceControlResource()
        {
            Id = Guid.Empty;
        }
    }
}
