﻿using System;
//━
namespace Jawara.Resources
{
    public class LookupResource
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
    }
}
