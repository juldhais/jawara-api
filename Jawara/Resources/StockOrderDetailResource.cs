﻿using Jawara.Helpers;
using System;

namespace Jawara.Resources
{
    public class StockOrderDetailResource
    {
        public Guid Id { get; set; }

        public Guid? StockOrderId { get; set; }

        public int Sequence { get; set; }

        [Mapping("Item.Code")]
        public string ItemCode { get; set; }

        [Mapping("Item.Description")]
        public string ItemDescription { get; set; }

        public Guid? ItemId { get; set; }

        public string Unit { get; set; }
        public decimal Ratio { get; set; }

        public decimal Quantity { get; set; }

        [NotMappedToModel]
        public decimal QuantityDelivered { get; set; }

        [NotMappedToModel]
        public decimal QuantityReceived { get; set; }
        public decimal Cost { get; set; }
        public decimal Subtotal { get; set; }
        public string Remarks { get; set; }

        public StockOrderDetailResource()
        {
            Id = Guid.Empty;
        }
    }
}
