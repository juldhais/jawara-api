﻿using System;

namespace Jawara.Resources
{
    public class PriceControlCategoryResource
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }

        public PriceControlCategoryResource()
        {
            Id = Guid.Empty;
        }
    }
}
