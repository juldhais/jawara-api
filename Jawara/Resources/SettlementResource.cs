﻿using Jawara.Helpers;
using System;
using System.Collections.Generic;

namespace Jawara.Resources
{
    public class SettlementResource
    {
        public Guid Id { get; set; }
        public string DocumentNumber { get; set; }
        public DateTime DocumentDate { get; set; }

        [Mapping("Location.Code")]
        public string LocationCode { get; set; }
        public Guid? LocationId { get; set; }

        [Mapping("User.UserName")]
        public string UserName { get; set; }
        public Guid? UserId { get; set; }

        public List<SettlementDetailResource> Details { get; set; }

        public SettlementResource()
        {
            Details = new List<SettlementDetailResource>();
        }
    }
}
