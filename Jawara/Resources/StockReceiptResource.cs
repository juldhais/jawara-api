﻿using Jawara.Helpers;
using System;
using System.Collections.Generic;

namespace Jawara.Resources
{
    public class StockReceiptResource
    {
        public Guid Id { get; set; }

        public string DocumentNumber { get; set; }
        public DateTime DocumentDate { get; set; }

        public Guid? StockDeliveryId { get; set; }
        public string ReferenceNumber { get; set; }

        [Mapping("DestinationLocation.Code")]
        public string DestinationLocationCode { get; set; }
        public Guid? DestinationLocationId { get; set; }

        [Mapping("SourceLocation.Code")]
        public string SourceLocationCode { get; set; }
        public Guid? SourceLocationId { get; set; }

        public string Remarks { get; set; }

        [NotMappedToModel]
        public DateTime? DateCreated { get; set; }

        [NotMappedToModel]
        public DateTime? DateUpdated { get; set; }

        [NotMappedToModel]
        public string CreatedBy { get; set; }

        [NotMappedToModel]
        public string UpdatedBy { get; set; }

        [NotMappedToModel]
        public Guid? CreatedById { get; set; }

        [NotMappedToModel]
        public Guid? UpdatedById { get; set; }

        public List<StockReceiptDetailResource> Details { get; set; }

        public StockReceiptResource()
        {
            Id = Guid.Empty;
            Details = new List<StockReceiptDetailResource>();
        }
    }
}
