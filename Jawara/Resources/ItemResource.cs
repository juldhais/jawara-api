﻿using Jawara.Helpers;
using System;
using System.Collections.Generic;

namespace Jawara.Resources
{
    public class ItemResource
    {
        [NotMappedToModel]
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string Barcode { get; set; }
        public string Unit { get; set; }

        public decimal PurchasePrice { get; set; }
        public decimal SalesPrice { get; set; }

        public bool Purchasable { get; set; }
        public bool Stockable { get; set; }
        public bool Manufacturable { get; set; }
        public bool Sellable { get; set; }

        [Mapping("ItemCategory.Code")]
        public string ItemCategoryCode { get; set; }

        [NotExport]
        public Guid? ItemCategoryId { get; set; }

        [Mapping("ItemSubcategory.Code")]
        public string ItemSubcategoryCode { get; set; }

        [NotExport]
        public Guid? ItemSubcategoryId { get; set; }


        public string Tag { get; set; }

        [Mapping("Supplier.Code")]
        public string SupplierCode { get; set; }

        public Guid? SupplierId { get; set; }
        public string SupplierItemCode { get; set; }


        public string Image { get; set; }
        public string Remarks { get; set; }

        public List<ItemUnitResource> Units { get; set; }

        public ItemResource()
        {
            Id = Guid.Empty;
            Units = new List<ItemUnitResource>();
        }
    }
}
