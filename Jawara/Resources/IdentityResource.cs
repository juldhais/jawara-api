﻿namespace Jawara.Resources
{
    public class IdentityResource
    {
        public UserResource User { get; set; }
        public RoleResource Role { get; set; }
        public string Token { get; set; }
    }
}
