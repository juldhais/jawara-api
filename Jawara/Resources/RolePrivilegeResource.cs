﻿using System;

namespace Jawara.Resources
{
    public class RolePrivilegeResource
    {
        public Guid Id { get; set; }
        public Guid? RoleId { get; set; }

        public string Privilege { get; set; }

        public RolePrivilegeResource()
        {
            Id = Guid.Empty;
        }
    }
}
