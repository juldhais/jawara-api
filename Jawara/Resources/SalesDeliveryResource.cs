﻿using Jawara.Helpers;
using System;
using System.Collections.Generic;

namespace Jawara.Resources
{
    public class SalesDeliveryResource
    {
        public Guid Id { get; set; }

        public string DocumentNumber { get; set; }
        public DateTime DocumentDate { get; set; }
        public string ReferenceNumber { get; set; }
        public DateTime? DueDate { get; set; }

        [NotMappedToModel]
        public string Status { get; set; }

        [Mapping("SalesOrder.DocumentNumber")]
        public string SalesOrderDocumentNumber { get; set; }
        public Guid? SalesOrderId { get; set; }

        [Mapping("Location.Code")]
        public string LocationCode { get; set; }
        public Guid? LocationId { get; set; }

        [Mapping("Customer.Code")]
        public string CustomerCode { get; set; }

        [Mapping("Customer.Description")]
        public string CustomerDescription { get; set; }

        public Guid? CustomerId { get; set; }

        public decimal TotalBeforeTax { get; set; }
        public decimal TotalValueAddedTax { get; set; }
        public decimal TotalIncomeTax { get; set; }
        public decimal TotalOtherTax { get; set; }
        public decimal TotalAfterTax { get; set; }

        [NotMappedToModel]
        public decimal TotalPayment { get; set; }

        [NotMappedToModel]
        public decimal Balance { get; set; }

        public string Remarks { get; set; }

        [NotMappedToModel]
        public DateTime? DateCreated { get; set; }

        [NotMappedToModel]
        public DateTime? DateUpdated { get; set; }

        [NotMappedToModel]
        public string CreatedBy { get; set; }

        [NotMappedToModel]
        public string UpdatedBy { get; set; }

        [NotMappedToModel]
        public Guid? CreatedById { get; set; }

        [NotMappedToModel]
        public Guid? UpdatedById { get; set; }

        public List<SalesDeliveryDetailResource> Details { get; set; }
        public List<DirectPaymentResource> Payments { get; set; }

        public SalesDeliveryResource()
        {
            Id = Guid.Empty;
            Details = new List<SalesDeliveryDetailResource>();
            Payments = new List<DirectPaymentResource>();
        }
    }
}
