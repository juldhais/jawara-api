﻿namespace Jawara.Resources
{
    public class PrintResource
    {
        public string Type { get; set; }
        public string Module { get; set; }
        public object Data { get; set; }
    }
}
