﻿using Jawara.Helpers;
using System;

namespace Jawara.Resources
{
    public class StockControlResource
    {
        public Guid Id { get; set; }

        [Mapping("StockControlCategory.Code")]
        public string StockControlCategoryCode { get; set; }

        public Guid? StockControlCategoryId { get; set; }

        [Mapping("Item.Code")]
        public string ItemCode { get; set; }

        [Mapping("Item.Description")]
        public string ItemDescription { get; set; }

        public Guid? ItemId { get; set; }

        public decimal MinimumStock { get; set; }
        public decimal MaximumStock { get; set; }
    }
}
