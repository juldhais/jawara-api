﻿namespace Jawara.Resources
{
    public class ConfigurationEditResource
    {
        public string CompanyName { get; set; }
        public string PrintHeader { get; set; }
        public string PrintSubheader { get; set; }
        public string PrintFooter { get; set; }
        public string AutoCutter { get; set; }
        public string PrintType { get; set; }
        public string UseDiscount { get; set; }
        public string UseTax { get; set; }
    }
}
