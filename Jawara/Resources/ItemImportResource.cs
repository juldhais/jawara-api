﻿namespace Jawara.Resources
{
    public class ItemImportResource
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public string Barcode { get; set; }
        public string Unit { get; set; }

        public decimal PurchasePrice { get; set; }
        public decimal SalesPrice { get; set; }

        public bool Purchasable { get; set; }
        public bool Stockable { get; set; }
        public bool Manufacturable { get; set; }
        public bool Sellable { get; set; }

        public string Category { get; set; }
        public string Subcategory { get; set; }
        public string Tag { get; set; }

        public string Supplier { get; set; }
        public string SupplierItemCode { get; set; }

        public string Remarks { get; set; }
    }
}
