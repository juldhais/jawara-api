﻿using Jawara.Helpers;
using System;

namespace Jawara.Resources
{
    public class CustomerResource
    {
        [NotMappedToModel]
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }

        [Mapping("CustomerCategory.Code")]
        public string CustomerCategoryCode { get; set; }
        public Guid? CustomerCategoryId { get; set; }

        [Mapping("PriceControlCategory.Code")]
        public string PriceControlCategoryCode { get; set; }
        public Guid? PriceControlCategoryId { get; set; }

        [Mapping("Location.Code")]
        public string LocationCode { get; set; }
        public Guid? LocationId { get; set; }

        public string Address1 { get; set; }
        public string Phone1 { get; set; }
        public string Email1 { get; set; }
        public string Website1 { get; set; }
        public string Contact1 { get; set; }

        public string Address2 { get; set; }
        public string Phone2 { get; set; }
        public string Email2 { get; set; }
        public string Website2 { get; set; }
        public string Contact2 { get; set; }

        public string TaxNumber { get; set; }

        public string Image { get; set; }
        public string Remarks { get; set; }

        public CustomerResource()
        {
            Id = Guid.Empty;
        }
    }
}
