﻿using Jawara.Helpers;
using System;
using System.Collections.Generic;

namespace Jawara.Resources
{
    public class SalesPaymentResource
    {
        public Guid Id { get; set; }

        public string DocumentNumber { get; set; }
        public DateTime DocumentDate { get; set; }
        public string ReferenceNumber { get; set; }

        public bool Direct { get; set; }

        [Mapping("Location.Code")]
        public string LocationCode { get; set; }
        public Guid? LocationId { get; set; }

        [Mapping("Customer.Code")]
        public string CustomerCode { get; set; }

        [Mapping("Customer.Description")]
        public string CustomerDescription { get; set; }
        public Guid? CustomerId { get; set; }

        public decimal TotalPayment { get; set; }
        public decimal TotalReturn { get; set; }
        public decimal AmountPaid { get; set; }

        [Mapping("PaymentMethod.Code")]
        public string PaymentMethodCode { get; set; }
        public Guid? PaymentMethodId { get; set; }

        public string Remarks { get; set; }

        [NotMappedToModel]
        public DateTime? DateCreated { get; set; }

        [NotMappedToModel]
        public DateTime? DateUpdated { get; set; }

        [NotMappedToModel]
        public string CreatedBy { get; set; }

        [NotMappedToModel]
        public string UpdatedBy { get; set; }

        [NotMappedToModel]
        public Guid? CreatedById { get; set; }

        [NotMappedToModel]
        public Guid? UpdatedById { get; set; }

        public List<SalesPaymentDetailResource> Details { get; set; }
        public List<SalesPaymentReturnResource> Returns { get; set; }

        public SalesPaymentResource()
        {
            Id = Guid.Empty;
            Details = new List<SalesPaymentDetailResource>();
            Returns = new List<SalesPaymentReturnResource>();
        }
    }
}
