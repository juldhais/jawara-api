﻿using Jawara.Helpers;
using System;

namespace Jawara.Resources
{
    public class SupplierResource
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }

        [Mapping("SupplierCategory.Code")]
        public string SupplierCategoryCode { get; set; }

        public Guid? SupplierCategoryId { get; set; }

        public string Address1 { get; set; }
        public string Phone1 { get; set; }
        public string Email1 { get; set; }
        public string Website1 { get; set; }
        public string Contact1 { get; set; }

        public string Address2 { get; set; }
        public string Phone2 { get; set; }
        public string Email2 { get; set; }
        public string Website2 { get; set; }
        public string Contact2 { get; set; }

        public string TaxNumber { get; set; }

        public string Image { get; set; }
        public string Remarks { get; set; }

        public SupplierResource()
        {
            Id = Guid.Empty;
        }
    }
}
