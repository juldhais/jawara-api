﻿using Jawara.Helpers;
using System;

namespace Jawara.Resources
{
    public class PurchasePaymentDetailResource
    {
        public Guid Id { get; set; }

        public Guid? PurchasePaymentId { get; set; }

        [Mapping("PurchaseReceipt.DocumentNumber")]
        public string DocumentNumber { get; set; }

        [Mapping("PurchaseReceipt.DocumentDate")]
        public DateTime DocumentDate { get; set; }
        public Guid? PurchaseReceiptId { get; set; }

        public decimal Balance { get; set; }
        public decimal Payment { get; set; }
        public decimal Remaining { get; set; }

        public string Remarks { get; set; }

        public PurchasePaymentDetailResource()
        {
            Id = Guid.Empty;
        }
    }
}
