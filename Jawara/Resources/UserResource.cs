﻿using Jawara.Helpers;
using System;

namespace Jawara.Resources
{
    public class UserResource
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string Description { get; set; }

        [Mapping("Role.Code")]
        public string RoleCode { get; set; }
        public Guid? RoleId { get; set; }

        [Mapping("Role.IsAdministrator")]
        public bool IsAdministrator { get; set; }

        [Mapping("DefaultLocation.Code")]
        public string DefaultLocationCode { get; set; }
        public Guid? DefaultLocationId { get; set; }

        public UserResource()
        {
            Id = Guid.Empty;
        }
    }
}
