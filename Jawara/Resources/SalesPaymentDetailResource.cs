﻿using Jawara.Helpers;
using System;

namespace Jawara.Resources
{
    public class SalesPaymentDetailResource
    {
        public Guid Id { get; set; }

        public Guid? SalesPaymentId { get; set; }

        [Mapping("SalesDelivery.DocumentNumber")]
        public string DocumentNumber { get; set; }

        [Mapping("SalesDelivery.DocumentDate")]
        public DateTime DocumentDate { get; set; }

        public Guid? SalesDeliveryId { get; set; }

        public decimal Balance { get; set; }
        public decimal Payment { get; set; }
        public decimal Remaining { get; set; }

        public string Remarks { get; set; }

        public SalesPaymentDetailResource()
        {
            Id = Guid.Empty;
        }
    }
}
