﻿using System;

namespace Jawara.Resources
{
    public class PaymentMethodResource
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public int Sequence { get; set; }

        public bool Purchase { get; set; }
        public bool Sales { get; set; }

        public PaymentMethodResource()
        {
            Id = Guid.Empty;
        }
    }
}
