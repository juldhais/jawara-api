﻿namespace Jawara.Resources
{
    public class PriceControlImportResource
    {
        public string PriceControlCategory { get; set; }
        public string Item { get; set; }
        public string Unit { get; set; }
        public decimal Ratio { get; set; }
        public decimal Price { get; set; }
        public decimal MinimumQuantity { get; set; }
    }
}
