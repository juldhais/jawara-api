﻿using System;

namespace Jawara.Resources
{
    public class UnitResource
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public int Sequence { get; set; }

        public DateTime? DateCreated { get; set; }

        public UnitResource()
        {
            Id = Guid.Empty;
        }
    }
}
