﻿using Jawara.Helpers;
using System;
using System.Collections.Generic;

namespace Jawara.Resources
{
    public class BillOfMaterialResource
    {
        public Guid Id { get; set; }

        public string Code { get; set; }
        public string Description { get; set; }

        [Mapping("Parent.Code")]
        public string ParentCode { get; set; }

        [Mapping("Parent.Description")]
        public string ParentDescription { get; set; }
        public Guid? ParentId { get; set; }

        public decimal ParentQuantity { get; set; }
        public string ParentUnit { get; set; }
        public decimal ParentRatio { get; set; }

        public List<BillOfMaterialDetailResource> Details { get; set; }

        public BillOfMaterialResource()
        {
            Details = new List<BillOfMaterialDetailResource>();
        }
    }
}
