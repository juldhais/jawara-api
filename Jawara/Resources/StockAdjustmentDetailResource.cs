﻿using Jawara.Helpers;
using System;

namespace Jawara.Resources
{
    public class StockAdjustmentDetailResource
    {
        public Guid Id { get; set; }

        public Guid? StockAdjustmentId { get; set; }

        public int Sequence { get; set; }

        [Mapping("Item.Code")]
        public string ItemCode { get; set; }

        [Mapping("Item.Description")]
        public string ItemDescription { get; set; }

        public Guid? ItemId { get; set; }

        public string Unit { get; set; }
        public decimal Ratio { get; set; }

        public decimal Quantity { get; set; }
        public decimal Cost { get; set; }

        public string Remarks { get; set; }

        public StockAdjustmentDetailResource()
        {
            Id = Guid.Empty;
        }
    }
}
