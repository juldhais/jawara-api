﻿namespace Jawara.Resources
{
    public class StockControlImportResource
    {
        public string StockControlCategory { get; set; }
        public string Item { get; set; }

        public decimal MinimumStock { get; set; }
        public decimal MaximumStock { get; set; }
    }
}