﻿using System;

namespace Jawara.Resources
{
    public class ConfigurationResource
    {
        public Guid Id { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }

        public ConfigurationResource()
        {
            Id = Guid.Empty;
        }
    }
}
