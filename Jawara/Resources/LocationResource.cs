﻿using Jawara.Helpers;
using System;

namespace Jawara.Resources
{
    public class LocationResource
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string Type { get; set; } // Head Office, Warehouse, Store

        [Mapping("PriceControlCategory.Code")]
        public string PriceControlCategoryCode { get; set; }
        public Guid? PriceControlCategoryId { get; set; }


        [Mapping("StockControlCategory.Code")]
        public string StockControlCategoryCode { get; set; }
        public Guid? StockControlCategoryId { get; set; }

        public string Address { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Contact { get; set; }

        public string Remarks { get; set; }

        public LocationResource()
        {
            Id = Guid.Empty;
        }
    }
}
