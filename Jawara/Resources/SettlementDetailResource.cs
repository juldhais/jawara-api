﻿using Jawara.Helpers;
using System;

namespace Jawara.Resources
{
    public class SettlementDetailResource
    {
        public Guid Id { get; set; }
        public Guid? SettlementId { get; set; }

        [Mapping("PaymentMethod.Code")]
        public string PaymentMethodCode { get; set; }
        public Guid? PaymentMethodId { get; set; }
        public decimal InputBalance { get; set; }

        [NotMappedToModel]
        public decimal SystemBalance { get; set; }

        [NotMappedToModel]
        public decimal Difference { get; set; }
    }
}
