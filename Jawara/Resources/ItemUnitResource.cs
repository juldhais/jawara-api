﻿using System;

namespace Jawara.Resources
{
    public class ItemUnitResource
    {
        public Guid Id { get; set; }

        public Guid? ItemId { get; set; }

        public string Unit { get; set; }
        public decimal Ratio { get; set; }

        public ItemUnitResource()
        {
            Id = Guid.Empty;
        }
    }
}
