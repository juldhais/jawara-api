﻿using System;

namespace Jawara.Resources
{
    public class ItemSubcategoryResource
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }

        public ItemSubcategoryResource()
        {
            Id = Guid.Empty;
        }
    }
}
