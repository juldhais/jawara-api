﻿using System;

namespace Jawara.Resources
{
    public class ItemTagResource
    {
        public Guid Id { get; set; }
        public string Code { get; set; }

        public ItemTagResource()
        {
            Id = Guid.Empty;
        }
    }
}
