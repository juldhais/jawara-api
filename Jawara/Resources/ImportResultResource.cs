﻿using System.Collections.Generic;

namespace Jawara.Resources
{
    public class ImportResultResource
    {
        public bool IsError { get; set; }
        public List<string> ErrorMessages { get; set; }

        public ImportResultResource()
        {
            IsError = false;
            ErrorMessages = new List<string>();
        }

        public void AddErrorMessage(string message)
        {
            IsError = true;
            ErrorMessages.Add(message);
        }
    }
}
