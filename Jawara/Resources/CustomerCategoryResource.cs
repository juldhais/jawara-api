﻿using System;

namespace Jawara.Resources
{
    public class CustomerCategoryResource
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }

        public CustomerCategoryResource()
        {
            Id = Guid.Empty;
        }
    }
}
