﻿using Jawara.Helpers;
using System;

namespace Jawara.Resources
{
    public class ManufacturingOrderDetailResource
    {
        public Guid Id { get; set; }
        public Guid? ManufacturingOrderId { get; set; }
        public int Sequence { get; set; }

        [Mapping("Component.Code")]
        public string ComponentCode { get; set; }

        [Mapping("Component.Description")]
        public string ComponentDescription { get; set; }
        public Guid? ComponentId { get; set; }

        public decimal Quantity { get; set; }
        public decimal QuantityIssued { get; set; }
        public string Unit { get; set; }
        public decimal Ratio { get; set; }
        public decimal Cost { get; set; }
        public decimal SubtotalCost { get; set; }
        public string Remarks { get; set; }
    }
}
