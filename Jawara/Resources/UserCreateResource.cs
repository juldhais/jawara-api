﻿using System;

namespace Jawara.Resources
{
    public class UserCreateResource
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Description { get; set; }

        public Guid? RoleId { get; set; }

        public Guid? DefaultLocationId { get; set; }

        public UserCreateResource()
        {
            Id = Guid.Empty;
        }
    }
}
