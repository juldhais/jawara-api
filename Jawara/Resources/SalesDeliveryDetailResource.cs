﻿using Jawara.Helpers;
using System;

namespace Jawara.Resources
{
    public class SalesDeliveryDetailResource
    {
        public Guid Id { get; set; }

        public Guid? SalesDeliveryId { get; set; }

        public int Sequence { get; set; }

        [Mapping("Item.Code")]
        public string ItemCode { get; set; }

        [Mapping("Item.Description")]
        public string ItemDescription { get; set; }

        public Guid? ItemId { get; set; }

        public string Unit { get; set; }
        public decimal Ratio { get; set; }

        public decimal Quantity { get; set; }
        public decimal Price { get; set; }

        public decimal DiscountPercent1 { get; set; }
        public decimal DiscountPercent2 { get; set; }
        public decimal DiscountAmount { get; set; }
        public decimal SubtotalBeforeTax { get; set; }

        [Mapping("ValueAddedTax.Code")]
        public string ValueAddedTaxCode { get; set; }
        public Guid? ValueAddedTaxId { get; set; }
        public decimal ValueAddedTaxAmount { get; set; }

        [Mapping("IncomeTax.Code")]
        public string IncomeTaxCode { get; set; }
        public Guid? IncomeTaxId { get; set; }
        public decimal IncomeTaxAmount { get; set; }

        [Mapping("OtherTax.Code")]
        public string OtherTaxCode { get; set; }
        public Guid? OtherTaxId { get; set; }
        public decimal OtherTaxAmount { get; set; }

        public decimal SubtotalAfterTax { get; set; }

        public string Remarks { get; set; }

        public SalesDeliveryDetailResource()
        {
            Id = Guid.Empty;
        }
    }
}
