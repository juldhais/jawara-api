﻿using System;
using System.Collections.Generic;

namespace Jawara.Resources
{
    public class RoleResource
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public bool IsAdministrator { get; set; }

        public List<string> Privileges { get; set; }

        public RoleResource()
        {
            Id = Guid.Empty;
            Privileges = new List<string>();
        }
    }
}
