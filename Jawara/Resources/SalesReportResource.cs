﻿using Jawara.Helpers;
using System;
using System.Collections.Generic;

namespace Jawara.Resources
{
    public class SalesReportResource
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Location { get; set; }
        public string CustomerCategory { get; set; }
        public string Customer { get; set; }
        public string User { get; set; }
        public string Status { get; set; }

        public decimal TotalBeforeTax { get; set; }
        public decimal TotalTax { get; set; }
        public decimal TotalAfterTax { get; set; }

        public decimal TotalPayment { get; set; }
        public decimal Balance { get; set; }

        public List<SalesReportData> Data { get; set; }

        public SalesReportResource()
        {
            Data = new List<SalesReportData>();
        }
    }

    public class SalesReportData
    {
        public Guid Id { get; set; }

        public string DocumentNumber { get; set; }
        public DateTime DocumentDate { get; set; }
        public string ReferenceNumber { get; set; }

        public string Status { get; set; }

        [Mapping("SalesOrder.DocumentNumber")]
        public string SalesOrderDocumentNumber { get; set; }
        public Guid? SalesOrderId { get; set; }

        [Mapping("Location.Name")]
        public string Location { get; set; }
        public Guid? LocationId { get; set; }

        [Mapping("Customer.Name")]
        public string Customer { get; set; }
        public Guid? CustomerId { get; set; }

        public decimal TotalBeforeTax { get; set; }
        public decimal TotalTax { get; set; }
        public decimal TotalAfterTax { get; set; }

        public decimal TotalPayment { get; set; }
        public decimal Balance { get; set; }

        public string Remarks { get; set; }
    }

    public class SalesReportParam
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Guid? LocationId { get; set; }
        public Guid? CustomerCategoryId { get; set; }
        public Guid? CustomerId { get; set; }
        public Guid? UserId { get; set; }
        public string Status { get; set; }
    }
}
