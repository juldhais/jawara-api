﻿using Jawara.Enums;
using Jawara.Helpers;
using Jawara.Resources;
using Jawara.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Jawara.API.Controllers
{
    [Route("configuration")]
    public class ConfigurationController : ControllerBase
    {
        private readonly ConfigurationService configurationService;
        private readonly RoleService roleService;

        public ConfigurationController(ConfigurationService configurationService,
                                      RoleService roleService)
        {
            this.configurationService = configurationService;
            this.roleService = roleService;
        }

        [HttpGet("{key}")]
        [Authorize]
        public ActionResult<ConfigurationResource> Get(string key)
        {
            var resource = configurationService.Get(key);

            return Ok(resource);
        }

        [HttpPost]
        [Authorize]
        public ActionResult Set([FromBody] ConfigurationResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.Configuration);

            configurationService.Set(resource, this.GetUserId());

            return Ok();
        }

        [HttpGet("get")]
        [Authorize]
        public ActionResult GetEdit()
        {
            var resource = configurationService.GetEdit();

            return Ok(resource);
        }

        [HttpPost("set")]
        [Authorize]
        public ActionResult SetEdit([FromBody] ConfigurationEditResource resource)
        {
            configurationService.SetEdit(resource, this.GetUserId());

            return Ok();
        }

        [HttpGet("date")]
        public ActionResult GetServerDate()
        {
            return Ok(DateTime.Now);
        }
    }
}
