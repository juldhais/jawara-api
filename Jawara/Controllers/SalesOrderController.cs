﻿using Jawara.Enums;
using Jawara.Helpers;
using Jawara.Resources;
using Jawara.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Jawara.API.Controllers
{
    [Route("sales-order")]
    public class SalesOrderController : ControllerBase
    {
        private readonly SalesOrderService orderService;
        private readonly RoleService roleService;

        public SalesOrderController(SalesOrderService orderService,
                                      RoleService roleService)
        {
            this.orderService = orderService;
            this.roleService = roleService;
        }

        [HttpGet]
        [Authorize]
        public ActionResult<PagingResult<SalesOrderResource>> GetList(string keyword, string status, Guid? customerId, Guid? locationId, DateTime? startDate, DateTime? endDate, int page, int size)
        {
            var result = orderService.GetList(keyword, status, customerId, locationId, startDate, endDate, page, size);

            return Ok(result);
        }

        [HttpGet("lookup")]
        [Authorize]
        public ActionResult<List<LookupResource>> GetLookup(string keyword, string status, Guid? customerId, Guid? locationId)
        {
            var result = orderService.GetLookup(keyword, status, customerId, locationId);

            return Ok(result);
        }

        [HttpGet("{id}/delivery")]
        [Authorize]
        public ActionResult<PagingResult<SalesDeliveryResource>> GetListDelivery(Guid? id, int page, int size)
        {
            var result = orderService.GetListDelivery(id, page, size);

            return Ok(result);
        }

        [HttpGet("{id}")]
        [Authorize]
        public ActionResult<SalesOrderResource> Get(Guid id)
        {
            var resource = orderService.Get(id);

            return Ok(resource);
        }

        [HttpGet("lookup/{id}")]
        [Authorize]
        public ActionResult<LookupResource> GetSingleLookup(Guid? id)
        {
            var result = orderService.GetSingleLookup(id);

            return Ok(result);
        }

        [HttpPost]
        [Authorize]
        public ActionResult<ScalarResource> Create([FromBody] SalesOrderResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.SalesOrderCreate);

            var result = orderService.Create(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpPut("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> Update(Guid id, [FromBody] SalesOrderResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.SalesOrderUpdate);

            resource.Id = id;
            var result = orderService.Update(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpDelete("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> Delete(Guid id)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.SalesOrderDelete);

            orderService.Delete(id, this.GetUserId());

            return NoContent();
        }

        [HttpPost("open")]
        [Authorize]
        public ActionResult Open([FromBody] ScalarResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.SalesOrderOpen);

            orderService.Open(resource.Value.ToGuid(), this.GetUserId());

            return Ok();
        }

        [HttpPost("close")]
        [Authorize]
        public ActionResult Close([FromBody] ScalarResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.SalesOrderClose);

            orderService.Close(resource.Value.ToGuid(), this.GetUserId());

            return Ok();
        }
    }
}
