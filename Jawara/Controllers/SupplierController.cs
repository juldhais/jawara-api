﻿using Jawara.Enums;
using Jawara.Helpers;
using Jawara.Resources;
using Jawara.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text.Json;

namespace Jawara.API.Controllers
{
    [Route("supplier")]
    public class SupplierController : ControllerBase
    {
        private readonly SupplierService supplierService;
        private readonly RoleService roleService;
        private readonly IWebHostEnvironment webhostEnvironment;

        public SupplierController(SupplierService supplierService,
                                  RoleService roleService,
                                  IWebHostEnvironment webhostEnvironment)
        {
            this.supplierService = supplierService;
            this.roleService = roleService;
            this.webhostEnvironment = webhostEnvironment;
        }

        [HttpGet]
        [Authorize]
        public ActionResult<PagingResult<SupplierResource>> GetList(string keyword, int page, int size)
        {
            var result = supplierService.GetList(keyword, page, size);

            return Ok(result);
        }

        [HttpGet("lookup")]
        [Authorize]
        public ActionResult<List<LookupResource>> GetLookup(string keyword)
        {
            var result = supplierService.GetLookup(keyword);

            return Ok(result);
        }

        [HttpGet("{id}")]
        [Authorize]
        public ActionResult<SupplierResource> Get(Guid id)
        {
            var resource = supplierService.Get(id);

            return Ok(resource);
        }

        [HttpGet("lookup/{id}")]
        [Authorize]
        public ActionResult<LookupResource> GetSingleLookup(Guid? id)
        {
            var result = supplierService.GetSingleLookup(id);

            return Ok(result);
        }

        [HttpPost]
        [Authorize]
        public ActionResult<ScalarResource> Create([FromBody] SupplierResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.SupplierCreate);

            var result = supplierService.Create(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpPut("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> Update(Guid id, [FromBody] SupplierResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.SupplierUpdate);

            resource.Id = id;
            var result = supplierService.Update(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpPatch("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> PatchUpdate(Guid id, [FromBody] JsonElement patchResource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.ItemUpdate);

            var resource = supplierService.Get(id);
            var json = patchResource.GetRawText();
            JsonConvert.PopulateObject(json, resource);
            resource.Id = id;
            var result = supplierService.Update(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpDelete("{id}")]
        [Authorize]
        public ActionResult Delete(Guid id)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.SupplierDelete);

            supplierService.Delete(id, this.GetUserId());

            return NoContent();
        }

        [HttpPost("import")]
        [Authorize]
        [RequestSizeLimit(5242880)]
        public IActionResult Import()
        {
            try
            {
                roleService.CheckPrivilege(this.GetRoleId(), Privilege.SupplierCreate);

                if (Request.Form.Files == null || Request.Form.Files.Count == 0)
                    return NoContent();

                var file = Request.Form.Files[0];

                if (!file.FileName.EndsWith(".xlsx"))
                    throw new BadRequestException("File must be excel document (xlsx).");

                var filePath = this.SaveFile(webhostEnvironment, file);
                var list = ExcelHelper.Import<SupplierImportResource>(filePath);

                if (list.Count == 0)
                    throw new BadRequestException("Excel file does not contain acceptable data.");

                var result = supplierService.Import(list, this.GetUserId());

                return Ok(result);
            }
            catch (Exception ex)
            {
                var result = new ImportResultResource();
                result.IsError = true;
                result.ErrorMessages.Add(ex.GetBaseException().Message);
                return Ok(result);
            }
        }
    }
}
