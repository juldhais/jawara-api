﻿using Jawara.Enums;
using Jawara.Helpers;
using Jawara.Resources;
using Jawara.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Jawara.Controllers
{
    [Route("item")]
    public class ItemController : Controller
    {
        private readonly ItemService itemService;
        private readonly RoleService roleService;
        private readonly IWebHostEnvironment webhostEnvironment;

        public ItemController(ItemService itemService,
                              RoleService roleService,
                              IWebHostEnvironment webhostEnvironment)
        {
            this.itemService = itemService;
            this.roleService = roleService;
            this.webhostEnvironment = webhostEnvironment;
        }

        [HttpGet]
        [Authorize]
        public ActionResult<PagingResult<ItemResource>> GetList(string keyword, int page, int size)
        {
            var result = itemService.GetList(keyword, page, size);

            return Ok(result);
        }

        [HttpGet("purchasable")]
        [Authorize]
        public ActionResult<PagingResult<ItemResource>> GetListPurchasable(string keyword, int page, int size)
        {
            var result = itemService.GetListPurchasable(keyword, page, size);

            return Ok(result);
        }

        [HttpGet("stockable")]
        [Authorize]
        public ActionResult<PagingResult<ItemResource>> GetListStockable(string keyword, int page, int size)
        {
            var result = itemService.GetListStockable(keyword, page, size);

            return Ok(result);
        }

        [HttpGet("manufacturable")]
        [Authorize]
        public ActionResult<PagingResult<ItemResource>> GetListManufacturable(string keyword, int page, int size)
        {
            var result = itemService.GetListManufacturable(keyword, page, size);

            return Ok(result);
        }

        [HttpGet("sellable")]
        [Authorize]
        public ActionResult<PagingResult<ItemResource>> GetListSellable(string keyword, int page, int size)
        {
            var result = itemService.GetListSellable(keyword, page, size);

            return Ok(result);
        }

        [HttpGet("lookup")]
        [Authorize]
        public ActionResult<List<LookupResource>> GetLookup(string keyword)
        {
            var result = itemService.GetLookup(keyword);

            return Ok(result);
        }

        [HttpGet("lookup/stockable")]
        [Authorize]
        public ActionResult<List<LookupResource>> GetLookupStockable(string keyword)
        {
            var result = itemService.GetLookupStockable(keyword);

            return Ok(result);
        }

        [HttpGet("lookup/purchasable")]
        [Authorize]
        public ActionResult<List<LookupResource>> GetLookupPurchasable(string keyword)
        {
            var result = itemService.GetLookupPurchasable(keyword);

            return Ok(result);
        }

        [HttpGet("lookup/manufacturable")]
        [Authorize]
        public ActionResult<List<LookupResource>> GetLookupManufacturable(string keyword)
        {
            var result = itemService.GetLookupManufacturable(keyword);

            return Ok(result);
        }

        [HttpGet("lookup/sellable")]
        [Authorize]
        public ActionResult<List<LookupResource>> GetLookupSellable(string keyword)
        {
            var result = itemService.GetLookupSellable(keyword);

            return Ok(result);
        }

        [HttpGet("{id}")]
        [Authorize]
        public ActionResult<ItemResource> Get(Guid id)
        {
            var resource = itemService.Get(id);

            return Ok(resource);
        }

        [HttpGet("code/{code}")]
        [Authorize]
        public ActionResult<ItemResource> GetByCode(string code)
        {
            var resource = itemService.Get(code);

            return Ok(resource);
        }

        [HttpGet("lookup/{id}")]
        [Authorize]
        public ActionResult<LookupResource> GetSingleLookup(Guid? id)
        {
            var result = itemService.GetSingleLookup(id);

            return Ok(result);
        }

        [HttpPost]
        [Authorize]
        public ActionResult<ScalarResource> Create([FromBody] ItemResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.ItemCreate);

            var result = itemService.Create(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpPut("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> Update(Guid id, [FromBody] ItemResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.ItemUpdate);

            resource.Id = id;
            var result = itemService.Update(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpDelete("{id}")]
        [Authorize]
        public ActionResult Delete(Guid id)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.ItemDelete);

            itemService.Delete(id, this.GetUserId());

            return NoContent();
        }

        [HttpGet("stock")]
        [Authorize]
        public ActionResult<PagingResult<ItemStockResource>> GetStock(Guid? itemId, Guid? locationId, int page, int size)
        {
            var result = itemService.GetStock(itemId, locationId, page, size);

            return Ok(result);
        }

        [HttpPost("sales-price")]
        [Authorize]
        public ActionResult<ScalarResource> GetSalesPrice([FromBody] SalesPriceResource resource)
        {
            var result = itemService.GetSalesPrice(resource);

            return Ok(new ScalarResource(result));
        }

        [HttpPost("import")]
        [Authorize]
        [RequestSizeLimit(5242880)]
        public IActionResult Import()
        {
            try
            {
                roleService.CheckPrivilege(this.GetRoleId(), Privilege.ItemCreate);

                if (Request.Form.Files == null || Request.Form.Files.Count == 0)
                    return NoContent();

                var file = Request.Form.Files[0];

                if (!file.FileName.EndsWith(".xlsx"))
                    throw new BadRequestException("File must be excel document (xlsx).");

                var filePath = this.SaveFile(webhostEnvironment, file);
                var list = ExcelHelper.Import<ItemImportResource>(filePath);

                if (list.Count == 0)
                    throw new BadRequestException("Excel file does not contain acceptable data.");

                var result = itemService.Import(list, this.GetUserId());

                return Ok(result);
            }
            catch (Exception ex)
            {
                var result = new ImportResultResource();
                result.IsError = true;
                result.ErrorMessages.Add(ex.GetBaseException().Message);
                return Ok(result);
            }
        }
    }
}
