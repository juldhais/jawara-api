﻿using Jawara.Enums;
using Jawara.Helpers;
using Jawara.Resources;
using Jawara.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Text.Json;

namespace Jawara.API.Controllers
{
    [Route("stock-control")]
    public class StockControlController : ControllerBase
    {
        private readonly StockControlService stockControlService;
        private readonly RoleService roleService;
        private readonly IWebHostEnvironment webhostEnvironment;

        public StockControlController(StockControlService stockControlService,
                                      RoleService roleService,
                                      IWebHostEnvironment webhostEnvironment)
        {
            this.stockControlService = stockControlService;
            this.roleService = roleService;
            this.webhostEnvironment = webhostEnvironment;
        }

        [HttpGet]
        [Authorize]
        public ActionResult<PagingResult<StockControlResource>> GetList(Guid? stockControlCategoryId, Guid? itemId, int page, int size)
        {
            var result = stockControlService.GetList(stockControlCategoryId, itemId, page, size);

            return Ok(result);
        }

        [HttpGet("{id}")]
        [Authorize]
        public ActionResult<StockControlResource> Get(Guid id)
        {
            var resource = stockControlService.Get(id);

            return Ok(resource);
        }

        [HttpPost]
        [Authorize]
        public ActionResult<ScalarResource> Create([FromBody] StockControlResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.StockControlCreate);

            var result = stockControlService.Create(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpPut("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> Update(Guid id, [FromBody] StockControlResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.StockControlUpdate);

            resource.Id = id;
            var result = stockControlService.Update(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpPatch("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> PatchUpdate(Guid id, [FromBody] JsonElement patchResource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.ItemUpdate);

            var resource = stockControlService.Get(id);
            var json = patchResource.GetRawText();
            JsonConvert.PopulateObject(json, resource);
            resource.Id = id;
            var result = stockControlService.Update(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpDelete("{id}")]
        [Authorize]
        public ActionResult Delete(Guid id)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.StockControlDelete);

            stockControlService.Delete(id, this.GetUserId());

            return NoContent();
        }

        [HttpPost("import")]
        [Authorize]
        [RequestSizeLimit(5242880)]
        public IActionResult Import()
        {
            try
            {
                roleService.CheckPrivilege(this.GetRoleId(), Privilege.StockControlCreate);

                if (Request.Form.Files == null || Request.Form.Files.Count == 0)
                    return NoContent();

                var file = Request.Form.Files[0];

                if (!file.FileName.EndsWith(".xlsx"))
                    throw new BadRequestException("File must be excel document (xlsx).");

                var filePath = this.SaveFile(webhostEnvironment, file);
                var list = ExcelHelper.Import<StockControlImportResource>(filePath);

                if (list.Count == 0)
                    throw new BadRequestException("Excel file does not contain acceptable data.");

                var result = stockControlService.Import(list, this.GetUserId());

                return Ok(result);
            }
            catch (Exception ex)
            {
                var result = new ImportResultResource();
                result.IsError = true;
                result.ErrorMessages.Add(ex.GetBaseException().Message);
                return Ok(result);
            }
        }
    }
}
