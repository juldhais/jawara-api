﻿using Jawara.Enums;
using Jawara.Helpers;
using Jawara.Resources;
using Jawara.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text.Json;

namespace Jawara.API.Controllers
{
    [Route("item-subcategory")]
    public class ItemSubcategoryController : ControllerBase
    {
        private readonly ItemSubcategoryService itemSubcategoryService;
        private readonly RoleService roleService;

        public ItemSubcategoryController(ItemSubcategoryService itemSubcategoryService,
                                         RoleService roleService)
        {
            this.itemSubcategoryService = itemSubcategoryService;
            this.roleService = roleService;
        }

        [HttpGet]
        [Authorize]
        public ActionResult<PagingResult<ItemSubcategoryResource>> GetList(string keyword, int page, int size)
        {
            var result = itemSubcategoryService.GetList(keyword, page, size);

            return Ok(result);
        }

        [HttpGet("lookup")]
        [Authorize]
        public ActionResult<List<LookupResource>> GetLookup(string keyword)
        {
            var result = itemSubcategoryService.GetLookup(keyword);

            return Ok(result);
        }

        [HttpGet("{id}")]
        [Authorize]
        public ActionResult<ItemSubcategoryResource> Get(Guid id)
        {
            var resource = itemSubcategoryService.Get(id);

            return Ok(resource);
        }

        [HttpGet("lookup/{id}")]
        [Authorize]
        public ActionResult<LookupResource> GetSingleLookup(Guid? id)
        {
            var result = itemSubcategoryService.GetSingleLookup(id);

            return Ok(result);
        }

        [HttpPost]
        [Authorize]
        public ActionResult<ScalarResource> Create([FromBody] ItemSubcategoryResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.ItemSubcategoryCreate);

            var result = itemSubcategoryService.Create(resource, null);

            return Ok(new ScalarResource(result));
        }

        [HttpPut("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> Update(Guid id, [FromBody] ItemSubcategoryResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.ItemSubcategoryUpdate);

            resource.Id = id;
            var result = itemSubcategoryService.Update(resource, null);

            return Ok(new ScalarResource(result));
        }

        [HttpPatch("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> PatchUpdate(Guid id, [FromBody] JsonElement patchResource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.ItemSubcategoryUpdate);

            var resource = itemSubcategoryService.Get(id);
            var json = patchResource.GetRawText();
            JsonConvert.PopulateObject(json, resource);
            resource.Id = id;
            var result = itemSubcategoryService.Update(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpDelete("{id}")]
        [Authorize]
        public ActionResult Delete(Guid id)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.ItemSubcategoryDelete);

            itemSubcategoryService.Delete(id, null);

            return NoContent();
        }
    }
}
