﻿using Jawara.Enums;
using Jawara.Helpers;
using Jawara.Resources;
using Jawara.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text.Json;

namespace Jawara.API.Controllers
{
    [Route("supplier-category")]
    public class SupplierCategoryController : ControllerBase
    {
        private readonly SupplierCategoryService supplierCategoryService;
        private readonly RoleService roleService;

        public SupplierCategoryController(SupplierCategoryService supplierCategoryService,
                                      RoleService roleService)
        {
            this.supplierCategoryService = supplierCategoryService;
            this.roleService = roleService;
        }

        [HttpGet]
        [Authorize]
        public ActionResult<PagingResult<SupplierCategoryResource>> GetList(string keyword, int page, int size)
        {
            var result = supplierCategoryService.GetList(keyword, page, size);

            return Ok(result);
        }

        [HttpGet("lookup")]
        [Authorize]
        public ActionResult<List<LookupResource>> GetLookup(string keyword)
        {
            var result = supplierCategoryService.GetLookup(keyword);

            return Ok(result);
        }

        [HttpGet("{id}")]
        [Authorize]
        public ActionResult<SupplierCategoryResource> Get(Guid id)
        {
            var resource = supplierCategoryService.Get(id);

            return Ok(resource);
        }

        [HttpGet("lookup/{id}")]
        [Authorize]
        public ActionResult<LookupResource> GetSingleLookup(Guid? id)
        {
            var result = supplierCategoryService.GetSingleLookup(id);

            return Ok(result);
        }

        [HttpPost]
        [Authorize]
        public ActionResult<ScalarResource> Create([FromBody] SupplierCategoryResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.SupplierCategoryCreate);

            var result = supplierCategoryService.Create(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpPut("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> Update(Guid id, [FromBody] SupplierCategoryResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.SupplierCategoryUpdate);

            resource.Id = id;
            var result = supplierCategoryService.Update(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpPatch("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> PatchUpdate(Guid id, [FromBody] JsonElement patchResource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.SupplierCategoryUpdate);

            var resource = supplierCategoryService.Get(id);
            var json = patchResource.GetRawText();
            JsonConvert.PopulateObject(json, resource);
            resource.Id = id;
            var result = supplierCategoryService.Update(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpDelete("{id}")]
        [Authorize]
        public ActionResult Delete(Guid id)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.SupplierCategoryDelete);

            supplierCategoryService.Delete(id, this.GetUserId());

            return NoContent();
        }
    }
}
