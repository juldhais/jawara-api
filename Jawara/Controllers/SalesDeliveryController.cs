﻿using Jawara.Enums;
using Jawara.Helpers;
using Jawara.Resources;
using Jawara.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Jawara.API.Controllers
{
    [Route("sales-delivery")]
    public class SalesDeliveryController : ControllerBase
    {
        private readonly SalesDeliveryService deliveryService;
        private readonly RoleService roleService;
        private readonly SalesDeliveryPrintService printService;

        public SalesDeliveryController(SalesDeliveryService deliveryService,
            RoleService roleService,
            SalesDeliveryPrintService printService)
        {
            this.deliveryService = deliveryService;
            this.roleService = roleService;
            this.printService = printService;
        }

        [HttpGet]
        [Authorize]
        public ActionResult<PagingResult<SalesDeliveryResource>> GetList(string keyword, string status, Guid? customerId, Guid? locationId, DateTime? startDate, DateTime? endDate, int page, int size)
        {
            var result = deliveryService.GetList(keyword, status, customerId, locationId, startDate, endDate, page, size);

            return Ok(result);
        }

        [HttpGet("{id}")]
        [Authorize]
        public ActionResult<SalesDeliveryResource> Get(Guid id)
        {
            var resource = deliveryService.Get(id);

            return Ok(resource);
        }

        [HttpPost]
        [Authorize]
        public ActionResult<ScalarResource> Create([FromBody] SalesDeliveryResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.SalesDeliveryCreate);

            var result = deliveryService.Create(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpPut("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> Update(Guid id, [FromBody] SalesDeliveryResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.SalesDeliveryUpdate);

            resource.Id = id;
            var result = deliveryService.Update(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpDelete("{id}")]
        [Authorize]
        public ActionResult Delete(Guid id)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.SalesDeliveryDelete);

            deliveryService.Delete(id, this.GetUserId());

            return NoContent();
        }

        [HttpGet("print/{id}")]
        [Authorize]
        public ActionResult<PrintResource> Print(Guid id)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.SalesDelivery);

            var result = printService.Print(id);

            return Ok(result);
        }
    }
}
