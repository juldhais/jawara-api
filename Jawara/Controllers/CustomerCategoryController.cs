﻿using Jawara.Enums;
using Jawara.Helpers;
using Jawara.Resources;
using Jawara.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text.Json;

namespace Jawara.API.Controllers
{
    [Route("customer-category")]
    public class CustomerCategoryController : ControllerBase
    {
        private readonly CustomerCategoryService customerCategoryService;
        private readonly RoleService roleService;

        public CustomerCategoryController(CustomerCategoryService customerCategoryService,
                                      RoleService roleService)
        {
            this.customerCategoryService = customerCategoryService;
            this.roleService = roleService;
        }

        [HttpGet]
        [Authorize]
        public ActionResult<PagingResult<CustomerCategoryResource>> GetList(string keyword, int page, int size)
        {
            var result = customerCategoryService.GetList(keyword, page, size);

            return Ok(result);
        }

        [HttpGet("lookup")]
        [Authorize]
        public ActionResult<List<LookupResource>> GetLookup(string keyword)
        {
            var result = customerCategoryService.GetLookup(keyword);

            return Ok(result);
        }

        [HttpGet("{id}")]
        [Authorize]
        public ActionResult<CustomerCategoryResource> Get(Guid id)
        {
            var resource = customerCategoryService.Get(id);

            return Ok(resource);
        }

        [HttpGet("lookup/{id}")]
        [Authorize]
        public ActionResult<LookupResource> GetSingleLookup(Guid? id)
        {
            var result = customerCategoryService.GetSingleLookup(id);

            return Ok(result);
        }

        [HttpPost]
        [Authorize]
        public ActionResult<ScalarResource> Create([FromBody] CustomerCategoryResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.CustomerCategoryCreate);

            var result = customerCategoryService.Create(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpPut("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> Update(Guid id, [FromBody] CustomerCategoryResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.CustomerCategoryUpdate);

            resource.Id = id;
            var result = customerCategoryService.Update(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpPatch("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> PatchUpdate(Guid id, [FromBody] JsonElement patchResource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.CustomerCategoryUpdate);

            var resource = customerCategoryService.Get(id);
            var json = patchResource.GetRawText();
            JsonConvert.PopulateObject(json, resource);
            resource.Id = id;
            var result = customerCategoryService.Update(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpDelete("{id}")]
        [Authorize]
        public ActionResult Delete(Guid id)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.CustomerCategoryDelete);

            customerCategoryService.Delete(id, this.GetUserId());

            return NoContent();
        }
    }
}
