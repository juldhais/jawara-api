﻿using Jawara.Enums;
using Jawara.Helpers;
using Jawara.Resources;
using Jawara.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Jawara.API.Controllers
{
    [Route("stock-order")]
    public class StockOrderController : ControllerBase
    {
        private readonly StockOrderService orderService;
        private readonly RoleService roleService;

        public StockOrderController(StockOrderService orderService,
                                      RoleService roleService)
        {
            this.orderService = orderService;
            this.roleService = roleService;
        }

        [HttpGet]
        [Authorize]
        public ActionResult<PagingResult<StockOrderResource>> GetList(string keyword, string status, Guid? destinationId, Guid? sourceId, DateTime? startDate, DateTime? endDate, int page, int size)
        {
            var result = orderService.GetList(keyword, status, destinationId, sourceId, startDate, endDate, page, size);

            return Ok(result);
        }

        [HttpGet("lookup")]
        [Authorize]
        public ActionResult<List<LookupResource>> GetLookup(string keyword, string status, Guid? sourceId, Guid? destinationId)
        {
            var result = orderService.GetLookup(keyword, status, sourceId, destinationId);

            return Ok(result);
        }

        [HttpGet("{id}/delivery")]
        [Authorize]
        public ActionResult<PagingResult<StockDeliveryResource>> GetListDelivery(Guid? id, int page, int size)
        {
            var result = orderService.GetListDelivery(id, page, size);

            return Ok(result);
        }

        [HttpGet("{id}/receipt")]
        [Authorize]
        public ActionResult<PagingResult<StockReceiptResource>> GetListReceipt(Guid? id, int page, int size)
        {
            var result = orderService.GetListReceipt(id, page, size);

            return Ok(result);
        }

        [HttpGet("{id}")]
        [Authorize]
        public ActionResult<StockOrderResource> Get(Guid id)
        {
            var resource = orderService.Get(id);

            return Ok(resource);
        }

        [HttpGet("lookup/{id}")]
        [Authorize]
        public ActionResult<LookupResource> GetSingleLookup(Guid? id)
        {
            var result = orderService.GetSingleLookup(id);

            return Ok(result);
        }

        [HttpPost]
        [Authorize]
        public ActionResult<ScalarResource> Create([FromBody] StockOrderResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.StockOrderCreate);

            var result = orderService.Create(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpPut("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> Update(Guid id, [FromBody] StockOrderResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.StockOrderUpdate);

            resource.Id = id;
            var result = orderService.Update(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpDelete("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> Delete(Guid id)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.StockOrderDelete);

            orderService.Delete(id, this.GetUserId());

            return NoContent();
        }

        [HttpPost("open")]
        [Authorize]
        public ActionResult Open([FromBody] ScalarResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.StockOrderOpen);

            orderService.Open(resource.Value.ToGuid(), this.GetUserId());

            return NoContent();
        }

        [HttpPost("close")]
        [Authorize]
        public ActionResult Close([FromBody] ScalarResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.StockOrderClose);

            orderService.Close(resource.Value.ToGuid(), this.GetUserId());

            return NoContent();
        }
    }
}
