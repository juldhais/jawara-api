﻿using Jawara.Enums;
using Jawara.Helpers;
using Jawara.Resources;
using Jawara.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Jawara.API.Controllers
{
    [Route("sales-payment")]
    public class SalesPaymentController : ControllerBase
    {
        private readonly SalesPaymentService paymentService;
        private readonly RoleService roleService;

        public SalesPaymentController(SalesPaymentService paymentService,
                                      RoleService roleService)
        {
            this.paymentService = paymentService;
            this.roleService = roleService;
        }

        [HttpGet]
        [Authorize]
        public ActionResult<PagingResult<SalesPaymentResource>> GetList(string keyword, Guid? customerId, Guid? locationId, DateTime? startDate, DateTime? endDate, int page, int size)
        {
            var result = paymentService.GetList(keyword, customerId, locationId, startDate, endDate, page, size);

            return Ok(result);
        }

        [HttpGet("{id}")]
        [Authorize]
        public ActionResult<SalesPaymentResource> Get(Guid id)
        {
            var resource = paymentService.Get(id);

            return Ok(resource);
        }

        [HttpPost]
        [Authorize]
        public ActionResult<ScalarResource> Create([FromBody] SalesPaymentResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.SalesPaymentCreate);

            var result = paymentService.Create(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpPut("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> Update(Guid id, [FromBody] SalesPaymentResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.SalesPaymentUpdate);

            resource.Id = id;
            var result = paymentService.Update(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpDelete("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> Delete(Guid id)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.SalesPaymentDelete);

            paymentService.Delete(id, this.GetUserId());

            return NoContent();
        }
    }
}
