﻿using Jawara.Enums;
using Jawara.Helpers;
using Jawara.Resources;
using Jawara.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Jawara.API.Controllers
{
    [Route("manufacturing-order")]
    public class ManufacturingOrderController : ControllerBase
    {
        private readonly ManufacturingOrderService orderService;
        private readonly RoleService roleService;

        public ManufacturingOrderController(ManufacturingOrderService orderService,
                                      RoleService roleService)
        {
            this.orderService = orderService;
            this.roleService = roleService;
        }

        [HttpGet]
        [Authorize]
        public ActionResult<PagingResult<ManufacturingOrderResource>> GetList(
            string keyword,
            string status,
            Guid? sectionId,
            Guid? bomId,
            Guid? parentId,
            Guid? locationId,
            DateTime? startDate,
            DateTime? endDate,
            int page,
            int size)
        {
            var result = orderService.GetList(
                keyword: keyword,
                status: status,
                sectionId: sectionId,
                bomId: bomId,
                parentId: parentId,
                locationId: locationId,
                startDate: startDate,
                endDate: endDate,
                page: page,
                size: size);

            return Ok(result);
        }

        [HttpGet("lookup")]
        [Authorize]
        public ActionResult<List<LookupResource>> GetLookup(
            string keyword,
            string status,
            Guid? sectionId,
            Guid? bomId,
            Guid? parentId,
            Guid? locationId)
        {
            var result = orderService.GetLookup(
                keyword: keyword,
                status: status,
                sectionId: sectionId,
                bomId: bomId,
                parentId: parentId,
                locationId: locationId);

            return Ok(result);
        }

        [HttpGet("{id}")]
        [Authorize]
        public ActionResult<ManufacturingOrderResource> Get(Guid id)
        {
            var resource = orderService.Get(id);

            return Ok(resource);
        }

        [HttpGet("lookup/{id}")]
        [Authorize]
        public ActionResult<LookupResource> GetSingleLookup(Guid id)
        {
            var resource = orderService.GetSingleLookup(id);

            return Ok(resource);
        }

        [HttpPost]
        [Authorize]
        public ActionResult<ScalarResource> Create([FromBody] ManufacturingOrderResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.ManufacturingOrderCreate);

            var result = orderService.Create(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpPut("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> Update(Guid id, [FromBody] ManufacturingOrderResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.ManufacturingOrderUpdate);

            resource.Id = id;
            var result = orderService.Update(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpDelete("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> Delete(Guid id)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.ManufacturingOrderDelete);

            orderService.Delete(id, this.GetUserId());

            return NoContent();
        }

        [HttpPost("open")]
        [Authorize]
        public ActionResult Open([FromBody] ScalarResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.ManufacturingOrderOpen);

            orderService.Open(resource.Value.ToGuid(), this.GetUserId());

            return Ok();
        }

        [HttpPost("close")]
        [Authorize]
        public ActionResult Close([FromBody] ScalarResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.ManufacturingOrderClose);

            orderService.Close(resource.Value.ToGuid(), this.GetUserId());

            return Ok();
        }
    }
}
