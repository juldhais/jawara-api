﻿using Jawara.Enums;
using Jawara.Helpers;
using Jawara.Resources;
using Jawara.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Jawara.API.Controllers
{
    [Route("stock-delivery")]
    public class StockDeliveryController : ControllerBase
    {
        private readonly StockDeliveryService deliveryService;
        private readonly RoleService roleService;

        public StockDeliveryController(StockDeliveryService deliveryService,
                                      RoleService roleService)
        {
            this.deliveryService = deliveryService;
            this.roleService = roleService;
        }

        [HttpGet]
        [Authorize]
        public ActionResult<PagingResult<StockDeliveryResource>> GetList(string keyword, string status, Guid? destinationId, Guid? sourceId, DateTime? startDate, DateTime? endDate, int page, int size)
        {
            var result = deliveryService.GetList(keyword, status, destinationId, sourceId, startDate, endDate, page, size);

            return Ok(result);
        }

        [HttpGet("{id}/receipt")]
        [Authorize]
        public ActionResult<PagingResult<StockReceiptResource>> GetListReceipt(Guid? id, int page, int size)
        {
            var result = deliveryService.GetListReceipt(id, page, size);

            return Ok(result);
        }

        [HttpGet("{id}")]
        [Authorize]
        public ActionResult<StockDeliveryResource> Get(Guid id)
        {
            var resource = deliveryService.Get(id);

            return Ok(resource);
        }

        [HttpPost]
        [Authorize]
        public ActionResult<ScalarResource> Create([FromBody] StockDeliveryResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.StockDeliveryCreate);

            var result = deliveryService.Create(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpPut("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> Update(Guid id, [FromBody] StockDeliveryResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.StockDeliveryUpdate);

            resource.Id = id;
            var result = deliveryService.Update(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpDelete("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> Delete(Guid id)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.StockDeliveryDelete);

            deliveryService.Delete(id, this.GetUserId());

            return NoContent();
        }

        [HttpPost("open")]
        [Authorize]
        public ActionResult Open([FromBody] ScalarResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.StockDeliveryOpen);

            deliveryService.Open(resource.Value.ToGuid(), this.GetUserId());

            return NoContent();
        }

        [HttpPost("close")]
        [Authorize]
        public ActionResult Close([FromBody] ScalarResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.StockDeliveryClose);

            deliveryService.Close(resource.Value.ToGuid(), this.GetUserId());

            return NoContent();
        }
    }
}
