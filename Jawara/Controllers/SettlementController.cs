﻿using Jawara.Enums;
using Jawara.Helpers;
using Jawara.Resources;
using Jawara.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Text.Json;

namespace Jawara.API.Controllers
{
    [Route("settlement")]
    public class SettlementController : ControllerBase
    {
        private readonly SettlementService settlementService;
        private readonly RoleService roleService;

        public SettlementController(SettlementService settlementService,
                                  RoleService roleService)
        {
            this.settlementService = settlementService;
            this.roleService = roleService;
        }

        [HttpGet]
        [Authorize]
        public ActionResult<PagingResult<SettlementResource>> GetList(string keyword, Guid? locationId, Guid? userId, DateTime? startDate, DateTime? endDate, int page, int size)
        {
            var result = settlementService.GetList(keyword, locationId, userId, startDate, endDate, page, size);

            return Ok(result);
        }

        [HttpGet("{id}")]
        [Authorize]
        public ActionResult<SettlementResource> Get(Guid id)
        {
            var resource = settlementService.Get(id);

            return Ok(resource);
        }

        [HttpPost]
        [Authorize]
        public ActionResult<ScalarResource> Create([FromBody] SettlementResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.SettlementCreate);

            var result = settlementService.Create(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpPut("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> Update(Guid id, [FromBody] SettlementResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.SettlementUpdate);

            resource.Id = id;
            var result = settlementService.Update(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpPatch("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> PatchUpdate(Guid id, [FromBody] JsonElement patchResource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.SettlementUpdate);

            var resource = settlementService.Get(id);
            var json = patchResource.GetRawText();
            JsonConvert.PopulateObject(json, resource);
            resource.Id = id;
            var result = settlementService.Update(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpDelete("{id}")]
        [Authorize]
        public ActionResult Delete(Guid id)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.SettlementDelete);

            settlementService.Delete(id, this.GetUserId());

            return NoContent();
        }
    }
}
