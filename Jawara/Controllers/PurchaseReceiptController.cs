﻿using Jawara.Enums;
using Jawara.Helpers;
using Jawara.Resources;
using Jawara.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Jawara.API.Controllers
{
    [Route("purchase-receipt")]
    public class PurchaseReceiptController : ControllerBase
    {
        private readonly PurchaseReceiptService receiptService;
        private readonly RoleService roleService;

        public PurchaseReceiptController(PurchaseReceiptService receiptService,
                                      RoleService roleService)
        {
            this.receiptService = receiptService;
            this.roleService = roleService;
        }

        [HttpGet]
        [Authorize]
        public ActionResult<PagingResult<PurchaseReceiptResource>> GetList(string keyword, string status, Guid? supplierId, Guid? locationId, DateTime? startDate, DateTime? endDate, int page, int size)
        {
            var result = receiptService.GetList(keyword, status, supplierId, locationId, startDate, endDate, page, size);

            return Ok(result);
        }

        [HttpGet("{id}")]
        [Authorize]
        public ActionResult<PurchaseReceiptResource> Get(Guid id)
        {
            var resource = receiptService.Get(id);

            return Ok(resource);
        }

        [HttpPost]
        [Authorize]
        public ActionResult<ScalarResource> Create([FromBody] PurchaseReceiptResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.PurchaseReceiptCreate);

            var result = receiptService.Create(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpPut("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> Update(Guid id, [FromBody] PurchaseReceiptResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.PurchaseReceiptUpdate);

            resource.Id = id;
            var result = receiptService.Update(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpDelete("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> Delete(Guid id)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.PurchaseReceiptDelete);

            receiptService.Delete(id, this.GetUserId());

            return NoContent();
        }
    }
}
