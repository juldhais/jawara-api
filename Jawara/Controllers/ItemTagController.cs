﻿using Jawara.Enums;
using Jawara.Helpers;
using Jawara.Resources;
using Jawara.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text.Json;

namespace Jawara.API.Controllers
{
    [Route("item-tag")]
    public class ItemTagController : ControllerBase
    {
        private readonly ItemTagService itemTagService;
        private readonly RoleService roleService;

        public ItemTagController(ItemTagService itemTagService,
                                      RoleService roleService)
        {
            this.itemTagService = itemTagService;
            this.roleService = roleService;
        }

        [HttpGet]
        [Authorize]
        public ActionResult<PagingResult<ItemTagResource>> GetList(string keyword, int page, int size)
        {
            var result = itemTagService.GetList(keyword, page, size);

            return Ok(result);
        }

        [HttpGet("lookup")]
        [Authorize]
        public ActionResult<List<LookupResource>> GetLookup(string keyword)
        {
            var result = itemTagService.GetLookup(keyword);

            return Ok(result);
        }

        [HttpGet("{id}")]
        [Authorize]
        public ActionResult<ItemTagResource> Get(Guid id)
        {
            var resource = itemTagService.Get(id);

            return Ok(resource);
        }

        [HttpGet("lookup/{id}")]
        [Authorize]
        public ActionResult<LookupResource> GetSingleLookup(Guid? id)
        {
            var result = itemTagService.GetSingleLookup(id);

            return Ok(result);
        }

        [HttpPost]
        [Authorize]
        public ActionResult<ScalarResource> Create([FromBody] ItemTagResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.ItemTagCreate);

            var result = itemTagService.Create(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpPut("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> Update(Guid id, [FromBody] ItemTagResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.ItemTagUpdate);

            resource.Id = id;
            var result = itemTagService.Update(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpPatch("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> PatchUpdate(Guid id, [FromBody] JsonElement patchResource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.ItemTagUpdate);

            var resource = itemTagService.Get(id);
            var json = patchResource.GetRawText();
            JsonConvert.PopulateObject(json, resource);
            resource.Id = id;
            var result = itemTagService.Update(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpDelete("{id}")]
        [Authorize]
        public ActionResult Delete(Guid id)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.ItemTagDelete);

            itemTagService.Delete(id, this.GetUserId());

            return NoContent();
        }
    }
}
