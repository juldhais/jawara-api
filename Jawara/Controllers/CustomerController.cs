﻿using Jawara.Enums;
using Jawara.Helpers;
using Jawara.Resources;
using Jawara.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text.Json;

namespace Jawara.API.Controllers
{
    [Route("customer")]
    public class CustomerController : ControllerBase
    {
        private readonly CustomerService customerService;
        private readonly RoleService roleService;
        private readonly IWebHostEnvironment webhostEnvironment;

        public CustomerController(CustomerService customerService,
                                  RoleService roleService,
                                  IWebHostEnvironment webhostEnvironment)
        {
            this.customerService = customerService;
            this.roleService = roleService;
            this.webhostEnvironment = webhostEnvironment;
        }

        [HttpGet]
        [Authorize]
        public ActionResult<PagingResult<CustomerResource>> GetList(string keyword, int page, int size)
        {
            var result = customerService.GetList(keyword, page, size);

            return Ok(result);
        }

        [HttpGet("lookup")]
        [Authorize]
        public ActionResult<List<LookupResource>> GetLookup(string keyword)
        {
            var result = customerService.GetLookup(keyword);

            return Ok(result);
        }

        [HttpGet("{id}")]
        [Authorize]
        public ActionResult<CustomerResource> Get(Guid id)
        {
            var resource = customerService.Get(id);

            return Ok(resource);
        }

        [HttpGet("lookup/{id}")]
        [Authorize]
        public ActionResult<LookupResource> GetSingleLookup(Guid? id)
        {
            var result = customerService.GetSingleLookup(id);

            return Ok(result);
        }

        [HttpPost]
        [Authorize]
        public ActionResult<ScalarResource> Create([FromBody] CustomerResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.CustomerCreate);

            var result = customerService.Create(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpPut("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> Update(Guid id, [FromBody] CustomerResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.CustomerUpdate);

            resource.Id = id;
            var result = customerService.Update(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpPatch("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> PatchUpdate(Guid id, [FromBody] JsonElement patchResource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.CustomerUpdate);

            var resource = customerService.Get(id);
            var json = patchResource.GetRawText();
            JsonConvert.PopulateObject(json, resource);
            resource.Id = id;
            var result = customerService.Update(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpDelete("{id}")]
        [Authorize]
        public ActionResult Delete(Guid id)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.CustomerDelete);

            customerService.Delete(id, this.GetUserId());

            return NoContent();
        }

        [HttpPost("import")]
        [Authorize]
        [RequestSizeLimit(5242880)]
        public IActionResult Import()
        {
            try
            {
                roleService.CheckPrivilege(this.GetRoleId(), Privilege.CustomerCreate);

                if (Request.Form.Files == null || Request.Form.Files.Count == 0)
                    return NoContent();

                var file = Request.Form.Files[0];

                if (!file.FileName.EndsWith(".xlsx"))
                    throw new BadRequestException("File must be excel document (xlsx).");

                var filePath = this.SaveFile(webhostEnvironment, file);
                var list = ExcelHelper.Import<CustomerImportResource>(filePath);

                if (list.Count == 0)
                    throw new BadRequestException("Excel file does not contain acceptable data.");

                var result = customerService.Import(list, this.GetUserId());

                return Ok(result);
            }
            catch (Exception ex)
            {
                var result = new ImportResultResource();
                result.IsError = true;
                result.ErrorMessages.Add(ex.GetBaseException().Message);
                return Ok(result);
            }
        }
    }
}
