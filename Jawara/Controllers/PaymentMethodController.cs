﻿using Jawara.Enums;
using Jawara.Helpers;
using Jawara.Resources;
using Jawara.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text.Json;

namespace Jawara.API.Controllers
{
    [Route("payment-method")]
    public class PaymentMethodController : ControllerBase
    {
        private readonly PaymentMethodService paymentMethodService;
        private readonly RoleService roleService;

        public PaymentMethodController(PaymentMethodService unitService,
                                      RoleService roleService)
        {
            this.paymentMethodService = unitService;
            this.roleService = roleService;
        }

        [HttpGet]
        [Authorize]
        public ActionResult<PagingResult<PaymentMethodResource>> GetList(string keyword, int page, int size)
        {
            var result = paymentMethodService.GetList(keyword, page, size);

            return Ok(result);
        }

        [HttpGet("purchase")]
        [Authorize]
        public ActionResult<PagingResult<PaymentMethodResource>> GetListPurchase(string keyword, int page, int size)
        {
            var result = paymentMethodService.GetListPurchase(keyword, page, size);

            return Ok(result);
        }

        [HttpGet("sales")]
        [Authorize]
        public ActionResult<PagingResult<PaymentMethodResource>> GetListSales(string keyword, int page, int size)
        {
            var result = paymentMethodService.GetListSales(keyword, page, size);

            return Ok(result);
        }

        [HttpGet("lookup")]
        [Authorize]
        public ActionResult<List<LookupResource>> GetLookup(string keyword)
        {
            var result = paymentMethodService.GetLookup(keyword);

            return Ok(result);
        }

        [HttpGet("lookup/sales")]
        [Authorize]
        public ActionResult<List<LookupResource>> GetLookupSales(string keyword)
        {
            var result = paymentMethodService.GetLookupSales(keyword);

            return Ok(result);
        }

        [HttpGet("lookup/purchase")]
        [Authorize]
        public ActionResult<List<LookupResource>> GetLookupPurchase(string keyword)
        {
            var result = paymentMethodService.GetLookupPurchase(keyword);

            return Ok(result);
        }

        [HttpGet("{id}")]
        [Authorize]
        public ActionResult<PaymentMethodResource> Get(Guid id)
        {
            var resource = paymentMethodService.Get(id);

            return Ok(resource);
        }

        [HttpGet("lookup/{id}")]
        [Authorize]
        public ActionResult<LookupResource> GetSingleLookup(Guid? id)
        {
            var result = paymentMethodService.GetSingleLookup(id);

            return Ok(result);
        }

        [HttpPost]
        [Authorize]
        public ActionResult<ScalarResource> Create([FromBody] PaymentMethodResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.PaymentMethodCreate);

            var result = paymentMethodService.Create(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpPut("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> Update(Guid id, [FromBody] PaymentMethodResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.PaymentMethodUpdate);

            resource.Id = id;
            var result = paymentMethodService.Update(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpPatch("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> PatchUpdate(Guid id, [FromBody] JsonElement patchResource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.PaymentMethodUpdate);

            var resource = paymentMethodService.Get(id);
            var json = patchResource.GetRawText();
            JsonConvert.PopulateObject(json, resource);
            resource.Id = id;
            var result = paymentMethodService.Update(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpDelete("{id}")]
        [Authorize]
        public ActionResult Delete(Guid id)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.PaymentMethodDelete);

            paymentMethodService.Delete(id, this.GetUserId());

            return NoContent();
        }
    }
}
