﻿using Jawara.Enums;
using Jawara.Helpers;
using Jawara.Resources;
using Jawara.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Jawara.API.Controllers
{
    [Route("stock-adjustment")]
    public class StockAdjustmentController : ControllerBase
    {
        private readonly StockAdjustmentService adjustmentService;
        private readonly RoleService roleService;

        public StockAdjustmentController(StockAdjustmentService adjustmentService,
                                      RoleService roleService)
        {
            this.adjustmentService = adjustmentService;
            this.roleService = roleService;
        }

        [HttpGet]
        [Authorize]
        public ActionResult<PagingResult<StockAdjustmentResource>> GetList(string keyword, Guid? locationId, DateTime? startDate, DateTime? endDate, int page, int size)
        {
            var result = adjustmentService.GetList(keyword, locationId, startDate, endDate, page, size);

            return Ok(result);
        }

        [HttpGet("{id}")]
        [Authorize]
        public ActionResult<StockAdjustmentResource> Get(Guid id)
        {
            var resource = adjustmentService.Get(id);

            return Ok(resource);
        }

        [HttpPost]
        [Authorize]
        public ActionResult<ScalarResource> Create([FromBody] StockAdjustmentResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.StockAdjustmentCreate);

            var result = adjustmentService.Create(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpPut("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> Update(Guid id, [FromBody] StockAdjustmentResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.StockAdjustmentUpdate);

            resource.Id = id;
            var result = adjustmentService.Update(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpDelete("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> Delete(Guid id)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.StockAdjustmentDelete);

            adjustmentService.Delete(id, this.GetUserId());

            return NoContent();
        }
    }
}
