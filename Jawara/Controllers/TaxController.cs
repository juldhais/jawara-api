﻿using Jawara.Enums;
using Jawara.Helpers;
using Jawara.Resources;
using Jawara.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text.Json;

namespace Jawara.API.Controllers
{
    [Route("tax")]
    public class TaxController : ControllerBase
    {
        private readonly TaxService taxService;
        private readonly RoleService roleService;

        public TaxController(TaxService taxService,
                              RoleService roleService)
        {
            this.taxService = taxService;
            this.roleService = roleService;
        }

        [HttpGet]
        [Authorize]
        public ActionResult<PagingResult<TaxResource>> GetList(string keyword, string type, int page, int size)
        {
            var result = taxService.GetList(keyword, type, page, size);

            return Ok(result);
        }

        [HttpGet("lookup")]
        [Authorize]
        public ActionResult<List<LookupResource>> GetLookup(string keyword, string type)
        {
            var result = taxService.GetLookup(keyword, type);

            return Ok(result);
        }

        [HttpGet("{id}")]
        [Authorize]
        public ActionResult<TaxResource> Get(Guid id)
        {
            var resource = taxService.Get(id);

            return Ok(resource);
        }

        [HttpGet("lookup/{id}")]
        [Authorize]
        public ActionResult<LookupResource> GetSingleLookup(Guid? id)
        {
            var result = taxService.GetSingleLookup(id);

            return Ok(result);
        }

        [HttpPost]
        [Authorize]
        public ActionResult<ScalarResource> Create([FromBody] TaxResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.TaxCreate);

            var result = taxService.Create(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpPut("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> Update(Guid id, [FromBody] TaxResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.TaxUpdate);

            resource.Id = id;
            var result = taxService.Update(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpPatch("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> PatchUpdate(Guid id, [FromBody] JsonElement patchResource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.TaxUpdate);

            var resource = taxService.Get(id);
            var json = patchResource.GetRawText();
            JsonConvert.PopulateObject(json, resource);
            resource.Id = id;
            var result = taxService.Update(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpDelete("{id}")]
        [Authorize]
        public ActionResult Delete(Guid id)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.TaxDelete);

            taxService.Delete(id, this.GetUserId());

            return NoContent();
        }
    }
}
