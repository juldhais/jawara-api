﻿using Jawara.Enums;
using Jawara.Helpers;
using Jawara.Resources;
using Jawara.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text.Json;

namespace Jawara.API.Controllers
{
    [Route("stock-control-category")]
    public class StockControlCategoryController : ControllerBase
    {
        private readonly StockControlCategoryService categoryService;
        private readonly RoleService roleService;

        public StockControlCategoryController(StockControlCategoryService categoryService,
                                      RoleService roleService)
        {
            this.categoryService = categoryService;
            this.roleService = roleService;
        }

        [HttpGet]
        [Authorize]
        public ActionResult<PagingResult<StockControlCategoryResource>> GetList(string keyword, int page, int size)
        {
            var result = categoryService.GetList(keyword, page, size);

            return Ok(result);
        }

        [HttpGet("lookup")]
        [Authorize]
        public ActionResult<List<LookupResource>> GetLookup(string keyword)
        {
            var result = categoryService.GetLookup(keyword);

            return Ok(result);
        }

        [HttpGet("{id}")]
        [Authorize]
        public ActionResult<StockControlCategoryResource> Get(Guid id)
        {
            var resource = categoryService.Get(id);

            return Ok(resource);
        }

        [HttpGet("lookup/{id}")]
        [Authorize]
        public ActionResult<LookupResource> GetSingleLookup(Guid? id)
        {
            var result = categoryService.GetSingleLookup(id);

            return Ok(result);
        }

        [HttpPost]
        [Authorize]
        public ActionResult<ScalarResource> Create([FromBody] StockControlCategoryResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.StockControlCategoryCreate);

            var result = categoryService.Create(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpPut("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> Update(Guid id, [FromBody] StockControlCategoryResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.StockControlCategoryUpdate);

            resource.Id = id;
            var result = categoryService.Update(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpPatch("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> PatchUpdate(Guid id, [FromBody] JsonElement patchResource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.ItemUpdate);

            var resource = categoryService.Get(id);
            var json = patchResource.GetRawText();
            JsonConvert.PopulateObject(json, resource);
            resource.Id = id;
            var result = categoryService.Update(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpDelete("{id}")]
        [Authorize]
        public ActionResult Delete(Guid id)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.StockControlCategoryDelete);

            categoryService.Delete(id, this.GetUserId());

            return NoContent();
        }
    }
}
