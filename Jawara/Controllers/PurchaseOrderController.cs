﻿using Jawara.Enums;
using Jawara.Helpers;
using Jawara.Resources;
using Jawara.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Jawara.API.Controllers
{
    [Route("purchase-order")]
    public class PurchaseOrderController : ControllerBase
    {
        private readonly PurchaseOrderService orderService;
        private readonly PurchaseOrderPrintService printService;
        private readonly RoleService roleService;

        public PurchaseOrderController(
            PurchaseOrderService orderService,
            PurchaseOrderPrintService printService,
            RoleService roleService)
        {
            this.orderService = orderService;
            this.printService = printService;
            this.roleService = roleService;
        }

        [HttpGet]
        [Authorize]
        public ActionResult<PagingResult<PurchaseOrderResource>> GetList(string keyword, string status, Guid? supplierId, Guid? locationId, DateTime? startDate, DateTime? endDate, int page, int size)
        {
            var result = orderService.GetList(keyword, status, supplierId, locationId, startDate, endDate, page, size);

            return Ok(result);
        }

        [HttpGet("lookup")]
        [Authorize]
        public ActionResult<List<LookupResource>> GetLookup(string keyword, string status, Guid? supplierId, Guid? locationId)
        {
            var result = orderService.GetLookup(keyword, status, supplierId, locationId);

            return Ok(result);
        }

        [HttpGet("{id}/receipt")]
        [Authorize]
        public ActionResult<PagingResult<PurchaseReceiptService>> GetListReceipt(Guid? id, int page, int size)
        {
            var result = orderService.GetListReceipt(id, page, size);

            return Ok(result);
        }

        [HttpGet("{id}")]
        [Authorize]
        public ActionResult<PurchaseOrderResource> Get(Guid id)
        {
            var resource = orderService.Get(id);

            return Ok(resource);
        }

        [HttpGet("lookup/{id}")]
        [Authorize]
        public ActionResult<LookupResource> GetSingleLookup(Guid? id)
        {
            var result = orderService.GetSingleLookup(id);

            return Ok(result);
        }

        [HttpPost]
        [Authorize]
        public ActionResult<ScalarResource> Create([FromBody] PurchaseOrderResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.PurchaseOrderCreate);

            var result = orderService.Create(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpPut("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> Update(Guid id, [FromBody] PurchaseOrderResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.PurchaseOrderUpdate);

            resource.Id = id;
            var result = orderService.Update(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpDelete("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> Delete(Guid id)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.PurchaseOrderDelete);

            orderService.Delete(id, this.GetUserId());

            return NoContent();
        }

        [HttpPost("open")]
        [Authorize]
        public ActionResult Open([FromBody] ScalarResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.PurchaseOrderOpen);

            orderService.Open(resource.Value.ToGuid(), this.GetUserId());

            return Ok();
        }

        [HttpPost("close")]
        [Authorize]
        public ActionResult Close([FromBody] ScalarResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.PurchaseOrderClose);

            orderService.Close(resource.Value.ToGuid(), this.GetUserId());

            return Ok();
        }

        [HttpGet("print/{id}")]
        [Authorize]
        public ActionResult<ScalarResource> Print(Guid? id)
        {
            var result = printService.Print(id);

            return Ok(new ScalarResource(result));
        }
    }
}
