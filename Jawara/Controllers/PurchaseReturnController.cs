﻿using Jawara.Enums;
using Jawara.Helpers;
using Jawara.Resources;
using Jawara.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Jawara.API.Controllers
{
    [Route("purchase-return")]
    public class PurchaseReturnController : ControllerBase
    {
        private readonly PurchaseReturnService returnService;
        private readonly RoleService roleService;

        public PurchaseReturnController(PurchaseReturnService returnService,
                                      RoleService roleService)
        {
            this.returnService = returnService;
            this.roleService = roleService;
        }

        [HttpGet]
        [Authorize]
        public ActionResult<PagingResult<PurchaseReturnResource>> GetList(string keyword, string status, Guid? supplierId, Guid? locationId, DateTime? startDate, DateTime? endDate, int page, int size)
        {
            var result = returnService.GetList(keyword, status, supplierId, locationId, startDate, endDate, page, size);

            return Ok(result);
        }

        [HttpGet("{id}")]
        [Authorize]
        public ActionResult<PurchaseReturnResource> Get(Guid id)
        {
            var resource = returnService.Get(id);

            return Ok(resource);
        }

        [HttpPost]
        [Authorize]
        public ActionResult<ScalarResource> Create([FromBody] PurchaseReturnResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.PurchaseReturnCreate);

            var result = returnService.Create(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpPut("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> Update(Guid id, [FromBody] PurchaseReturnResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.PurchaseReturnUpdate);

            resource.Id = id;
            var result = returnService.Update(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpDelete("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> Delete(Guid id)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.PurchaseReturnDelete);

            returnService.Delete(id, this.GetUserId());

            return NoContent();
        }
    }
}
