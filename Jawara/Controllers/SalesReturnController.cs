﻿using Jawara.Enums;
using Jawara.Helpers;
using Jawara.Resources;
using Jawara.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Jawara.API.Controllers
{
    [Route("sales-return")]
    public class SalesReturnController : ControllerBase
    {
        private readonly SalesReturnService returnService;
        private readonly RoleService roleService;

        public SalesReturnController(SalesReturnService returnService,
                                      RoleService roleService)
        {
            this.returnService = returnService;
            this.roleService = roleService;
        }

        [HttpGet]
        [Authorize]
        public ActionResult<PagingResult<SalesReturnResource>> GetList(string keyword, string status, Guid? customerId, Guid? locationId, DateTime? startDate, DateTime? endDate, int page, int size)
        {
            var result = returnService.GetList(keyword, status, customerId, locationId, startDate, endDate, page, size);

            return Ok(result);
        }

        [HttpGet("{id}")]
        [Authorize]
        public ActionResult<SalesReturnResource> Get(Guid id)
        {
            var resource = returnService.Get(id);

            return Ok(resource);
        }

        [HttpPost]
        [Authorize]
        public ActionResult<ScalarResource> Create([FromBody] SalesReturnResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.SalesReturnCreate);

            var result = returnService.Create(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpPut("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> Update(Guid id, [FromBody] SalesReturnResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.SalesReturnUpdate);

            resource.Id = id;
            var result = returnService.Update(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpDelete("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> Delete(Guid id)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.SalesReturnDelete);

            returnService.Delete(id, this.GetUserId());

            return NoContent();
        }
    }
}
