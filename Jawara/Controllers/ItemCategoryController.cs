﻿using Jawara.Enums;
using Jawara.Helpers;
using Jawara.Resources;
using Jawara.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text.Json;

namespace Jawara.API.Controllers
{
    [Route("item-category")]
    public class ItemCategoryController : ControllerBase
    {
        private readonly ItemCategoryService itemCategoryService;
        private readonly RoleService roleService;

        public ItemCategoryController(ItemCategoryService itemCategoryService,
                                      RoleService roleService)
        {
            this.itemCategoryService = itemCategoryService;
            this.roleService = roleService;
        }

        [HttpGet]
        [Authorize]
        public ActionResult<PagingResult<ItemCategoryResource>> GetList(string keyword, int page, int size)
        {
            var result = itemCategoryService.GetList(keyword, page, size);

            return Ok(result);
        }

        [HttpGet("lookup")]
        [Authorize]
        public ActionResult<List<LookupResource>> GetLookup(string keyword)
        {
            var result = itemCategoryService.GetLookup(keyword);

            return Ok(result);
        }

        [HttpGet("{id}")]
        [Authorize]
        public ActionResult<ItemCategoryResource> Get(Guid id)
        {
            var resource = itemCategoryService.Get(id);

            return Ok(resource);
        }

        [HttpGet("lookup/{id}")]
        [Authorize]
        public ActionResult<LookupResource> GetSingleLookup(Guid? id)
        {
            var result = itemCategoryService.GetSingleLookup(id);

            return Ok(result);
        }

        [HttpPost]
        [Authorize]
        public ActionResult<ScalarResource> Create([FromBody] ItemCategoryResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.ItemCategoryCreate);

            var result = itemCategoryService.Create(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpPut("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> Update(Guid id, [FromBody] ItemCategoryResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.ItemCategoryUpdate);

            resource.Id = id;
            var result = itemCategoryService.Update(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpPatch("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> PatchUpdate(Guid id, [FromBody] JsonElement patchResource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.ItemCategoryUpdate);

            var resource = itemCategoryService.Get(id);
            var json = patchResource.GetRawText();
            JsonConvert.PopulateObject(json, resource);
            resource.Id = id;
            var result = itemCategoryService.Update(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpDelete("{id}")]
        [Authorize]
        public ActionResult Delete(Guid id)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.ItemCategoryDelete);

            itemCategoryService.Delete(id, this.GetUserId());

            return NoContent();
        }
    }
}
