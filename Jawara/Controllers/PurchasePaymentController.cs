﻿using Jawara.Enums;
using Jawara.Helpers;
using Jawara.Resources;
using Jawara.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Jawara.API.Controllers
{
    [Route("purchase-payment")]
    public class PurchasePaymentController : ControllerBase
    {
        private readonly PurchasePaymentService paymentService;
        private readonly RoleService roleService;

        public PurchasePaymentController(PurchasePaymentService paymentService,
                                      RoleService roleService)
        {
            this.paymentService = paymentService;
            this.roleService = roleService;
        }

        [HttpGet]
        [Authorize]
        public ActionResult<PagingResult<PurchasePaymentResource>> GetList(string keyword, Guid? supplierId, Guid? locationId, DateTime? startDate, DateTime? endDate, int page, int size)
        {
            var result = paymentService.GetList(keyword, supplierId, locationId, startDate, endDate, page, size);

            return Ok(result);
        }


        [HttpGet("{id}")]
        [Authorize]
        public ActionResult<PurchasePaymentResource> Get(Guid id)
        {
            var resource = paymentService.Get(id);

            return Ok(resource);
        }

        [HttpPost]
        [Authorize]
        public ActionResult<ScalarResource> Create([FromBody] PurchasePaymentResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.PurchasePaymentCreate);

            var result = paymentService.Create(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpPut("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> Update(Guid id, [FromBody] PurchasePaymentResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.PurchasePaymentUpdate);

            resource.Id = id;
            var result = paymentService.Update(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpDelete("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> Delete(Guid id)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.PurchasePaymentDelete);

            paymentService.Delete(id, this.GetUserId());

            return NoContent();
        }
    }
}
