﻿using Jawara.Enums;
using Jawara.Helpers;
using Jawara.Resources;
using Jawara.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Text.Json;

namespace Jawara.API.Controllers
{
    [Route("price-control")]
    public class PriceControlController : ControllerBase
    {
        private readonly PriceControlService priceControlService;
        private readonly RoleService roleService;
        private readonly IWebHostEnvironment webhostEnvironment;

        public PriceControlController(PriceControlService priceControlService,
                                      RoleService roleService,
                                      IWebHostEnvironment webhostEnvironment)
        {
            this.priceControlService = priceControlService;
            this.roleService = roleService;
            this.webhostEnvironment = webhostEnvironment;
        }

        [HttpGet]
        [Authorize]
        public ActionResult<PagingResult<PriceControlResource>> GetList(Guid? priceControlCategoryId, Guid? itemId, int page, int size)
        {
            var result = priceControlService.GetList(priceControlCategoryId, itemId, page, size);

            return Ok(result);
        }

        [HttpGet("{id}")]
        [Authorize]
        public ActionResult<PriceControlResource> Get(Guid id)
        {
            var resource = priceControlService.Get(id);

            return Ok(resource);
        }

        [HttpPost]
        [Authorize]
        public ActionResult<ScalarResource> Create([FromBody] PriceControlResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.PriceControlCreate);

            var result = priceControlService.Create(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpPut("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> Update(Guid id, [FromBody] PriceControlResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.PriceControlUpdate);

            resource.Id = id;
            var result = priceControlService.Update(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpPatch("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> PatchUpdate(Guid id, [FromBody] JsonElement patchResource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.PriceControlUpdate);

            var resource = priceControlService.Get(id);
            var json = patchResource.GetRawText();
            JsonConvert.PopulateObject(json, resource);
            resource.Id = id;
            var result = priceControlService.Update(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpDelete("{id}")]
        [Authorize]
        public ActionResult Delete(Guid id)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.PriceControlDelete);

            priceControlService.Delete(id, this.GetUserId());

            return NoContent();
        }

        [HttpPost("batch")]
        [Authorize]
        public ActionResult BatchUpdate([FromBody] BatchUpdatePriceControlResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.PriceControlUpdate);

            priceControlService.BatchUpdate(resource, this.GetUserId());

            return Ok();
        }

        [HttpPost("import")]
        [Authorize]
        [RequestSizeLimit(5242880)]
        public IActionResult Import()
        {
            try
            {
                roleService.CheckPrivilege(this.GetRoleId(), Privilege.PriceControlCreate);

                if (Request.Form.Files == null || Request.Form.Files.Count == 0)
                    return NoContent();

                var file = Request.Form.Files[0];

                if (!file.FileName.EndsWith(".xlsx"))
                    throw new BadRequestException("File must be excel document (xlsx).");

                var filePath = this.SaveFile(webhostEnvironment, file);
                var list = ExcelHelper.Import<PriceControlImportResource>(filePath);

                if (list.Count == 0)
                    throw new BadRequestException("Excel file does not contain acceptable data.");

                var result = priceControlService.Import(list, this.GetUserId());

                return Ok(result);
            }
            catch (Exception ex)
            {
                var result = new ImportResultResource();
                result.IsError = true;
                result.ErrorMessages.Add(ex.GetBaseException().Message);
                return Ok(result);
            }
        }
    }
}
