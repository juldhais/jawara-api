﻿using Jawara.Enums;
using Jawara.Helpers;
using Jawara.Resources;
using Jawara.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text.Json;

namespace Jawara.API.Controllers
{
    [Route("bill-of-material")]
    public class BillOfMaterialController : ControllerBase
    {
        private readonly BillOfMaterialService bomService;
        private readonly RoleService roleService;

        public BillOfMaterialController(BillOfMaterialService bomService,
                                  RoleService roleService)
        {
            this.bomService = bomService;
            this.roleService = roleService;
        }

        [HttpGet]
        [Authorize]
        public ActionResult<PagingResult<BillOfMaterialResource>> GetList(string keyword, int page, int size)
        {
            var result = bomService.GetList(keyword, page, size);

            return Ok(result);
        }

        [HttpGet("lookup")]
        [Authorize]
        public ActionResult<List<LookupResource>> GetLookup(string keyword)
        {
            var result = bomService.GetLookup(keyword);

            return Ok(result);
        }

        [HttpGet("{id}")]
        [Authorize]
        public ActionResult<BillOfMaterialResource> Get(Guid id)
        {
            var resource = bomService.Get(id);

            return Ok(resource);
        }

        [HttpGet("lookup/{id}")]
        [Authorize]
        public ActionResult<LookupResource> GetSingleLookup(Guid? id)
        {
            var result = bomService.GetSingleLookup(id);

            return Ok(result);
        }

        [HttpPost]
        [Authorize]
        public ActionResult<ScalarResource> Create([FromBody] BillOfMaterialResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.BillOfMaterialCreate);

            var result = bomService.Create(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpPut("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> Update(Guid id, [FromBody] BillOfMaterialResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.BillOfMaterialUpdate);

            resource.Id = id;
            var result = bomService.Update(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpPatch("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> PatchUpdate(Guid id, [FromBody] JsonElement patchResource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.BillOfMaterialUpdate);

            var resource = bomService.Get(id);
            var json = patchResource.GetRawText();
            JsonConvert.PopulateObject(json, resource);
            resource.Id = id;
            var result = bomService.Update(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpDelete("{id}")]
        [Authorize]
        public ActionResult Delete(Guid id)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.BillOfMaterialDelete);

            bomService.Delete(id, this.GetUserId());

            return NoContent();
        }
    }
}
