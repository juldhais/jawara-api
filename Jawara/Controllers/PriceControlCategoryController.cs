﻿using Jawara.Enums;
using Jawara.Helpers;
using Jawara.Resources;
using Jawara.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text.Json;

namespace Jawara.API.Controllers
{
    [Route("price-control-category")]
    public class PriceControlCategoryController : ControllerBase
    {
        private readonly PriceControlCategoryService priceControlCategoryService;
        private readonly RoleService roleService;

        public PriceControlCategoryController(PriceControlCategoryService priceControlCategoryService,
                                      RoleService roleService)
        {
            this.priceControlCategoryService = priceControlCategoryService;
            this.roleService = roleService;
        }

        [HttpGet]
        [Authorize]
        public ActionResult<PagingResult<PriceControlCategoryResource>> GetList(string keyword, int page, int size)
        {
            var result = priceControlCategoryService.GetList(keyword, page, size);

            return Ok(result);
        }

        [HttpGet("lookup")]
        [Authorize]
        public ActionResult<List<LookupResource>> GetLookup(string keyword)
        {
            var result = priceControlCategoryService.GetLookup(keyword);

            return Ok(result);
        }

        [HttpGet("{id}")]
        [Authorize]
        public ActionResult<PriceControlCategoryResource> Get(Guid id)
        {
            var resource = priceControlCategoryService.Get(id);

            return Ok(resource);
        }

        [HttpGet("lookup/{id}")]
        [Authorize]
        public ActionResult<LookupResource> GetSingleLookup(Guid? id)
        {
            var result = priceControlCategoryService.GetSingleLookup(id);

            return Ok(result);
        }

        [HttpPost]
        [Authorize]
        public ActionResult<ScalarResource> Create([FromBody] PriceControlCategoryResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.PriceControlCategoryCreate);

            var result = priceControlCategoryService.Create(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpPut("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> Update(Guid id, [FromBody] PriceControlCategoryResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.PriceControlCategoryUpdate);

            resource.Id = id;
            var result = priceControlCategoryService.Update(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpPatch("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> PatchUpdate(Guid id, [FromBody] JsonElement patchResource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.PriceControlCategoryUpdate);

            var resource = priceControlCategoryService.Get(id);
            var json = patchResource.GetRawText();
            JsonConvert.PopulateObject(json, resource);
            resource.Id = id;
            var result = priceControlCategoryService.Update(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpDelete("{id}")]
        [Authorize]
        public ActionResult Delete(Guid id)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.PriceControlCategoryDelete);

            priceControlCategoryService.Delete(id, this.GetUserId());

            return NoContent();
        }
    }
}
