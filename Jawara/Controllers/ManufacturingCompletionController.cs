﻿using Jawara.Enums;
using Jawara.Helpers;
using Jawara.Resources;
using Jawara.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Jawara.API.Controllers
{
    [Route("manufacturing-completion")]
    public class ManufacturingCompletionController : ControllerBase
    {
        private readonly ManufacturingCompletionService orderService;
        private readonly RoleService roleService;

        public ManufacturingCompletionController(ManufacturingCompletionService orderService,
                                      RoleService roleService)
        {
            this.orderService = orderService;
            this.roleService = roleService;
        }

        [HttpGet]
        [Authorize]
        public ActionResult<PagingResult<ManufacturingCompletionResource>> GetList(
            string keyword,
            Guid? sectionId,
            Guid? bomId,
            Guid? parentId,
            Guid? locationId,
            DateTime? startDate,
            DateTime? endDate,
            int page,
            int size)
        {
            var result = orderService.GetList(
                keyword: keyword,
                sectionId: sectionId,
                bomId: bomId,
                parentId: parentId,
                locationId: locationId,
                startDate: startDate,
                endDate: endDate,
                page: page,
                size: size);

            return Ok(result);
        }

        [HttpGet("{id}")]
        [Authorize]
        public ActionResult<ManufacturingCompletionResource> Get(Guid id)
        {
            var resource = orderService.Get(id);

            return Ok(resource);
        }

        [HttpPost]
        [Authorize]
        public ActionResult<ScalarResource> Create([FromBody] ManufacturingCompletionResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.ManufacturingCompletionCreate);

            var result = orderService.Create(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpPut("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> Update(Guid id, [FromBody] ManufacturingCompletionResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.ManufacturingCompletionUpdate);

            resource.Id = id;
            var result = orderService.Update(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpDelete("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> Delete(Guid id)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.ManufacturingCompletionDelete);

            orderService.Delete(id, this.GetUserId());

            return NoContent();
        }
    }
}
