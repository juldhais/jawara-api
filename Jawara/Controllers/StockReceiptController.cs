﻿using Jawara.Enums;
using Jawara.Helpers;
using Jawara.Resources;
using Jawara.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Jawara.API.Controllers
{
    [Route("stock-receipt")]
    public class StockReceiptController : ControllerBase
    {
        private readonly StockReceiptService receiptService;
        private readonly RoleService roleService;

        public StockReceiptController(StockReceiptService receiptService,
                                      RoleService roleService)
        {
            this.receiptService = receiptService;
            this.roleService = roleService;
        }

        [HttpGet]
        [Authorize]
        public ActionResult<PagingResult<StockReceiptResource>> GetList(string keyword, Guid? destinationId, Guid? sourceId, DateTime? startDate, DateTime? endDate, int page, int size)
        {
            var result = receiptService.GetList(keyword, destinationId, sourceId, startDate, endDate, page, size);

            return Ok(result);
        }

        [HttpGet("{id}")]
        [Authorize]
        public ActionResult<StockReceiptResource> Get(Guid id)
        {
            var resource = receiptService.Get(id);

            return Ok(resource);
        }

        [HttpPost]
        [Authorize]
        public ActionResult<ScalarResource> Create([FromBody] StockReceiptResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.StockReceiptCreate);

            var result = receiptService.Create(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpPut("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> Update(Guid id, [FromBody] StockReceiptResource resource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.StockReceiptUpdate);

            resource.Id = id;
            var result = receiptService.Update(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpDelete("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> Delete(Guid id)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.StockReceiptDelete);

            receiptService.Delete(id, this.GetUserId());

            return NoContent();
        }
    }
}
