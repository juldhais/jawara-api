﻿using Jawara.Enums;
using Jawara.Helpers;
using Jawara.Resources;
using Jawara.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Jawara.API.Controllers
{
    [Route("sales-report")]
    public class SalesReportController : ControllerBase
    {
        private readonly SalesReportService reportService;
        private readonly RoleService roleService;

        public SalesReportController(SalesReportService reportService,
                                     RoleService roleService)
        {
            this.reportService = reportService;
            this.roleService = roleService;
        }

        [HttpPost]
        [Authorize]
        public ActionResult<SalesReportResource> GetReport([FromBody] SalesReportParam param)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.SalesReport);

            var result = reportService.GetReport(param);

            return Ok(result);
        }
    }
}
