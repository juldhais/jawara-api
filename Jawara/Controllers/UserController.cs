﻿using Jawara.Enums;
using Jawara.Helpers;
using Jawara.Resources;
using Jawara.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Text.Json;

namespace UNICA.API.Controllers
{
    [Route("user")]
    public class UserController : ControllerBase
    {
        private readonly IConfiguration configuration;
        private readonly UserService userService;
        private readonly RoleService roleService;

        public UserController(IConfiguration configuration,
                              UserService userService,
                              RoleService roleService)
        {
            this.configuration = configuration;
            this.userService = userService;
            this.roleService = roleService;
        }

        [HttpGet("{id}")]
        [Authorize]
        public ActionResult<UserResource> Get(Guid? id)
        {
            var userResource = userService.Get(id);

            return Ok(userResource);
        }

        [HttpGet("lookup/{id}")]
        [Authorize]
        public ActionResult<LookupResource> GetSingleLookup(Guid? id)
        {
            var result = userService.GetSingleLookup(id);

            return Ok(result);
        }

        [HttpGet]
        [Authorize]
        public ActionResult<PagingResult<UserResource>> GetList(string keyword, int page, int size)
        {
            var listUser = userService.GetList(keyword, page, size);

            return Ok(listUser);
        }

        [HttpGet("lookup")]
        [Authorize]
        public ActionResult<List<LookupResource>> GetLookup(string keyword)
        {
            var result = userService.GetLookup(keyword);

            return Ok(result);
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("login")]
        public IActionResult Login([FromBody] LoginResource loginResource)
        {
            var userRes = userService.Login(loginResource);
            var roleRes = roleService.Get(userRes.RoleId);

            var claims = new[]
            {
                new Claim("Id", userRes.Id.ToString()),
                new Claim("UserName", userRes.UserName),
                new Claim("RoleId", userRes.RoleId.ToString())
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["TokenKey"]));
            var credential = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                issuer: "Jawara",
                audience: "Jawara",
                claims: claims,
                expires: DateTime.Now.AddHours(24),
                signingCredentials: credential);

            return Ok(new IdentityResource
            {
                User = userRes,
                Role = roleRes,
                Token = new JwtSecurityTokenHandler().WriteToken(token)
            });
        }

        [HttpPost]
        [Authorize]
        public ActionResult<ScalarResource> Create([FromBody] UserCreateResource resource)
        {
            var id = userService.Create(resource, this.GetUserId());

            return Ok(new ScalarResource(id));
        }

        [HttpPut("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> Update(Guid id, [FromBody] UserResource resource)
        {
            resource.Id = id;
            var result = userService.Update(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpPatch("{id}")]
        [Authorize]
        public ActionResult<ScalarResource> PatchUpdate(Guid id, [FromBody] JsonElement patchResource)
        {
            roleService.CheckPrivilege(this.GetRoleId(), Privilege.UserUpdate);

            var resource = userService.Get(id);
            var json = patchResource.GetRawText();
            JsonConvert.PopulateObject(json, resource);
            resource.Id = id;
            var result = userService.Update(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }

        [HttpDelete("{id}")]
        [Authorize]
        public IActionResult Delete(Guid id)
        {
            userService.Delete(id, this.GetUserId());

            return NoContent();
        }

        [HttpPost]
        [Route("change-password")]
        [Authorize]
        public ActionResult<ScalarResource> ChangePassword([FromBody] ChangePasswordResource resource)
        {
            var result = userService.ChangePassword(resource, this.GetUserId());

            return Ok(new ScalarResource(result));
        }
    }
}
