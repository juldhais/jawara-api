﻿using Jawara.Helpers;
using Jawara.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Jawara.Repositories
{
    public static class DataContextExtensions
    {
        public static void Create<TModel>(this DataContext db, TModel model, Guid? createdById)
            where TModel : class, IModel, new()
        {
            model.Id = Guid.NewGuid();
            model.IsDeleted = false;
            model.DateCreated = DateTime.Now;
            model.CreatedById = createdById;
            db.Set<TModel>().Add(model);
        }

        public static void Update<TModel>(this DataContext db, TModel model, Guid? modifiedById)
            where TModel : class, IModel, new()
        {
            if (modifiedById != null)
            {
                model.DateUpdated = DateTime.Now;
                model.UpdatedById = modifiedById;
            }
        }

        public static void Delete<TModel>(this DataContext db, Guid? id, Guid? deletedById)
            where TModel : class, IModel, new()
        {
            var model = db.Set<TModel>().Find(id);
            model.IsDeleted = true;
            model.DateUpdated = DateTime.Now;
            model.UpdatedById = deletedById;
        }

        public static void Delete<TModel>(this DataContext db, Expression<Func<TModel, bool>> criteria, Guid? deletedById)
            where TModel : class, IModel, new()
        {
            var listId = db.Set<TModel>().Where(criteria).Select(x => x.Id);
            foreach (var id in listId)
            {
                var model = db.Set<TModel>().Find(id);
                model.IsDeleted = true;
                model.DateUpdated = DateTime.Now;
                model.UpdatedById = deletedById;
            }
        }

        public static void Remove<TModel>(this DataContext db, Guid id)
            where TModel : class, IModel, new()
        {
            var model = new TModel { Id = id };
            db.Set<TModel>().Remove(model);
        }

        public static void Remove<TModel>(this DataContext db, Expression<Func<TModel, bool>> criteria)
            where TModel : class, IModel, new()
        {
            var listId = db.Set<TModel>().Where(criteria).Select(x => x.Id);
            foreach (var id in listId)
            {
                var model = new TModel { Id = id };
                db.Set<TModel>().Remove(model);
            }
        }

        public static TModel Get<TModel>(this DataContext db, Guid? id)
            where TModel : class, IModel, new()
        {
            return db.Find<TModel>(id);
        }

        public static TResult Get<TModel, TResult>(this DataContext db, Guid? id)
            where TModel : class, IModel, new()
        {
            return db.Set<TModel>()
                    .Where(x => x.Id == id)
                    .Project().To<TResult>()
                    .FirstOrDefault();
        }

        public static List<TResult> GetList<TModel, TResult>(this DataContext db) where TModel : class, IModel, new()
        {
            return db.Set<TModel>()
                    .Where(x => x.IsDeleted == false)
                    .Project().To<TResult>()
                    .ToList();
        }

        public static IQueryable<TModel> GetQuery<TModel>(this DataContext db) where TModel : class, IModel, new()
        {
            return db.Set<TModel>().Where(x => x.IsDeleted == false);
        }

        public static StampInfo GetStampInfo<TModel>(this DataContext db, Guid? id)
            where TModel : class, IModel, new()
        {
            var model = db.Set<TModel>().Find(id);

            if (model == null) return null;

            var stampInfo = new StampInfo();
            stampInfo.CreatedById = model.CreatedById;
            stampInfo.UpdatedById = model.UpdatedById;
            stampInfo.DateCreated = model.DateCreated;
            stampInfo.DateUpdated = model.DateUpdated;

            stampInfo.CreatedBy = db.User.Where(x => x.Id == model.CreatedById).Select(x => x.UserName).FirstOrDefault();
            stampInfo.UpdatedBy = db.User.Where(x => x.Id == model.UpdatedById).Select(x => x.UserName).FirstOrDefault();

            return stampInfo;
        }

        public static string GetConfiguration(this DataContext db, string key)
        {
            var config = db.Configuration
                           .Where(x => x.IsDeleted == false
                                    && x.Key == key)
                           .FirstOrDefault();

            return config?.Value;
        }

        public static void SetConfiguration(this DataContext db, string key, string value)
        {
            var config = db.Configuration
                           .Where(x => x.IsDeleted == false
                                    && x.Key == key)
                           .FirstOrDefault();

            config.Value = value;
        }
    }

    public class StampInfo
    {
        public bool IsDeleted { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }

        public Guid? CreatedById { get; set; }
        public Guid? UpdatedById { get; set; }

        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
    }
}
