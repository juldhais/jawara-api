﻿using Jawara.Models;
using Microsoft.EntityFrameworkCore;

namespace Jawara.Repositories
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions options) : base(options) { }

        public virtual DbSet<BillOfMaterial> BillOfMaterial { get; set; }
        public virtual DbSet<BillOfMaterialDetail> BillOfMaterialDetail { get; set; }
        public virtual DbSet<Configuration> Configuration { get; set; }
        public virtual DbSet<Customer> Customer { get; set; }
        public virtual DbSet<CustomerCategory> CustomerCategory { get; set; }
        public virtual DbSet<Item> Item { get; set; }
        public virtual DbSet<ItemCategory> ItemCategory { get; set; }
        public virtual DbSet<PriceControl> PriceControl { get; set; }
        public virtual DbSet<ItemStock> ItemStock { get; set; }
        public virtual DbSet<ItemSubcategory> ItemSubcategory { get; set; }
        public virtual DbSet<ItemTag> ItemTag { get; set; }
        public virtual DbSet<ItemUnit> ItemUnit { get; set; }
        public virtual DbSet<Location> Location { get; set; }
        public virtual DbSet<ManufacturingOrder> ManufacturingOrder { get; set; }
        public virtual DbSet<ManufacturingOrderDetail> ManufacturingOrderDetail { get; set; }
        public virtual DbSet<ManufacturingCompletion> ManufacturingCompletion { get; set; }
        public virtual DbSet<ManufacturingCompletionDetail> ManufacturingCompletionDetail { get; set; }
        public virtual DbSet<PaymentMethod> PaymentMethod { get; set; }
        public virtual DbSet<PurchaseOrder> PurchaseOrder { get; set; }
        public virtual DbSet<PurchaseOrderDetail> PurchaseOrderDetail { get; set; }
        public virtual DbSet<PurchasePayment> PurchasePayment { get; set; }
        public virtual DbSet<PurchasePaymentDetail> PurchasePaymentDetail { get; set; }
        public virtual DbSet<PurchaseReceipt> PurchaseReceipt { get; set; }
        public virtual DbSet<PurchaseReceiptDetail> PurchaseReceiptDetail { get; set; }
        public virtual DbSet<PurchaseReturn> PurchaseReturn { get; set; }
        public virtual DbSet<PurchaseReturnDetail> PurchaseReturnDetail { get; set; }
        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<RolePrivilege> RolePrivilege { get; set; }
        public virtual DbSet<SalesDelivery> SalesDelivery { get; set; }
        public virtual DbSet<SalesDeliveryDetail> SalesDeliveryDetail { get; set; }
        public virtual DbSet<SalesOrder> SalesOrder { get; set; }
        public virtual DbSet<SalesOrderDetail> SalesOrderDetail { get; set; }
        public virtual DbSet<SalesPayment> SalesPayment { get; set; }
        public virtual DbSet<SalesPaymentDetail> SalesPaymentDetail { get; set; }
        public virtual DbSet<PriceControlCategory> PriceControlCategory { get; set; }
        public virtual DbSet<SalesReturn> SalesReturn { get; set; }
        public virtual DbSet<SalesReturnDetail> SalesReturnDetail { get; set; }
        public virtual DbSet<Section> Section { get; set; }
        public virtual DbSet<Settlement> Settlement { get; set; }
        public virtual DbSet<SettlementDetail> SettlementDetail { get; set; }
        public virtual DbSet<StockAdjustment> StockAdjustment { get; set; }
        public virtual DbSet<StockAdjustmentDetail> StockAdjustmentDetail { get; set; }
        public virtual DbSet<StockControl> StockControl { get; set; }
        public virtual DbSet<StockControlCategory> StockControlCategory { get; set; }
        public virtual DbSet<StockOrder> StockOrder { get; set; }
        public virtual DbSet<StockOrderDetail> StockOrderDetail { get; set; }
        public virtual DbSet<StockDelivery> StockDelivery { get; set; }
        public virtual DbSet<StockDeliveryDetail> StockDeliveryDetail { get; set; }
        public virtual DbSet<StockReceipt> StockReceipt { get; set; }
        public virtual DbSet<StockReceiptDetail> StockReceiptDetail { get; set; }
        public virtual DbSet<StockMovement> StockMovement { get; set; }
        public virtual DbSet<Supplier> Supplier { get; set; }
        public virtual DbSet<SupplierCategory> SupplierCategory { get; set; }
        public virtual DbSet<Tax> Tax { get; set; }
        public virtual DbSet<Unit> Unit { get; set; }
        public virtual DbSet<User> User { get; set; }
    }
}
