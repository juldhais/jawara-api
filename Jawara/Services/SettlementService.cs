﻿using Jawara.Helpers;
using Jawara.Models;
using Jawara.Repositories;
using Jawara.Resources;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Jawara.Services
{
    public class SettlementService
    {
        private readonly DataContext db;

        public SettlementService(DataContext db)
        {
            this.db = db;
        }

        public SettlementResource Get(Guid? id)
        {
            var settlement = db.Get<Settlement, SettlementResource>(id);
            if (settlement != null)
            {
                settlement.Details = db.SettlementDetail
                    .Where(x => x.IsDeleted == false
                                && x.SettlementId == id)
                    .OrderBy(x => x.PaymentMethod.Sequence)
                    .Project().To<SettlementDetailResource>()
                    .ToList();

                if (settlement.Details == null)
                    settlement.Details = new List<SettlementDetailResource>();
            }

            return settlement;
        }

        public PagingResult<SettlementResource> GetList(string keyword, Guid? locationId, Guid? userId, DateTime? startDate, DateTime? endDate, int page, int size)
        {
            if (keyword.IsEmpty()
                && locationId.IsGuidEmpty()
                && userId.IsGuidEmpty()
                && startDate == null && endDate == null
                && page == 0 && size == 0)
                return new PagingResult<SettlementResource>();

            var query = db.GetQuery<Settlement>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.DocumentNumber.Contains(keyword));

            if (userId.IsGuidNotEmpty())
                query = query.Where(x => x.UserId == userId);

            if (locationId.IsGuidNotEmpty())
                query = query.Where(x => x.LocationId == locationId);

            if (startDate != null)
                query = query.Where(x => x.DocumentDate.Date >= startDate);

            if (endDate != null)
                query = query.Where(x => x.DocumentDate.Date <= endDate);

            query = query.OrderByDescending(x => x.DocumentNumber);

            var result = query.PaginateTo<Settlement, SettlementResource>(page, size);

            return result;
        }

        private void ValidateResource(SettlementResource res)
        {
            if (res.DocumentDate == default)
                throw new BadRequestException("Document date cannot be empty.");

            if (res.UserId.IsGuidEmpty())
                throw new BadRequestException("User cannot be empty.");

            if (res.LocationId.IsGuidEmpty())
                throw new BadRequestException("Location cannot be empty.");
        }

        public string GetNewDocumentNumber(DateTime date)
        {
            var prefix = $"ST-{date:yyMM}";

            var lastNumber = db.GetQuery<Settlement>()
                .Where(x => x.DocumentNumber.StartsWith(prefix))
                .OrderByDescending(x => x.DocumentNumber)
                .Select(x => x.DocumentNumber)
                .FirstOrDefault();

            if (lastNumber == null) return prefix + "0001";

            var newNumber = lastNumber.Substring(prefix.Length, 4).ToInteger() + 1;
            return prefix + newNumber.ToString("0000");
        }

        public Guid Create(SettlementResource res, Guid? userId)
        {
            ValidateResource(res);

            var transaction = db.Database.BeginTransaction();

            try
            {
                var settlement = new Settlement();
                settlement.MapFrom(res);
                settlement.DocumentNumber = GetNewDocumentNumber(res.DocumentDate);
                db.Create(settlement, userId);

                foreach (var detailRes in res.Details)
                {
                    var detail = new SettlementDetail();
                    detail.MapFrom(detailRes);
                    detail.SettlementId = settlement.Id;

                    var totalIncoming = 0;
                    var totalOutgoing = 0;

                    detail.SystemBalance = totalIncoming - totalOutgoing;
                    detail.Difference = detail.InputBalance - detail.SystemBalance;

                    db.Create(detail, userId);
                }

                db.SaveChanges();
                transaction.Commit();

                return settlement.Id;
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
        }

        public Guid Update(SettlementResource res, Guid? userId)
        {
            ValidateResource(res);

            var transaction = db.Database.BeginTransaction();

            try
            {
                db.Database.ExecuteSqlRaw("DELETE FROM SettlementDetail WHERE SettlementId = {0} AND IsDeleted = 0", res.Id);

                //update

                var settlement = db.Get<Settlement>(res.Id);

                if (settlement == null)
                    throw new BadRequestException("Settlement not found.");

                settlement.MapFrom(res);
                db.Update(settlement, userId);

                foreach (var detailRes in res.Details)
                {
                    var detail = new SettlementDetail();
                    detail.MapFrom(detailRes);
                    detail.SettlementId = settlement.Id;

                    var totalIncoming = 0;
                    var totalOutgoing = 0;

                    detail.SystemBalance = totalIncoming - totalOutgoing;
                    detail.Difference = detail.InputBalance - detail.SystemBalance;

                    db.Create(detail, userId);
                }

                db.SaveChanges();
                transaction.Commit();

                return res.Id;
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
        }

        public void Delete(Guid? id, Guid? userId)
        {
            var settlement = db.Get<Settlement>(id);

            db.Delete<SettlementDetail>(x => x.SettlementId == id, userId);
            db.Delete<Settlement>(id, userId);
            db.SaveChanges();
        }
    }
}
