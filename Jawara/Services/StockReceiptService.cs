﻿using Jawara.Enums;
using Jawara.Helpers;
using Jawara.Models;
using Jawara.Repositories;
using Jawara.Resources;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace Jawara.Services
{
    public class StockReceiptService
    {
        private readonly DataContext db;
        private readonly ItemService itemService;
        private readonly StockOrderService stockOrderService;
        private readonly StockDeliveryService stockDeliveryService;

        public StockReceiptService(DataContext db,
            ItemService itemService,
            StockOrderService stockOderService,
            StockDeliveryService stockDeliveryService)
        {
            this.db = db;
            this.itemService = itemService;
            this.stockOrderService = stockOderService;
            this.stockDeliveryService = stockDeliveryService;
        }

        public StockReceiptResource Get(Guid? id)
        {
            var receipt = db.Get<StockReceipt, StockReceiptResource>(id);
            if (receipt != null)
            {
                var listDetail = db.GetQuery<StockReceiptDetail>()
                    .Where(x => x.StockReceiptId == id)
                    .OrderBy(x => x.Sequence)
                    .Project().To<StockReceiptDetailResource>()
                    .ToList();

                if (listDetail != null) receipt.Details = listDetail;

                receipt.CreatedBy = db.GetQuery<User>()
                    .Where(x => x.Id == receipt.CreatedById)
                    .Select(x => x.Description)
                    .FirstOrDefault();

                receipt.UpdatedBy = db.GetQuery<User>()
                    .Where(x => x.Id == receipt.UpdatedById)
                    .Select(x => x.Description)
                    .FirstOrDefault();
            }

            return receipt;
        }

        public PagingResult<StockReceiptResource> GetList(string keyword, Guid? destinationId, Guid? sourceId, DateTime? startDate, DateTime? endDate, int page, int size)
        {
            if (keyword.IsEmpty()
                && destinationId.IsGuidEmpty()
                && sourceId.IsGuidEmpty()
                && startDate == null && endDate == null
                && page == 0 && size == 0)
                return new PagingResult<StockReceiptResource>();

            var query = db.GetQuery<StockReceipt>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.DocumentNumber.Contains(keyword)
                    || x.Remarks.Contains(keyword));

            if (destinationId.IsGuidNotEmpty())
                query = query.Where(x => x.DestinationLocationId == destinationId);

            if (sourceId.IsGuidNotEmpty())
                query = query.Where(x => x.SourceLocationId == sourceId);

            if (startDate != null)
                query = query.Where(x => x.DocumentDate.Date >= startDate);

            if (endDate != null)
                query = query.Where(x => x.DocumentDate.Date <= endDate);

            query = query.OrderByDescending(x => x.DocumentNumber);

            var result = query.PaginateTo<StockReceipt, StockReceiptResource>(page, size);

            return result;
        }

        private void ValidateResource(StockReceiptResource res)
        {
            if (res.DocumentDate == default)
                throw new BadRequestException("Document date cannot be empty.");

            if (res.DestinationLocationId.IsGuidEmpty())
                throw new BadRequestException("Destination cannot be empty.");

            if (res.SourceLocationId.IsGuidEmpty())
                throw new BadRequestException("Source Location cannot be empty.");
        }

        public string GetNewDocumentNumber(DateTime date)
        {
            var prefix = $"IR-{date:yyMM}";

            var lastNumber = db.GetQuery<StockReceipt>()
                .Where(x => x.DocumentNumber.StartsWith(prefix))
                .OrderByDescending(x => x.DocumentNumber)
                .Select(x => x.DocumentNumber)
                .FirstOrDefault();

            if (lastNumber == null) return prefix + "0001";

            var newNumber = lastNumber.Substring(prefix.Length, 4).ToInteger() + 1;
            return prefix + newNumber.ToString("0000");
        }

        public Guid Create(StockReceiptResource res, Guid? userId)
        {
            ValidateResource(res);

            var transaction = db.Database.BeginTransaction();

            try
            {
                var receipt = new StockReceipt();
                receipt.MapFrom(res);
                receipt.DocumentNumber = GetNewDocumentNumber(res.DocumentDate);
                db.Create(receipt, userId);

                // detail
                var sequence = 0;
                foreach (var detail in res.Details)
                {
                    if (detail.ItemId.IsGuidEmpty()) continue;
                    if (detail.Quantity == 0 || detail.Ratio == 0) continue;

                    var item = db.Get<Item>(detail.ItemId);

                    var receiptDetail = new StockReceiptDetail();
                    receiptDetail.MapFrom(detail);
                    receiptDetail.StockReceiptId = receipt.Id;
                    receiptDetail.Sequence = ++sequence;
                    db.Create(receiptDetail, userId);

                    var itemStockable = db.GetQuery<Item>()
                        .Where(x => x.Id == receiptDetail.ItemId)
                        .Select(x => x.Stockable)
                        .FirstOrDefault();
                    if (itemStockable)
                    {
                        itemService.AddStockMovementIn(
                        transactionId: receipt.Id,
                        documentNumber: receipt.DocumentNumber,
                        documentDate: receipt.DocumentDate,
                        transactionType: TransactionType.StockReceipt,
                        itemId: receiptDetail.ItemId,
                        locationId: receipt.DestinationLocationId,
                        quantity: receiptDetail.Quantity * receiptDetail.Ratio,
                        cost: receiptDetail.Cost / receiptDetail.Ratio);
                    }
                }

                db.SaveChanges();

                var stockOrderId = db.GetQuery<StockDelivery>()
                    .Where(x => x.Id == receipt.StockDeliveryId)
                    .Select(x => x.StockOrderId)
                    .FirstOrDefault();

                stockOrderService.CalculateQuantityReceived(stockOrderId);
                stockDeliveryService.CalculateQuantityReceived(receipt.StockDeliveryId);

                transaction.Commit();

                return receipt.Id;
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
        }

        public Guid Update(StockReceiptResource res, Guid? userId)
        {
            ValidateResource(res);

            var transaction = db.Database.BeginTransaction();

            try
            {
                // delete old
                var oldRes = Get(res.Id);
                itemService.RemoveStockMovement(oldRes.Id);
                db.Database.ExecuteSqlRaw("DELETE FROM StockReceiptDetail WHERE StockReceiptId = {0} AND IsDeleted = 0", res.Id);

                //update
                var receipt = db.Get<StockReceipt>(res.Id);
                receipt.MapFrom(res);
                db.Update(receipt, userId);

                var sequence = 0;
                foreach (var detail in res.Details)
                {
                    if (detail.ItemId.IsGuidEmpty()) continue;
                    if (detail.Quantity == 0) continue;

                    var receiptDetail = new StockReceiptDetail();
                    receiptDetail.MapFrom(detail);
                    receiptDetail.StockReceiptId = receipt.Id;
                    receiptDetail.Sequence = ++sequence;
                    db.Create(receiptDetail, userId);

                    var itemStockable = db.GetQuery<Item>()
                        .Where(x => x.Id == receiptDetail.ItemId)
                        .Select(x => x.Stockable)
                        .FirstOrDefault();
                    if (itemStockable)
                    {
                        itemService.AddStockMovementIn(
                        transactionId: receipt.Id,
                        documentNumber: receipt.DocumentNumber,
                        documentDate: receipt.DocumentDate,
                        transactionType: TransactionType.StockReceipt,
                        itemId: receiptDetail.ItemId,
                        locationId: receipt.DestinationLocationId,
                        quantity: receiptDetail.Quantity * receiptDetail.Ratio,
                        cost: receiptDetail.Cost / receiptDetail.Ratio);
                    }
                }

                db.SaveChanges();

                var stockOrderId = db.GetQuery<StockDelivery>()
                    .Where(x => x.Id == receipt.StockDeliveryId)
                    .Select(x => x.StockOrderId)
                    .FirstOrDefault();
                stockOrderService.CalculateQuantityReceived(stockOrderId);
                stockDeliveryService.CalculateQuantityReceived(receipt.StockDeliveryId);

                transaction.Commit();

                return res.Id;
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
        }

        public void Delete(Guid? id, Guid? userId)
        {
            var transaction = db.Database.BeginTransaction();

            try
            {
                // delete old
                itemService.RemoveStockMovement(id);
                db.Delete<StockReceiptDetail>(x => x.StockReceiptId == id, userId);
                db.Delete<StockReceipt>(id, userId);
                db.SaveChanges();

                var stockReceipt = db.GetQuery<StockReceipt>()
                    .Where(x => x.Id == id)
                    .Select(x => new
                    {
                        x.StockDeliveryId,
                        x.StockDelivery.StockOrderId
                    }).FirstOrDefault();

                stockOrderService.CalculateQuantityReceived(stockReceipt.StockOrderId);
                stockDeliveryService.CalculateQuantityReceived(stockReceipt.StockDeliveryId);

                transaction.Commit();
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
        }
    }
}
