﻿using Jawara.Enums;
using Jawara.Helpers;
using Jawara.Models;
using Jawara.Repositories;
using Jawara.Resources;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Jawara.Services
{
    public class PurchaseReturnService
    {
        private readonly DataContext db;
        private readonly ItemService itemService;

        public PurchaseReturnService(DataContext db,
                                     ItemService itemService)
        {
            this.db = db;
            this.itemService = itemService;
        }

        public PurchaseReturnResource Get(Guid? id)
        {
            var ret = db.Get<PurchaseReturn, PurchaseReturnResource>(id);
            if (ret != null)
            {
                ret.Details = db.PurchaseReturnDetail
                    .Where(x => x.IsDeleted == false
                                && x.PurchaseReturnId == id)
                    .OrderBy(x => x.Sequence)
                    .Project().To<PurchaseReturnDetailResource>()
                    .ToList();

                if (ret.Details == null)
                    ret.Details = new List<PurchaseReturnDetailResource>();

                ret.CreatedBy = db.GetQuery<User>()
                    .Where(x => x.Id == ret.CreatedById)
                    .Select(x => x.Description)
                    .FirstOrDefault();

                ret.UpdatedBy = db.GetQuery<User>()
                    .Where(x => x.Id == ret.UpdatedById)
                    .Select(x => x.Description)
                    .FirstOrDefault();
            }

            return ret;
        }

        public PagingResult<PurchaseReturnResource> GetList(string keyword, string status, Guid? supplierId, Guid? locationId, DateTime? startDate, DateTime? endDate, int page, int size)
        {
            if (keyword.IsEmpty()
                && status.IsEmpty()
                && supplierId.IsGuidEmpty()
                && locationId.IsGuidEmpty()
                && startDate == null && endDate == null
                && page == 0 && size == 0)
                return new PagingResult<PurchaseReturnResource>();

            var query = db.GetQuery<PurchaseReturn>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.DocumentNumber.Contains(keyword)
                    || x.ReferenceNumber.Contains(keyword)
                    || x.Remarks.Contains(keyword));

            if (supplierId.IsGuidNotEmpty())
                query = query.Where(x => x.SupplierId == supplierId);

            if (locationId.IsGuidNotEmpty())
                query = query.Where(x => x.LocationId == locationId);

            if (status.IsNotEmpty())
                query = query.Where(x => x.Status == status);

            if (startDate != null)
                query = query.Where(x => x.DocumentDate.Date >= startDate);

            if (endDate != null)
                query = query.Where(x => x.DocumentDate.Date <= endDate);

            query = query.OrderByDescending(x => x.DocumentNumber);

            var result = query.PaginateTo<PurchaseReturn, PurchaseReturnResource>(page, size);

            return result;
        }

        private void Calculate(PurchaseReturnResource res)
        {
            res.TotalBeforeTax = 0;
            res.TotalValueAddedTax = 0;
            res.TotalIncomeTax = 0;
            res.TotalOtherTax = 0;
            res.TotalAfterTax = 0;

            foreach (var detail in res.Details)
            {
                var subtotal = detail.Quantity * detail.Price;
                var subtotalAfterDiscount1 = subtotal * (100 - detail.DiscountPercent1) / 100;
                var subtotalAfterDiscount2 = subtotalAfterDiscount1 * (100 - detail.DiscountPercent2) / 100;
                var subtotalAfterDiscountAmount = subtotalAfterDiscount2 - detail.DiscountAmount;
                detail.SubtotalBeforeTax = subtotalAfterDiscountAmount;

                if (detail.ValueAddedTaxId.IsGuidNotEmpty())
                {
                    var vat = db.Get<Tax>(detail.ValueAddedTaxId);
                    detail.ValueAddedTaxAmount = detail.SubtotalBeforeTax * vat.Rate / 100 + vat.Amount;
                }
                if (detail.IncomeTaxId.IsGuidNotEmpty())
                {
                    var ict = db.Get<Tax>(detail.IncomeTaxId);
                    detail.IncomeTaxAmount = detail.SubtotalBeforeTax * ict.Rate / 100 + ict.Amount;
                }
                if (detail.OtherTaxId.IsGuidNotEmpty())
                {
                    var oth = db.Get<Tax>(detail.OtherTaxId);
                    detail.OtherTaxAmount = detail.SubtotalBeforeTax * oth.Rate / 100 + oth.Amount;
                }

                detail.SubtotalAfterTax = detail.SubtotalBeforeTax + detail.ValueAddedTaxAmount + detail.IncomeTaxAmount + detail.OtherTaxAmount;

                res.TotalBeforeTax += detail.SubtotalBeforeTax;
                res.TotalValueAddedTax += detail.ValueAddedTaxAmount;
                res.TotalIncomeTax += detail.IncomeTaxAmount;
                res.TotalOtherTax += detail.OtherTaxAmount;
                res.TotalAfterTax += detail.SubtotalAfterTax;
            }
        }

        private void ValidateResource(PurchaseReturnResource res)
        {
            if (res.DocumentDate == default)
                throw new BadRequestException("Document date cannot be empty.");

            if (res.SupplierId.IsGuidEmpty())
                throw new BadRequestException("Supplier cannot be empty.");

            if (res.LocationId.IsGuidEmpty())
                throw new BadRequestException("Location cannot be empty.");
        }

        public string GetNewDocumentNumber(DateTime date)
        {
            var prefix = $"PT-{date:yyMM}";

            var lastNumber = db.GetQuery<PurchaseReturn>()
                               .Where(x => x.DocumentNumber.StartsWith(prefix))
                               .OrderByDescending(x => x.DocumentNumber)
                               .Select(x => x.DocumentNumber)
                               .FirstOrDefault();

            if (lastNumber == null) return prefix + "0001";

            var newNumber = lastNumber.Substring(prefix.Length, 4).ToInteger() + 1;
            return prefix + newNumber.ToString("0000");
        }

        public Guid Create(PurchaseReturnResource res, Guid? userId)
        {
            ValidateResource(res);

            var transaction = db.Database.BeginTransaction();

            try
            {
                Calculate(res);

                var ret = new PurchaseReturn();
                ret.MapFrom(res);
                ret.DocumentNumber = GetNewDocumentNumber(res.DocumentDate);
                ret.Status = ReturnStatus.Unused;
                db.Create(ret, userId);

                var sequence = 0;
                foreach (var detail in res.Details)
                {
                    if (detail.ItemId.IsGuidEmpty()) continue;
                    if (detail.Quantity == 0 || detail.Ratio == 0) continue;

                    var retDetail = new PurchaseReturnDetail();
                    retDetail.MapFrom(detail);
                    retDetail.PurchaseReturnId = ret.Id;
                    retDetail.Sequence = ++sequence;
                    db.Create(retDetail, userId);

                    var item = db.Get<Item>(detail.ItemId);
                    if (item.Stockable)
                    {
                        itemService.AddStockMovementOut(
                            transactionId: ret.Id,
                            documentNumber: ret.DocumentNumber,
                            documentDate: ret.DocumentDate,
                            transactionType: TransactionType.PurchaseReturn,
                            itemId: detail.ItemId,
                            locationId: ret.LocationId,
                            quantity: detail.Quantity * detail.Ratio);
                    }
                }

                db.SaveChanges();

                transaction.Commit();

                return ret.Id;
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
        }

        public Guid Update(PurchaseReturnResource res, Guid? userId)
        {
            ValidateResource(res);

            var transaction = db.Database.BeginTransaction();

            try
            {
                // delete old
                itemService.RemoveStockMovement(res.Id);
                db.Database.ExecuteSqlRaw("DELETE FROM PurchaseReturnDetail WHERE PurchaseReturnId = {0}", res.Id);

                //update
                Calculate(res);

                var ret = db.Get<PurchaseReturn>(res.Id);
                ret.MapFrom(res);
                db.Update(ret, userId);

                var sequence = 0;
                foreach (var detail in res.Details)
                {
                    if (detail.ItemId.IsGuidEmpty()) continue;
                    if (detail.Quantity == 0) continue;

                    var retDetail = new PurchaseReturnDetail();
                    retDetail.MapFrom(detail);
                    retDetail.PurchaseReturnId = ret.Id;
                    retDetail.Sequence = ++sequence;
                    db.Create(retDetail, userId);

                    var item = db.Get<Item>(detail.ItemId);
                    if (item.Stockable)
                    {
                        itemService.AddStockMovementOut(
                            transactionId: ret.Id,
                            documentNumber: ret.DocumentNumber,
                            documentDate: ret.DocumentDate,
                            transactionType: TransactionType.PurchaseReturn,
                            itemId: detail.ItemId,
                            locationId: ret.LocationId,
                            quantity: detail.Quantity * detail.Ratio);
                    }
                }

                db.SaveChanges();

                transaction.Commit();

                return res.Id;
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
        }

        public void Delete(Guid? id, Guid? userId)
        {
            var transaction = db.Database.BeginTransaction();

            try
            {
                // delete old
                itemService.RemoveStockMovement(id);
                db.Delete<PurchaseReturnDetail>(x => x.PurchaseReturnId == id, userId);
                db.Delete<PurchaseReturn>(id, userId);
                db.SaveChanges();

                transaction.Commit();
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
        }
    }
}
