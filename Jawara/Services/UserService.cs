﻿using Jawara.Helpers;
using Jawara.Models;
using Jawara.Repositories;
using Jawara.Resources;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Jawara.Services
{
    public class UserService
    {
        private readonly DataContext db;

        public UserService(DataContext db)
        {
            this.db = db;
        }

        public Guid ChangePassword(ChangePasswordResource resource, Guid? userId)
        {
            var user = db.User.Find(userId);

            if (user.UserName != resource.UserName)
                throw new BadRequestException("User name did not match.");

            var updatingUser = Get(userId);
            if (!updatingUser.IsAdministrator)
            {
                if (StringEncryptor.Encrypt(resource.OldPassword) != user.Password)
                    throw new BadRequestException("Password did not match.");
            }

            user.Password = StringEncryptor.Encrypt(resource.NewPassword);

            db.Update(user, userId);
            db.SaveChanges();

            return user.Id;
        }

        public Guid Create(UserCreateResource resource, Guid? userId)
        {
            if (resource.UserName.IsEmpty())
                throw new BadRequestException("User name cannot be empty.");

            if (resource.Description.IsEmpty())
                throw new BadRequestException("Code cannot be empty.");

            if (resource.Password.IsEmpty())
                throw new BadRequestException("Password cannot be empty.");

            if (db.User.Any(x => x.UserName == resource.UserName))
                throw new BadRequestException("Username is already taken.");

            var user = new User();
            user.MapFrom(resource);
            user.Password = StringEncryptor.Encrypt(resource.Password);

            db.Create(user, userId);
            db.SaveChanges();

            return user.Id;
        }

        public void Delete(Guid? id, Guid? deletedById)
        {
            db.Delete<User>(id, deletedById);
        }

        public UserResource Get(Guid? id)
        {
            return db.Get<User, UserResource>(id);
        }

        public List<UserResource> GetList()
        {
            return db.GetList<User, UserResource>();
        }

        public PagingResult<UserResource> GetList(string keyword, int page = 0, int size = 0)
        {
            var query = db.GetQuery<User>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.Description.Contains(keyword));

            return query.OrderBy(x => x.Description)
                .PaginateTo<User, UserResource>(page, size);
        }

        public List<LookupResource> GetLookup(string keyword)
        {
            if (keyword.IsEmpty() || keyword.Length < 2)
                return new List<LookupResource>();

            var query = db.GetQuery<User>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.Description.Contains(keyword));

            var result = query.OrderBy(x => x.UserName)
                .Select(x => new LookupResource
                {
                    Id = x.Id,
                    Code = x.UserName + " ─ " + x.Description
                }).ToList();

            return result;
        }

        public LookupResource GetSingleLookup(Guid? id)
        {
            var result = db.GetQuery<User>()
                .Where(x => x.Id == id)
                .Select(x => new LookupResource
                {
                    Id = x.Id,
                    Code = x.UserName + " ─ " + x.Description
                }).FirstOrDefault();

            return result;
        }

        public UserResource Login(LoginResource resource)
        {
            if (resource.UserName.IsEmpty())
                throw new BadRequestException("User name cannot be empty.");

            if (resource.Password.IsEmpty())
                throw new BadRequestException("Password cannot be empty.");

            var user = db.GetQuery<User>()
                .Where(x => x.UserName == resource.UserName)
                .FirstOrDefault();

            if (user == null)
                throw new BadRequestException("User not found.");

            if (user.Password != StringEncryptor.Encrypt(resource.Password))
                throw new BadRequestException("Wrong user name or password.");

            return Get(user.Id);
        }

        public Guid Update(UserResource resource, Guid? updatedById)
        {
            if (resource.UserName.IsEmpty())
                throw new BadRequestException("User name cannot be empty.");

            if (resource.Description.IsEmpty())
                throw new BadRequestException("Code cannot be empty.");

            if (db.User.Any(x => x.UserName == resource.UserName && x.Id != resource.Id))
                throw new BadRequestException("Username is already taken.");

            var user = db.User.Find(resource.Id);
            user.MapFrom(resource);
            db.Update(user, updatedById);
            db.SaveChanges();

            return user.Id;
        }
    }
}