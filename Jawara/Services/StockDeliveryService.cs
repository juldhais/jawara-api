﻿using Jawara.Enums;
using Jawara.Helpers;
using Jawara.Models;
using Jawara.Repositories;
using Jawara.Resources;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Jawara.Services
{
    public class StockDeliveryService
    {
        private readonly DataContext db;
        private readonly ItemService itemService;
        private readonly StockOrderService stockOrderService;

        public StockDeliveryService(DataContext db,
            ItemService itemService,
            StockOrderService stockOrderService)
        {
            this.db = db;
            this.itemService = itemService;
            this.stockOrderService = stockOrderService;
        }

        public StockDeliveryResource Get(Guid? id)
        {
            var delivery = db.Get<StockDelivery, StockDeliveryResource>(id);
            if (delivery != null)
            {
                var listDetail = db.GetQuery<StockDeliveryDetail>()
                    .Where(x => x.StockDeliveryId == id)
                    .OrderBy(x => x.Sequence)
                    .Project().To<StockDeliveryDetailResource>()
                    .ToList();

                if (listDetail != null) delivery.Details = listDetail;

                delivery.CreatedBy = db.GetQuery<User>()
                    .Where(x => x.Id == delivery.CreatedById)
                    .Select(x => x.Description)
                    .FirstOrDefault();

                delivery.UpdatedBy = db.GetQuery<User>()
                    .Where(x => x.Id == delivery.UpdatedById)
                    .Select(x => x.Description)
                    .FirstOrDefault();
            }

            return delivery;
        }

        public PagingResult<StockDeliveryResource> GetList(string keyword, string status, Guid? destinationId, Guid? sourceId, DateTime? startDate, DateTime? endDate, int page, int size)
        {
            if (keyword.IsEmpty()
                && status.IsEmpty()
                && destinationId.IsGuidEmpty()
                && sourceId.IsGuidEmpty()
                && startDate == null && endDate == null
                && page == 0 && size == 0)
                return new PagingResult<StockDeliveryResource>();

            var query = db.GetQuery<StockDelivery>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.DocumentNumber.Contains(keyword)
                    || x.Remarks.Contains(keyword));

            if (status.IsNotEmpty())
                query = query.Where(x => x.Status == status);

            if (destinationId.IsGuidNotEmpty())
                query = query.Where(x => x.DestinationLocationId == destinationId);

            if (sourceId.IsGuidNotEmpty())
                query = query.Where(x => x.SourceLocationId == sourceId);

            if (startDate != null)
                query = query.Where(x => x.DocumentDate.Date >= startDate);

            if (endDate != null)
                query = query.Where(x => x.DocumentDate.Date <= endDate);

            query = query.OrderByDescending(x => x.DocumentNumber);

            var result = query.PaginateTo<StockDelivery, StockDeliveryResource>(page, size);

            return result;
        }

        public List<LookupResource> GetLookup(string keyword, string status, Guid? sourceId, Guid? destinationId)
        {
            if (keyword.IsEmpty()
                && status.IsEmpty()
                && sourceId.IsGuidEmpty()
                && destinationId.IsGuidEmpty())
                return new List<LookupResource>();

            var query = db.GetQuery<StockDelivery>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.DocumentNumber.Contains(keyword)
                    || x.Remarks.Contains(keyword));

            if (status.IsNotEmpty())
                query = query.Where(x => x.Status == status);

            if (sourceId.IsGuidNotEmpty())
                query = query.Where(x => x.SourceLocationId == sourceId);

            if (destinationId.IsGuidNotEmpty())
                query = query.Where(x => x.DestinationLocationId == sourceId);

            var result = query.OrderByDescending(x => x.DocumentNumber)
                .Select(x => new LookupResource
                {
                    Id = x.Id,
                    Code = x.DocumentNumber + " ─ " + x.DestinationLocation.Description
                }).ToList();

            return result;
        }

        public PagingResult<StockReceiptResource> GetListReceipt(Guid? id, int page, int size)
        {
            if (id.IsGuidEmpty() && page == 0 && size == 0)
                return new PagingResult<StockReceiptResource>();

            var query = db.GetQuery<StockReceipt>()
                .Where(x => x.StockDeliveryId == id)
                .OrderByDescending(x => x.DocumentNumber);

            var result = query.PaginateTo<StockReceipt, StockReceiptResource>(page, size);

            return result;
        }

        private void ValidateResource(StockDeliveryResource res)
        {
            if (res.DocumentDate == default)
                throw new BadRequestException("Document date cannot be empty.");

            if (res.DestinationLocationId.IsGuidEmpty())
                throw new BadRequestException("Destination cannot be empty.");

            if (res.SourceLocationId.IsGuidEmpty())
                throw new BadRequestException("Source Location cannot be empty.");
        }

        public string GetNewDocumentNumber(DateTime date)
        {
            var prefix = $"ID-{date:yyMM}";

            var lastNumber = db.GetQuery<StockDelivery>()
                .Where(x => x.DocumentNumber.StartsWith(prefix))
                .OrderByDescending(x => x.DocumentNumber)
                .Select(x => x.DocumentNumber)
                .FirstOrDefault();

            if (lastNumber == null) return prefix + "0001";

            var newNumber = lastNumber.Substring(prefix.Length, 4).ToInteger() + 1;
            return prefix + newNumber.ToString("0000");
        }

        public Guid Create(StockDeliveryResource res, Guid? userId)
        {
            ValidateResource(res);

            var transaction = db.Database.BeginTransaction();

            try
            {
                var delivery = new StockDelivery();
                delivery.MapFrom(res);
                delivery.DocumentNumber = GetNewDocumentNumber(res.DocumentDate);
                db.Create(delivery, userId);

                // detail
                var sequence = 0;
                foreach (var detail in res.Details)
                {
                    if (detail.ItemId.IsGuidEmpty()) continue;
                    if (detail.Quantity == 0 || detail.Ratio == 0) continue;

                    var deliveryDetail = new StockDeliveryDetail();
                    deliveryDetail.MapFrom(detail);
                    deliveryDetail.StockDeliveryId = delivery.Id;
                    deliveryDetail.Sequence = ++sequence;
                    db.Create(deliveryDetail, userId);

                    var itemStockable = db.GetQuery<Item>()
                        .Where(x => x.Id == deliveryDetail.ItemId)
                        .Select(x => x.Stockable)
                        .FirstOrDefault();
                    if (itemStockable)
                    {
                        itemService.AddStockMovementOut(
                            transactionId: delivery.Id,
                            documentNumber: delivery.DocumentNumber,
                            documentDate: delivery.DocumentDate,
                            transactionType: TransactionType.StockDelivery,
                            itemId: detail.ItemId,
                            locationId: delivery.SourceLocationId,
                            detail.Quantity * detail.Ratio);
                    }
                }

                db.SaveChanges();

                stockOrderService.CalculateQuantityDelivered(delivery.Id);

                transaction.Commit();

                return delivery.Id;
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
        }

        public Guid Update(StockDeliveryResource res, Guid? userId)
        {
            ValidateResource(res);

            var transaction = db.Database.BeginTransaction();

            try
            {
                // delete old
                itemService.RemoveStockMovement(res.Id);
                db.SaveChanges();

                db.Database.ExecuteSqlRaw("DELETE FROM StockDeliveryDetail WHERE StockDeliveryId = {0} AND IsDeleted = 0", res.Id);

                //update
                var delivery = db.Get<StockDelivery>(res.Id);
                delivery.MapFrom(res);
                db.Update(delivery, userId);

                var sequence = 0;
                foreach (var detail in res.Details)
                {
                    if (detail.ItemId.IsGuidEmpty()) continue;
                    if (detail.Quantity == 0) continue;

                    var deliveryDetail = new StockDeliveryDetail();
                    deliveryDetail.MapFrom(detail);
                    deliveryDetail.StockDeliveryId = delivery.Id;
                    deliveryDetail.Sequence = ++sequence;
                    db.Create(deliveryDetail, userId);

                    var itemStockable = db.GetQuery<Item>()
                        .Where(x => x.Id == deliveryDetail.ItemId)
                        .Select(x => x.Stockable)
                        .FirstOrDefault();
                    if (itemStockable)
                    {
                        itemService.AddStockMovementOut(
                            transactionId: delivery.Id,
                            documentNumber: delivery.DocumentNumber,
                            documentDate: delivery.DocumentDate,
                            transactionType: TransactionType.StockDelivery,
                            itemId: detail.ItemId,
                            locationId: delivery.SourceLocationId,
                            quantity: detail.Quantity * detail.Ratio);
                    }
                }

                db.SaveChanges();

                stockOrderService.CalculateQuantityDelivered(delivery.Id);

                transaction.Commit();

                return res.Id;
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
        }

        public void Delete(Guid? id, Guid? userId)
        {
            var transaction = db.Database.BeginTransaction();

            try
            {
                // delete old
                itemService.RemoveStockMovement(id);
                db.Delete<StockDeliveryDetail>(x => x.StockDeliveryId == id, userId);
                db.Delete<StockDelivery>(id, userId);

                var stockOrderId = db.GetQuery<StockDelivery>()
                    .Where(x => x.Id == id)
                    .Select(x => x.StockOrderId)
                    .FirstOrDefault();

                stockOrderService.CalculateQuantityDelivered(stockOrderId);

                db.SaveChanges();

                transaction.Commit();
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
        }

        public void Open(Guid? id, Guid? userId)
        {
            var delivery = db.Get<StockDelivery>(id);
            delivery.Status = StockDeliveryStatus.Open;
            db.Update(delivery, userId);

            db.SaveChanges();
        }

        public void Close(Guid? id, Guid? userId)
        {
            var delivery = db.Get<StockDelivery>(id);
            delivery.Status = StockDeliveryStatus.Closed;
            db.Update(delivery, userId);

            db.SaveChanges();
        }

        public void CalculateQuantityReceived(Guid? id)
        {
            var delivery = db.Get<StockDelivery>(id);

            var deliveryDetails = db.GetQuery<StockDeliveryDetail>()
                .Where(x => x.StockDeliveryId == id)
                .Select(x => x.Id)
                .ToList();

            var isCompleted = true;
            foreach (var detailId in deliveryDetails)
            {
                var deliveryDetail = db.Get<StockDeliveryDetail>(detailId);

                var qtyReceived = db.GetQuery<StockReceiptDetail>()
                    .Where(x => x.StockReceipt.StockDeliveryId == id
                        && x.ItemId == deliveryDetail.ItemId)
                    .Sum(x => x.Quantity * x.Ratio);

                deliveryDetail.QuantityReceived = qtyReceived / deliveryDetail.Ratio;

                if (deliveryDetail.Quantity > deliveryDetail.QuantityReceived)
                    isCompleted = false;
            }

            if (isCompleted)
                delivery.Status = StockDeliveryStatus.Closed;

            db.SaveChanges();
        }
    }
}
