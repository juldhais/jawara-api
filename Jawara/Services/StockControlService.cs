﻿using Jawara.Helpers;
using Jawara.Models;
using Jawara.Repositories;
using Jawara.Resources;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Jawara.Services
{
    public class StockControlService
    {
        private readonly DataContext db;

        public StockControlService(DataContext db)
        {
            this.db = db;
        }

        public StockControlResource Get(Guid? id)
        {
            return db.Get<StockControl, StockControlResource>(id);
        }

        public PagingResult<StockControlResource> GetList(Guid? stockControlCategoryId, Guid? itemId, int page, int size)
        {
            var query = db.GetQuery<StockControl>();

            if (stockControlCategoryId.IsGuidNotEmpty())
                query = query.Where(x => x.StockControlCategoryId == stockControlCategoryId);

            if (itemId.IsGuidNotEmpty())
                query = query.Where(x => x.ItemId == itemId);

            var result = query.OrderBy(x => x.Item.Description)
                .ThenBy(x => x.StockControlCategory.Code)
                .PaginateTo<StockControl, StockControlResource>(page, size);

            return result;
        }

        public void ValidateResource(StockControlResource res)
        {
            if (res.StockControlCategoryId.IsGuidEmpty())
                throw new BadRequestException("Stock Control Category cannot be empty.");

            if (res.ItemId.IsEmpty())
                throw new BadRequestException("Item cannot be empty.");

            if (db.GetQuery<StockControl>()
                  .Any(x => x.StockControlCategoryId == res.StockControlCategoryId
                        && x.ItemId == res.ItemId
                        && x.Id != res.Id))
                throw new BadRequestException("Stock Control is already exist.");
        }

        public Guid Create(StockControlResource res, Guid? userId)
        {
            ValidateResource(res);

            var price = new StockControl();
            price.MapFrom(res);
            db.Create(price, userId);
            db.SaveChanges();

            return price.Id;
        }

        public Guid Update(StockControlResource res, Guid? userId)
        {
            ValidateResource(res);

            var price = db.Get<StockControl>(res.Id);
            price.MapFrom(res);
            db.Update(price, userId);
            db.SaveChanges();

            return price.Id;
        }

        public void Delete(Guid? id, Guid? userId)
        {
            db.Delete<StockControl>(id, userId);
            db.SaveChanges();
        }

        public ImportResultResource Import(List<StockControlImportResource> list, Guid? userId)
        {
            var transaction = db.Database.BeginTransaction();

            try
            {
                var result = new ImportResultResource();

                var line = 0;
                foreach (var res in list)
                {
                    line++;

                    if (res.Item.IsEmpty())
                        throw new BadRequestException($"[{line}] Item cannot be empty.");

                    if (res.StockControlCategory.IsEmpty())
                        throw new BadRequestException($"[{line}] Stock Control Category cannot be empty.");

                    var item = db.GetQuery<Item>()
                        .Where(x => x.Code == res.Item || x.Description == res.Item)
                        .FirstOrDefault();

                    if (item == null)
                        throw new BadRequestException($"[{line}] Item {res.Item} not found.");

                    var category = db.GetQuery<StockControlCategory>()
                        .Where(x => x.Code == res.StockControlCategory)
                        .FirstOrDefault();

                    if (category == null)
                        throw new BadRequestException($"[{line}] Stock Control category {res.StockControlCategory} not found.");


                    bool isNew = false;

                    var stockControl = db.GetQuery<StockControl>()
                        .Where(x => x.ItemId == item.Id
                            && x.StockControlCategoryId == category.Id)
                        .FirstOrDefault();

                    if (stockControl == null)
                    {
                        stockControl = new StockControl();
                        isNew = true;
                    }

                    stockControl.ItemId = item?.Id;
                    stockControl.StockControlCategoryId = category?.Id;
                    stockControl.MinimumStock = res.MinimumStock;
                    stockControl.MaximumStock = res.MaximumStock;

                    if (isNew) db.Create(stockControl, userId);
                    else db.Update(stockControl, userId);
                }

                db.SaveChanges();
                transaction.Commit();

                return result;
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
        }
    }
}
