﻿using Jawara.Enums;
using Jawara.Helpers;
using Jawara.Models;
using Jawara.Repositories;
using Jawara.Resources;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Jawara.Services
{
    public class SalesDeliveryService
    {
        private readonly DataContext db;
        private readonly ItemService itemService;
        private readonly SalesOrderService salesOrderService;
        private readonly SalesPaymentService paymentService;

        public SalesDeliveryService(DataContext db,
            ItemService itemService,
            SalesOrderService salesOrderService,
            SalesPaymentService paymentService)
        {
            this.db = db;
            this.itemService = itemService;
            this.salesOrderService = salesOrderService;
            this.paymentService = paymentService;
        }

        public SalesDeliveryResource Get(Guid? id)
        {
            var delivery = db.Get<SalesDelivery, SalesDeliveryResource>(id);
            if (delivery != null)
            {
                var listDetail = db.GetQuery<SalesDeliveryDetail>()
                    .Where(x => x.SalesDeliveryId == id)
                    .OrderBy(x => x.Sequence)
                    .Project().To<SalesDeliveryDetailResource>()
                    .ToList();

                if (listDetail != null) delivery.Details = listDetail;

                var listPayment = db.GetQuery<SalesPaymentDetail>()
                    .Where(x => x.SalesDeliveryId == id
                            && x.SalesPayment.Direct)
                    .OrderBy(x => x.SalesPayment.PaymentMethod.Sequence)
                    .Select(x => new DirectPaymentResource
                    {
                        PaymentId = x.SalesPaymentId,
                        PaymentMethodCode = x.SalesPayment.PaymentMethod.Code,
                        PaymentMethodId = x.SalesPayment.PaymentMethodId,
                        Amount = x.Payment,
                        Remarks = x.SalesPayment.Remarks
                    }).ToList();

                if (listPayment != null) delivery.Payments = listPayment;

                delivery.CreatedBy = db.GetQuery<User>()
                    .Where(x => x.Id == delivery.CreatedById)
                    .Select(x => x.Description)
                    .FirstOrDefault();

                delivery.UpdatedBy = db.GetQuery<User>()
                    .Where(x => x.Id == delivery.UpdatedById)
                    .Select(x => x.Description)
                    .FirstOrDefault();
            }

            return delivery;
        }

        public PagingResult<SalesDeliveryResource> GetList(string keyword, string status, Guid? customerId, Guid? locationId, DateTime? startDate, DateTime? endDate, int page, int size)
        {
            if (keyword.IsEmpty()
                && status.IsEmpty()
                && customerId.IsGuidEmpty()
                && locationId.IsGuidEmpty()
                && startDate == null && endDate == null
                && page == 0 && size == 0)
                return new PagingResult<SalesDeliveryResource>();

            var query = db.GetQuery<SalesDelivery>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.DocumentNumber.Contains(keyword)
                    || x.ReferenceNumber.Contains(keyword)
                    || x.Remarks.Contains(keyword));

            if (status.IsNotEmpty())
                query = query.Where(x => x.Status == status);

            if (customerId.IsGuidNotEmpty())
                query = query.Where(x => x.CustomerId == customerId);

            if (locationId.IsGuidNotEmpty())
                query = query.Where(x => x.LocationId == locationId);

            if (startDate != null)
                query = query.Where(x => x.DocumentDate.Date >= startDate);

            if (endDate != null)
                query = query.Where(x => x.DocumentDate.Date <= endDate);

            query = query.OrderByDescending(x => x.DocumentNumber);

            var result = query.PaginateTo<SalesDelivery, SalesDeliveryResource>(page, size);

            return result;
        }

        private void Calculate(SalesDeliveryResource res)
        {
            res.TotalBeforeTax = 0;
            res.TotalValueAddedTax = 0;
            res.TotalIncomeTax = 0;
            res.TotalOtherTax = 0;
            res.TotalAfterTax = 0;

            foreach (var detail in res.Details)
            {
                var subtotal = detail.Quantity * detail.Price;
                var subtotalAfterDiscount1 = subtotal * (100 - detail.DiscountPercent1) / 100;
                var subtotalAfterDiscount2 = subtotalAfterDiscount1 * (100 - detail.DiscountPercent2) / 100;
                var subtotalAfterDiscountAmount = subtotalAfterDiscount2 - detail.DiscountAmount;
                detail.SubtotalBeforeTax = subtotalAfterDiscountAmount;

                if (detail.ValueAddedTaxId.IsGuidNotEmpty())
                {
                    var vat = db.Get<Tax>(detail.ValueAddedTaxId);
                    detail.ValueAddedTaxAmount = detail.SubtotalBeforeTax * vat.Rate / 100 + vat.Amount;
                }
                if (detail.IncomeTaxId.IsGuidNotEmpty())
                {
                    var ict = db.Get<Tax>(detail.IncomeTaxId);
                    detail.IncomeTaxAmount = detail.SubtotalBeforeTax * ict.Rate / 100 + ict.Amount;
                }
                if (detail.OtherTaxId.IsGuidNotEmpty())
                {
                    var oth = db.Get<Tax>(detail.OtherTaxId);
                    detail.OtherTaxAmount = detail.SubtotalBeforeTax * oth.Rate / 100 + oth.Amount;
                }

                detail.SubtotalAfterTax = detail.SubtotalBeforeTax + detail.ValueAddedTaxAmount + detail.IncomeTaxAmount + detail.OtherTaxAmount;

                res.TotalBeforeTax += detail.SubtotalBeforeTax;
                res.TotalValueAddedTax += detail.ValueAddedTaxAmount;
                res.TotalIncomeTax += detail.IncomeTaxAmount;
                res.TotalOtherTax += detail.OtherTaxAmount;
                res.TotalAfterTax += detail.SubtotalAfterTax;
            }
        }

        private void ValidateResource(SalesDeliveryResource res)
        {
            if (res.DocumentDate == default)
                throw new BadRequestException("Document date cannot be empty.");

            if (res.CustomerId.IsGuidEmpty())
                throw new BadRequestException("Customer cannot be empty.");

            if (res.LocationId.IsGuidEmpty())
                throw new BadRequestException("Location cannot be empty.");
        }

        public string GetNewDocumentNumber(DateTime date)
        {
            var prefix = $"SD-{date:yyMM}";

            var lastNumber = db.GetQuery<SalesDelivery>()
                .Where(x => x.DocumentNumber.StartsWith(prefix))
                .OrderByDescending(x => x.DocumentNumber)
                .Select(x => x.DocumentNumber)
                .FirstOrDefault();

            if (lastNumber == null) return prefix + "0001";

            var newNumber = lastNumber.Substring(prefix.Length, 4).ToInteger() + 1;
            return prefix + newNumber.ToString("0000");
        }

        public Guid Create(SalesDeliveryResource res, Guid? userId)
        {
            ValidateResource(res);

            var transaction = db.Database.BeginTransaction();

            try
            {
                Calculate(res);

                var delivery = new SalesDelivery();
                delivery.MapFrom(res);
                delivery.DocumentNumber = GetNewDocumentNumber(res.DocumentDate);
                delivery.Balance = delivery.TotalAfterTax;
                delivery.Status = PaymentStatus.Unpaid;
                db.Create(delivery, userId);

                // detail
                var sequence = 0;
                foreach (var detail in res.Details)
                {
                    if (detail.ItemId.IsGuidEmpty()) continue;
                    if (detail.Quantity == 0 || detail.Ratio == 0) continue;

                    var item = db.Get<Item>(detail.ItemId);
                    if (item.Stockable)
                    {
                        itemService.AddStockMovementOut(
                            transactionId: delivery.Id,
                            documentNumber: delivery.DocumentNumber,
                            documentDate: delivery.DocumentDate,
                            transactionType: TransactionType.SalesDelivery,
                            itemId: detail.ItemId,
                            locationId: delivery.LocationId,
                            quantity: detail.Quantity * detail.Ratio);
                    }

                    var deliveryDetail = new SalesDeliveryDetail();
                    deliveryDetail.MapFrom(detail);
                    deliveryDetail.SalesDeliveryId = delivery.Id;
                    deliveryDetail.Sequence = ++sequence;
                    db.Create(deliveryDetail, userId);
                }

                db.SaveChanges();

                // direct payment
                foreach (var directPayment in res.Payments)
                {
                    if (directPayment.Amount == 0) continue;

                    var payment = new SalesPaymentResource();
                    payment.Direct = true;
                    payment.DocumentDate = delivery.DocumentDate;
                    payment.LocationId = delivery.LocationId;
                    payment.CustomerId = delivery.CustomerId;
                    payment.PaymentMethodId = directPayment.PaymentMethodId;
                    payment.ReferenceNumber = delivery.DocumentNumber;
                    payment.Remarks = directPayment.Remarks;

                    payment.Details = new List<SalesPaymentDetailResource>();
                    payment.Details.Add(new SalesPaymentDetailResource
                    {
                        SalesDeliveryId = delivery.Id,
                        Payment = directPayment.Amount,
                        Balance = delivery.TotalAfterTax,
                        Remaining = delivery.TotalAfterTax - directPayment.Amount
                    });

                    paymentService.Create(payment, userId);
                }

                salesOrderService.CalculateQuantityDelivered(delivery.SalesOrderId);

                transaction.Commit();

                return delivery.Id;
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
        }

        public Guid Update(SalesDeliveryResource res, Guid? userId)
        {
            ValidateResource(res);

            var transaction = db.Database.BeginTransaction();

            try
            {
                // delete old
                itemService.RemoveStockMovement(res.Id);

                db.Database.ExecuteSqlRaw("DELETE FROM SalesDeliveryDetail WHERE SalesDeliveryId = {0} AND IsDeleted = 0", res.Id);

                var oldDirectPayments = db.GetQuery<PurchasePaymentDetail>()
                   .Where(x => x.PurchaseReceiptId == res.Id
                       && x.PurchasePayment.Direct == true)
                   .Select(x => new
                   {
                       PaymentId = x.PurchasePayment.Id,
                       Amount = x.Payment
                   }).ToList();

                foreach (var oldPayment in oldDirectPayments)
                {
                    db.Database.ExecuteSqlRaw("DELETE FROM SalesPaymentDetail WHERE SalesPaymentId = {0}", oldPayment.PaymentId);
                    db.Database.ExecuteSqlRaw("DELETE FROM SalesPayment WHERE Id = {0}", oldPayment.PaymentId);
                }

                var totalOldPayment = oldDirectPayments.Sum(a => a.Amount);
                var oldDelivery = db.Get<SalesDelivery>(res.Id);
                oldDelivery.Balance += totalOldPayment;
                db.SaveChanges();

                var oldSalesOrderId = db.GetQuery<SalesDelivery>()
                    .Where(x => x.Id == res.Id)
                    .Select(x => x.SalesOrderId)
                    .FirstOrDefault();
                salesOrderService.CalculateQuantityDelivered(oldSalesOrderId);


                //update
                Calculate(res);

                var delivery = db.Get<SalesDelivery>(res.Id);
                delivery.MapFrom(res);
                db.Update(delivery, userId);

                var sequence = 0;
                foreach (var detail in res.Details)
                {
                    if (detail.ItemId.IsGuidEmpty()) continue;
                    if (detail.Quantity == 0) continue;

                    var item = db.Get<Item>(detail.ItemId);
                    if (item.Stockable)
                    {
                        itemService.AddStockMovementOut(
                            transactionId: delivery.Id,
                            documentNumber: delivery.DocumentNumber,
                            documentDate: delivery.DocumentDate,
                            transactionType: TransactionType.SalesDelivery,
                            itemId: detail.ItemId,
                            locationId: delivery.LocationId,
                            quantity: detail.Quantity * detail.Ratio);
                    }

                    var deliveryDetail = new SalesDeliveryDetail();
                    deliveryDetail.MapFrom(detail);
                    deliveryDetail.SalesDeliveryId = delivery.Id;
                    deliveryDetail.Sequence = ++sequence;

                    var lastPurchaseDate = db.GetQuery<PurchaseReceiptDetail>()
                        .Where(x => x.ItemId == deliveryDetail.ItemId)
                        .OrderByDescending(x => x.PurchaseReceipt.DocumentDate)
                        .Select(x => x.PurchaseReceipt.DocumentDate)
                        .FirstOrDefault();

                    db.Create(deliveryDetail, userId);
                }

                // direct payment
                foreach (var directPayment in res.Payments)
                {
                    if (directPayment.Amount == 0) continue;

                    var payment = new SalesPaymentResource();
                    payment.Direct = true;
                    payment.DocumentDate = delivery.DocumentDate;
                    payment.LocationId = delivery.LocationId;
                    payment.CustomerId = delivery.CustomerId;
                    payment.PaymentMethodId = directPayment.PaymentMethodId;
                    payment.ReferenceNumber = delivery.DocumentNumber;
                    payment.Remarks = directPayment.Remarks;

                    payment.Details = new List<SalesPaymentDetailResource>();
                    payment.Details.Add(new SalesPaymentDetailResource
                    {
                        SalesDeliveryId = delivery.Id,
                        Payment = directPayment.Amount,
                        Balance = delivery.TotalAfterTax,
                        Remaining = delivery.TotalAfterTax - directPayment.Amount
                    });

                    paymentService.Create(payment, userId);
                }

                db.SaveChanges();

                salesOrderService.CalculateQuantityDelivered(delivery.SalesOrderId);
                CalculateBalance(delivery.Id);

                transaction.Commit();

                return res.Id;
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
        }

        public void Delete(Guid? id, Guid? userId)
        {
            var transaction = db.Database.BeginTransaction();

            try
            {
                var paymentExist = db.GetQuery<SalesPaymentDetail>()
                    .Any(x => x.SalesDeliveryId == id);

                if (paymentExist) throw new BadRequestException("Unable to delete Sales Delivery because it is already paid.");

                // delete old
                itemService.RemoveStockMovement(id);
                db.Delete<SalesDeliveryDetail>(x => x.SalesDeliveryId == id, userId);
                db.Delete<SalesDelivery>(id, userId);
                db.SaveChanges();

                var oldSalesOrderId = db.GetQuery<SalesDelivery>()
                    .Where(x => x.Id == id)
                    .Select(x => x.SalesOrderId)
                    .FirstOrDefault();
                salesOrderService.CalculateQuantityDelivered(oldSalesOrderId);

                transaction.Commit();
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
        }

        private void CalculateBalance(Guid? salesDeliveryId)
        {
            var delivery = db.Get<SalesDelivery>(salesDeliveryId);
            delivery.TotalPayment = db.GetQuery<SalesPaymentDetail>()
                .Where(x => x.SalesDeliveryId == salesDeliveryId)
                .Sum(x => x.Payment);

            delivery.Balance = delivery.TotalAfterTax - delivery.TotalPayment;
            delivery.Status = delivery.Balance > 0 ? PaymentStatus.Unpaid : PaymentStatus.Paid;

            db.SaveChanges();
        }

        public List<SalesPaymentDetailResource> GetListPayment(Guid? id)
        {
            return db.GetQuery<SalesPaymentDetail>()
                .Where(x => x.SalesDeliveryId == id)
                .OrderBy(x => x.SalesPayment.DocumentDate)
                .Project().To<SalesPaymentDetailResource>()
                .ToList();
        }
    }
}
