﻿using Jawara.Enums;
using Jawara.Helpers;
using Jawara.Models;
using Jawara.Repositories;
using Jawara.Resources;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Jawara.Services
{
    public class PurchasePaymentService
    {
        private readonly DataContext db;

        public PurchasePaymentService(DataContext db)
        {
            this.db = db;
        }

        public PurchasePaymentResource Get(Guid? id)
        {
            var payment = db.Get<PurchasePayment, PurchasePaymentResource>(id);
            if (payment != null)
            {
                payment.Details = db.PurchasePaymentDetail
                    .Where(x => x.IsDeleted == false
                                && x.PurchasePaymentId == id)
                    .OrderBy(x => x.PurchaseReceipt.DocumentNumber)
                    .Project().To<PurchasePaymentDetailResource>()
                    .ToList();

                if (payment.Details == null)
                    payment.Details = new List<PurchasePaymentDetailResource>();

                payment.Returns = db.PurchaseReturn
                    .Where(x => x.IsDeleted == false
                            && x.PurchasePaymentId == id)
                    .OrderBy(x => x.DocumentNumber)
                    .Project().To<PurchasePaymentReturnResource>()
                    .ToList();

                if (payment.Returns == null)
                    payment.Returns = new List<PurchasePaymentReturnResource>();
                else
                    payment.Returns.ForEach(x => x.Check = true);

                payment.CreatedBy = db.GetQuery<User>()
                    .Where(x => x.Id == payment.CreatedById)
                    .Select(x => x.Description)
                    .FirstOrDefault();

                payment.UpdatedBy = db.GetQuery<User>()
                    .Where(x => x.Id == payment.UpdatedById)
                    .Select(x => x.Description)
                    .FirstOrDefault();
            }

            return payment;
        }

        public PagingResult<PurchasePaymentResource> GetList(string keyword, Guid? supplierId, Guid? locationId, DateTime? startDate, DateTime? endDate, int page, int size)
        {
            if (keyword.IsEmpty()
                && supplierId.IsGuidEmpty()
                && locationId.IsGuidEmpty()
                && startDate == null && endDate == null
                && page == 0 && size == 0)
                return new PagingResult<PurchasePaymentResource>();

            var query = db.GetQuery<PurchasePayment>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.DocumentNumber.Contains(keyword)
                    || x.ReferenceNumber.Contains(keyword)
                    || x.Remarks.Contains(keyword));

            if (supplierId.IsGuidNotEmpty())
                query = query.Where(x => x.SupplierId == supplierId);

            if (locationId.IsGuidNotEmpty())
                query = query.Where(x => x.LocationId == locationId);

            if (startDate != null)
                query = query.Where(x => x.DocumentDate.Date >= startDate);

            if (endDate != null)
                query = query.Where(x => x.DocumentDate.Date <= endDate);

            query = query.OrderByDescending(x => x.DocumentNumber);

            var result = query.PaginateTo<PurchasePayment, PurchasePaymentResource>(page, size);

            return result;
        }

        private void Calculate(PurchasePaymentResource res)
        {
            res.TotalPayment = res.Details.Sum(x => x.Payment);
            res.TotalReturn = res.Returns.Sum(x => x.TotalAfterTax);
            res.AmountPaid = res.TotalPayment - res.TotalReturn;
        }

        private void ValidateResource(PurchasePaymentResource res)
        {
            if (res.DocumentDate == default)
                throw new BadRequestException("Document date cannot be empty.");

            if (res.SupplierId.IsGuidEmpty())
                throw new BadRequestException("Supplier cannot be empty.");

            if (res.LocationId.IsGuidEmpty())
                throw new BadRequestException("Location cannot be empty.");
        }

        public string GetNewDocumentNumber(DateTime date)
        {
            var prefix = $"PP-{date:yyMM}";

            var lastNumber = db.GetQuery<PurchasePayment>()
                .Where(x => x.DocumentNumber.StartsWith(prefix))
                .OrderByDescending(x => x.DocumentNumber)
                .Select(x => x.DocumentNumber)
                .FirstOrDefault();

            if (lastNumber == null) return prefix + "0001";

            var newNumber = lastNumber.Substring(prefix.Length, 4).ToInteger() + 1;
            return prefix + newNumber.ToString("0000");
        }

        public Guid Create(PurchasePaymentResource res, Guid? userId)
        {
            ValidateResource(res);

            Calculate(res);

            var payment = new PurchasePayment();
            payment.MapFrom(res);
            payment.DocumentNumber = GetNewDocumentNumber(res.DocumentDate);
            db.Create(payment, userId);

            foreach (var detail in res.Details)
            {
                if (detail.Payment == 0) continue;
                if (detail.PurchaseReceiptId.IsGuidEmpty()) continue;

                var paymentDetail = new PurchasePaymentDetail();
                paymentDetail.MapFrom(detail);
                paymentDetail.PurchasePaymentId = payment.Id;
                db.Create(paymentDetail, userId);

                var receipt = db.Get<PurchaseReceipt>(detail.PurchaseReceiptId);
                receipt.TotalPayment += detail.Payment;
                receipt.Balance -= detail.Payment;

                if (receipt.Balance < 0)
                    throw new BadRequestException($"Payment amount for {receipt.DocumentNumber} exceeds the remaining balance.");

                receipt.Status = receipt.Balance == 0 ? PaymentStatus.Paid : PaymentStatus.Unpaid;
            }

            foreach (var paymentRet in res.Returns)
            {
                if (paymentRet.Check)
                {
                    var ret = db.Get<PurchaseReturn>(paymentRet.Id);
                    ret.Status = ReturnStatus.Used;
                    ret.PurchasePaymentId = payment.Id;
                }
            }

            db.SaveChanges();

            return payment.Id;
        }

        public Guid Update(PurchasePaymentResource res, Guid? userId)
        {
            ValidateResource(res);

            var transaction = db.Database.BeginTransaction();

            try
            {
                // delete old
                var listOldDetail = db.GetQuery<PurchasePaymentDetail>()
                    .Where(x => x.PurchasePaymentId == res.Id)
                    .ToList();

                foreach (var oldDetail in listOldDetail)
                {
                    var receipt = db.Get<PurchaseReceipt>(oldDetail.PurchaseReceiptId);
                    receipt.TotalPayment -= oldDetail.Payment;
                    receipt.Balance += oldDetail.Payment;
                    receipt.Status = PaymentStatus.Unpaid;
                }

                db.SaveChanges();

                db.Database.ExecuteSqlRaw("UPDATE PurchaseReturn SET Status = 'Unused' WHERE PurchasePaymentId = {0} AND IsDeleted = 0", res.Id);
                db.Database.ExecuteSqlRaw("DELETE FROM PurchasePaymentDetail WHERE PurchasePaymentId = {0} AND IsDeleted = 0", res.Id);


                //update
                Calculate(res);

                var payment = db.Get<PurchasePayment>(res.Id);

                if (payment == null) throw new BadRequestException("Purchase Payment not found.");

                payment.MapFrom(res);
                db.Update(payment, userId);

                foreach (var detail in res.Details)
                {
                    if (detail.Payment == 0) continue;
                    if (detail.PurchaseReceiptId.IsGuidEmpty()) continue;

                    var paymentDetail = new PurchasePaymentDetail();
                    paymentDetail.MapFrom(detail);
                    paymentDetail.PurchasePaymentId = payment.Id;
                    db.Create(paymentDetail, userId);

                    var receipt = db.Get<PurchaseReceipt>(detail.PurchaseReceiptId);
                    receipt.TotalPayment += detail.Payment;
                    receipt.Balance -= detail.Payment;

                    if (receipt.Balance < 0)
                        throw new BadRequestException($"Payment amount for {receipt.DocumentNumber} exceeds the remaining balance.");

                    receipt.Status = receipt.Balance == 0 ? PaymentStatus.Paid : PaymentStatus.Unpaid;
                }

                foreach (var paymentRet in res.Returns)
                {
                    if (paymentRet.Check)
                    {
                        var ret = db.Get<PurchaseReturn>(paymentRet.Id);
                        ret.Status = ReturnStatus.Used;
                        ret.PurchasePaymentId = payment.Id;
                    }
                }

                db.SaveChanges();

                transaction.Commit();

                return res.Id;
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
        }

        public void Delete(Guid? id, Guid? userId)
        {
            var transaction = db.Database.BeginTransaction();

            try
            {
                // delete old
                var listOldDetail = db.GetQuery<PurchasePaymentDetail>()
                    .Where(x => x.PurchasePaymentId == id)
                    .ToList();

                foreach (var oldDetail in listOldDetail)
                {
                    var receipt = db.Get<PurchaseReceipt>(oldDetail.PurchaseReceiptId);
                    receipt.TotalPayment -= oldDetail.Payment;
                    receipt.Balance += oldDetail.Payment;
                    receipt.Status = PaymentStatus.Unpaid;
                }

                db.Database.ExecuteSqlRaw("UPDATE PurchaseReturn SET Status = 'Unused' WHERE PurchasePaymentId = {0} AND IsDeleted = 0", id);

                db.Delete<PurchasePaymentDetail>(x => x.PurchasePaymentId == id, userId);
                db.Delete<PurchasePayment>(id, userId);

                db.SaveChanges();

                transaction.Commit();
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
        }
    }
}
