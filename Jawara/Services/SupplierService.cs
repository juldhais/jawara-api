﻿using Jawara.Helpers;
using Jawara.Models;
using Jawara.Repositories;
using Jawara.Resources;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Jawara.Services
{
    public class SupplierService
    {
        private readonly DataContext db;

        public SupplierService(DataContext db)
        {
            this.db = db;
        }

        public SupplierResource Get(Guid? id)
        {
            return db.Get<Supplier, SupplierResource>(id);
        }

        public SupplierResource Get(string code)
        {
            return db.GetQuery<Supplier>()
                .Where(x => x.Code == code)
                .Project().To<SupplierResource>()
                .FirstOrDefault();
        }

        public PagingResult<SupplierResource> GetList(string keyword, int page, int size)
        {
            var query = db.GetQuery<Supplier>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.Code == keyword
                    || x.Description.Contains(keyword)
                    || x.Phone1.Contains(keyword)
                    || x.Phone2.Contains(keyword));

            var result = query.OrderBy(x => x.Description)
                .PaginateTo<Supplier, SupplierResource>(page, size);

            return result;
        }

        public List<LookupResource> GetLookup(string keyword)
        {
            if (keyword.IsEmpty() || keyword.Length < 2)
                return new List<LookupResource>();

            var query = db.GetQuery<Supplier>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.Code == keyword
                    || x.Description.Contains(keyword));

            var result = query.OrderBy(x => x.Code)
                .Select(x => new LookupResource
                {
                    Id = x.Id,
                    Code = x.Code + " ─ " + x.Description
                }).ToList();

            return result;
        }

        public LookupResource GetSingleLookup(Guid? id)
        {
            var result = db.GetQuery<Supplier>()
                .Where(x => x.Id == id)
                .Select(x => new LookupResource
                {
                    Id = x.Id,
                    Code = x.Code + " ─ " + x.Description
                }).FirstOrDefault();

            return result;
        }

        public List<SupplierResource> Search(string keyword)
        {
            if (keyword.IsEmpty() || keyword.Length < 3)
                return new List<SupplierResource>();

            var query = db.GetQuery<Supplier>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.Code == keyword
                    || x.Description.Contains(keyword)
                    || x.Phone1.Contains(keyword)
                    || x.Phone2.Contains(keyword));

            var result = query.OrderBy(x => x.Description)
                .Project().To<SupplierResource>()
                .ToList();

            return result;
        }

        public void ValidateResource(SupplierResource res)
        {
            if (res.Code.IsEmpty())
                throw new BadRequestException("Code cannot be empty.");

            if (res.Description.IsEmpty())
                throw new BadRequestException("Code cannot be empty.");

            if (db.GetQuery<Supplier>().Any(x => x.Code == res.Code && x.Id != res.Id))
                throw new BadRequestException("Code is already used.");
        }

        public Guid Create(SupplierResource res, Guid? userId)
        {
            ValidateResource(res);

            var supplier = new Supplier();
            supplier.MapFrom(res);
            db.Create(supplier, userId);
            db.SaveChanges();

            return supplier.Id;
        }

        public Guid Update(SupplierResource res, Guid? userId)
        {
            ValidateResource(res);

            var supplier = db.Get<Supplier>(res.Id);
            supplier.MapFrom(res);
            db.Update(supplier, userId);
            db.SaveChanges();

            return supplier.Id;
        }

        public void Delete(Guid? id, Guid? userId)
        {
            db.Delete<Supplier>(id, userId);
            db.SaveChanges();
        }

        public ImportResultResource Import(List<SupplierImportResource> list, Guid? userId)
        {
            var transaction = db.Database.BeginTransaction();

            try
            {
                var result = new ImportResultResource();

                var line = 0;
                foreach (var supplierRes in list)
                {
                    line++;

                    if (supplierRes.Code.IsEmpty() && supplierRes.Description.IsEmpty()) continue;

                    if (supplierRes.Code.IsEmpty())
                        result.AddErrorMessage($"[{line}] Supplier code cannot be empty.");

                    if (supplierRes.Description.IsEmpty())
                        result.AddErrorMessage($"[{line}] Supplier name cannot be empty.");

                    if (result.IsError) continue;

                    bool isNew = false;

                    var supplier = db.GetQuery<Supplier>().FirstOrDefault(x => x.Code == supplierRes.Code);

                    if (supplier == null)
                    {
                        supplier = new Supplier();
                        isNew = true;
                    }

                    supplier.MapFrom(supplierRes);

                    if (supplierRes.Category.IsNotEmpty())
                    {
                        var category = db.GetQuery<SupplierCategory>()
                            .Where(x => x.Code == supplierRes.Category)
                            .FirstOrDefault();

                        if (category == null)
                        {
                            category = new SupplierCategory();
                            category.Code = supplierRes.Category;
                            db.Create(category, userId);
                            db.SaveChanges();
                        }

                        supplier.SupplierCategoryId = category?.Id;
                    }

                    if (isNew) db.Create(supplier, userId);
                    else db.Update(supplier, userId);
                }

                db.SaveChanges();
                transaction.Commit();

                return result;
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
        }
    }
}
