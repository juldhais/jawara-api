﻿using Jawara.Helpers;
using Jawara.Models;
using Jawara.Repositories;
using Jawara.Resources;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Jawara.Services
{
    public class CustomerService
    {
        private readonly DataContext db;

        public CustomerService(DataContext db)
        {
            this.db = db;
        }

        public CustomerResource Get(Guid? id)
        {
            return db.Get<Customer, CustomerResource>(id);
        }

        public CustomerResource Get(string code)
        {
            return db.GetQuery<Customer>()
                .Where(x => x.Code == code)
                .Project().To<CustomerResource>()
                .FirstOrDefault();
        }

        public PagingResult<CustomerResource> GetList(string keyword, int page, int size)
        {
            var query = db.GetQuery<Customer>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.Code == keyword
                    || x.Description.Contains(keyword)
                    || x.Phone1.Contains(keyword)
                    || x.Phone2.Contains(keyword));

            var result = query.OrderBy(x => x.Description)
                .PaginateTo<Customer, CustomerResource>(page, size);

            return result;
        }

        public List<LookupResource> GetLookup(string keyword)
        {
            if (keyword.IsEmpty() || keyword.Length < 2)
                return new List<LookupResource>();

            var query = db.GetQuery<Customer>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.Code == keyword
                    || x.Description.Contains(keyword));

            var result = query.OrderBy(x => x.Code)
                .Select(x => new LookupResource
                {
                    Id = x.Id,
                    Code = x.Code + " ─ " + x.Description
                }).ToList();

            return result;
        }

        public LookupResource GetSingleLookup(Guid? id)
        {
            var result = db.GetQuery<Customer>()
                .Where(x => x.Id == id)
                .Select(x => new LookupResource
                {
                    Id = x.Id,
                    Code = x.Code + " ─ " + x.Description
                }).FirstOrDefault();

            return result;
        }

        public void ValidateResource(CustomerResource res)
        {
            if (res.Code.IsEmpty())
                throw new BadRequestException("Code cannot be empty.");

            if (res.Description.IsEmpty())
                throw new BadRequestException("Code cannot be empty.");

            if (db.GetQuery<Customer>().Any(x => x.Code == res.Code && x.Id != res.Id))
                throw new BadRequestException("Code is already used.");
        }

        public Guid Create(CustomerResource res, Guid? userId)
        {
            ValidateResource(res);

            var customer = new Customer();
            customer.MapFrom(res);
            db.Create(customer, userId);
            db.SaveChanges();

            return customer.Id;
        }

        public Guid Update(CustomerResource res, Guid? userId)
        {
            ValidateResource(res);

            var customer = db.Get<Customer>(res.Id);
            customer.MapFrom(res);
            db.Update(customer, userId);
            db.SaveChanges();

            return customer.Id;
        }

        public void Delete(Guid? id, Guid? userId)
        {
            db.Delete<Customer>(id, userId);
            db.SaveChanges();
        }

        public ImportResultResource Import(List<CustomerImportResource> list, Guid? userId)
        {
            var transaction = db.Database.BeginTransaction();

            try
            {
                var result = new ImportResultResource();

                var line = 0;
                foreach (var customerRes in list)
                {
                    line++;

                    if (customerRes.Code.IsEmpty() && customerRes.Description.IsEmpty()) continue;

                    if (customerRes.Code.IsEmpty())
                        result.AddErrorMessage($"[{line}] Customer code cannot be empty.");

                    if (customerRes.Description.IsEmpty())
                        result.AddErrorMessage($"[{line}] Customer name cannot be empty.");

                    if (result.IsError) continue;

                    bool isNew = false;

                    var customer = db.GetQuery<Customer>().FirstOrDefault(x => x.Code == customerRes.Code);

                    if (customer == null)
                    {
                        customer = new Customer();
                        isNew = true;
                    }

                    customer.MapFrom(customerRes);

                    if (customerRes.Location.IsNotEmpty())
                    {
                        customer.LocationId = db.GetQuery<Location>()
                            .Where(x => x.Description == customerRes.Location)
                            .Select(x => x.Id)
                            .FirstOrDefault();

                        if (customer.LocationId.IsGuidEmpty())
                            throw new BadRequestException($"[{line}] Location {customerRes.Location} not found.");
                    }

                    if (customerRes.Category.IsNotEmpty())
                    {
                        var category = db.GetQuery<CustomerCategory>()
                            .Where(x => x.Code == customerRes.Category)
                            .FirstOrDefault();

                        if (category == null)
                        {
                            category = new CustomerCategory();
                            category.Code = customerRes.Category;
                            db.Create(category, userId);
                            db.SaveChanges();
                        }
                        customer.CustomerCategoryId = category?.Id;
                    }

                    if (customerRes.PriceControlCategory.IsNotEmpty())
                    {
                        var priceControlCategory = db.GetQuery<PriceControlCategory>()
                            .Where(x => x.Code == customerRes.PriceControlCategory)
                            .FirstOrDefault();
                        if (priceControlCategory == null)
                        {
                            priceControlCategory = new PriceControlCategory();
                            priceControlCategory.Code = customerRes.PriceControlCategory;
                            db.Create(priceControlCategory, userId);
                            db.SaveChanges();
                        }
                        customer.PriceControlCategoryId = priceControlCategory?.Id;
                    }

                    if (isNew) db.Create(customer, userId);
                    else db.Update(customer, userId);
                }

                db.SaveChanges();
                transaction.Commit();

                return result;
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
        }
    }
}
