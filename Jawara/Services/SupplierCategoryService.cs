﻿using Jawara.Helpers;
using Jawara.Models;
using Jawara.Repositories;
using Jawara.Resources;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Jawara.Services
{
    public class SupplierCategoryService
    {
        private readonly DataContext db;

        public SupplierCategoryService(DataContext db)
        {
            this.db = db;
        }

        public SupplierCategoryResource Get(Guid? id)
        {
            return db.Get<SupplierCategory, SupplierCategoryResource>(id);
        }

        public PagingResult<SupplierCategoryResource> GetList(string keyword, int page, int size)
        {
            var query = db.GetQuery<SupplierCategory>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.Code.Contains(keyword));

            var result = query.OrderBy(x => x.Code)
                .PaginateTo<SupplierCategory, SupplierCategoryResource>(page, size);

            return result;
        }

        public List<LookupResource> GetLookup(string keyword)
        {
            if (keyword.IsEmpty() || keyword.Length < 2)
                return new List<LookupResource>();

            var query = db.GetQuery<SupplierCategory>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.Code.Contains(keyword));

            var result = query.OrderBy(x => x.Code)
                .Select(x => new LookupResource
                {
                    Id = x.Id,
                    Code = x.Code
                }).ToList();

            return result;
        }

        public LookupResource GetSingleLookup(Guid? id)
        {
            var result = db.GetQuery<SupplierCategory>()
                .Where(x => x.Id == id)
                .Select(x => new LookupResource
                {
                    Id = x.Id,
                    Code = x.Code
                }).FirstOrDefault();

            return result;
        }

        public List<SupplierCategoryResource> Search(string keyword)
        {
            var query = db.GetQuery<SupplierCategory>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.Code.Contains(keyword));

            var result = query.OrderBy(x => x.Code)
                .Project().To<SupplierCategoryResource>()
                .ToList();

            return result;
        }

        public void ValidateResource(SupplierCategoryResource res)
        {
            if (res.Code.IsEmpty())
                throw new BadRequestException("Code cannot be empty.");

            if (db.GetQuery<SupplierCategory>().Any(x => x.Code == res.Code && x.Id != res.Id))
                throw new BadRequestException("Code is already used.");
        }

        public Guid Create(SupplierCategoryResource res, Guid? userId)
        {
            ValidateResource(res);

            var category = new SupplierCategory();
            category.MapFrom(res);
            db.Create(category, userId);
            db.SaveChanges();

            return category.Id;
        }

        public Guid Update(SupplierCategoryResource res, Guid? userId)
        {
            ValidateResource(res);

            var category = db.Get<SupplierCategory>(res.Id);
            category.MapFrom(res);
            db.Update(category, userId);
            db.SaveChanges();

            return category.Id;
        }

        public void Delete(Guid? id, Guid? userId)
        {
            db.Delete<SupplierCategory>(id, userId);
            db.SaveChanges();
        }
    }
}
