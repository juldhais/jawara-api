﻿using Jawara.Enums;
using Jawara.Repositories;
using Jawara.Resources;
using System;
using System.Text;

namespace Jawara.Services
{
    public class PurchaseOrderPrintService
    {
        private readonly DataContext db;
        private readonly ConfigurationService configService;
        private readonly PurchaseOrderService purchaseOrderService;

        private ConfigurationEditResource configEdit;

        public PurchaseOrderPrintService(DataContext db,
            ConfigurationService configService,
            PurchaseOrderService purchaseOrderService)
        {
            this.db = db;
            this.configService = configService;
            this.purchaseOrderService = purchaseOrderService;
            this.configEdit = configService.GetEdit();
        }

        public object Print(Guid? id)
        {
            var order = purchaseOrderService.Get(id);

            if (configEdit.PrintType == PrintType.Receipt30)
                return PrintReceipt30(order);

            else return PrintReceipt30(order);

            //return null;
        }

        private object PrintReceipt30(PurchaseOrderResource res)
        {
            var builder = new StringBuilder();

            builder.AppendLine("<html><body style='font-family: Consolas, Courier New'>");
            builder.AppendLine($"#{res.DocumentNumber}<br/>");
            builder.AppendLine($"{res.DocumentDate:dd/MM/yyyy}<br/>");
            builder.AppendLine($"{res.SupplierCode}<br/>");
            builder.AppendLine("<br/>");

            foreach (var detail in res.Details)
            {
                builder.AppendLine($"{detail.ItemDescription}<br/>");
                builder.AppendLine($"     {detail.Quantity:#,#0.##}   {detail.Price:#,#0.##}   {detail.SubtotalAfterTax:#,#0.##}<br/>");
            }

            builder.AppendLine("<br/>");
            builder.AppendLine($"TOTAL: {res.TotalAfterTax:#,#0.##}<br/>");

            builder.AppendLine("</body></html>");

            return builder.ToString();
        }
    }
}
