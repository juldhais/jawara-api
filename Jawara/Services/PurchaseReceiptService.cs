﻿using Jawara.Enums;
using Jawara.Helpers;
using Jawara.Models;
using Jawara.Repositories;
using Jawara.Resources;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Jawara.Services
{
    public class PurchaseReceiptService
    {
        private readonly DataContext db;
        private readonly ItemService itemService;
        private readonly PurchaseOrderService purchaseOrderService;
        private readonly PurchasePaymentService paymentService;

        public PurchaseReceiptService(DataContext db,
            ItemService itemService,
            PurchaseOrderService purchaseOrderService,
            PurchasePaymentService paymentService)
        {
            this.db = db;
            this.itemService = itemService;
            this.purchaseOrderService = purchaseOrderService;
            this.paymentService = paymentService;
        }

        public PurchaseReceiptResource Get(Guid? id)
        {
            var receipt = db.Get<PurchaseReceipt, PurchaseReceiptResource>(id);
            if (receipt != null)
            {
                var listDetail = db.GetQuery<PurchaseReceiptDetail>()
                    .Where(x => x.PurchaseReceiptId == id)
                    .OrderBy(x => x.Sequence)
                    .Project().To<PurchaseReceiptDetailResource>()
                    .ToList();

                if (listDetail != null) receipt.Details = listDetail;

                var listPayment = db.GetQuery<PurchasePaymentDetail>()
                    .Where(x => x.PurchaseReceiptId == id
                            && x.PurchasePayment.Direct)
                    .OrderBy(x => x.PurchasePayment.PaymentMethod.Sequence)
                    .Select(x => new DirectPaymentResource
                    {
                        PaymentId = x.PurchasePaymentId,
                        PaymentMethodCode = x.PurchasePayment.PaymentMethod.Code,
                        PaymentMethodId = x.PurchasePayment.PaymentMethodId,
                        Amount = x.Payment,
                        Remarks = x.PurchasePayment.Remarks
                    }).ToList();

                if (listPayment != null) receipt.Payments = listPayment;

                receipt.CreatedBy = db.GetQuery<User>()
                    .Where(x => x.Id == receipt.CreatedById)
                    .Select(x => x.Description)
                    .FirstOrDefault();

                receipt.UpdatedBy = db.GetQuery<User>()
                    .Where(x => x.Id == receipt.UpdatedById)
                    .Select(x => x.Description)
                    .FirstOrDefault();
            }

            return receipt;
        }

        public PagingResult<PurchaseReceiptResource> GetList(string keyword, string status, Guid? supplierId, Guid? locationId, DateTime? startDate, DateTime? endDate, int page, int size)
        {
            if (keyword.IsEmpty()
                && status.IsEmpty()
                && supplierId.IsGuidEmpty()
                && locationId.IsGuidEmpty()
                && startDate == null && endDate == null
                && page == 0 && size == 0)
                return new PagingResult<PurchaseReceiptResource>();

            var query = db.GetQuery<PurchaseReceipt>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.DocumentNumber.Contains(keyword)
                    || x.ReferenceNumber.Contains(keyword)
                    || x.Remarks.Contains(keyword));

            if (status.IsNotEmpty())
                query = query.Where(x => x.Status == status);

            if (supplierId.IsGuidNotEmpty())
                query = query.Where(x => x.SupplierId == supplierId);

            if (locationId.IsGuidNotEmpty())
                query = query.Where(x => x.LocationId == locationId);

            if (startDate != null)
                query = query.Where(x => x.DocumentDate.Date >= startDate);

            if (endDate != null)
                query = query.Where(x => x.DocumentDate.Date <= endDate);

            query = query.OrderByDescending(x => x.DocumentNumber);

            var result = query.PaginateTo<PurchaseReceipt, PurchaseReceiptResource>(page, size);

            return result;
        }

        private void Calculate(PurchaseReceiptResource res)
        {
            res.TotalBeforeTax = 0;
            res.TotalValueAddedTax = 0;
            res.TotalIncomeTax = 0;
            res.TotalOtherTax = 0;
            res.TotalAfterTax = 0;

            foreach (var detail in res.Details)
            {
                var subtotal = detail.Quantity * detail.Price;
                var subtotalAfterDiscount1 = subtotal * (100 - detail.DiscountPercent1) / 100;
                var subtotalAfterDiscount2 = subtotalAfterDiscount1 * (100 - detail.DiscountPercent2) / 100;
                var subtotalAfterDiscountAmount = subtotalAfterDiscount2 - detail.DiscountAmount;
                detail.SubtotalBeforeTax = subtotalAfterDiscountAmount;

                if (detail.ValueAddedTaxId.IsGuidNotEmpty())
                {
                    var vat = db.Get<Tax>(detail.ValueAddedTaxId);
                    detail.ValueAddedTaxAmount = detail.SubtotalBeforeTax * vat.Rate / 100 + vat.Amount;
                }
                if (detail.IncomeTaxId.IsGuidNotEmpty())
                {
                    var ict = db.Get<Tax>(detail.IncomeTaxId);
                    detail.IncomeTaxAmount = detail.SubtotalBeforeTax * ict.Rate / 100 + ict.Amount;
                }
                if (detail.OtherTaxId.IsGuidNotEmpty())
                {
                    var oth = db.Get<Tax>(detail.OtherTaxId);
                    detail.OtherTaxAmount = detail.SubtotalBeforeTax * oth.Rate / 100 + oth.Amount;
                }

                detail.SubtotalAfterTax = detail.SubtotalBeforeTax + detail.ValueAddedTaxAmount + detail.IncomeTaxAmount + detail.OtherTaxAmount;

                res.TotalBeforeTax += detail.SubtotalBeforeTax;
                res.TotalValueAddedTax += detail.ValueAddedTaxAmount;
                res.TotalIncomeTax += detail.IncomeTaxAmount;
                res.TotalOtherTax += detail.OtherTaxAmount;
                res.TotalAfterTax += detail.SubtotalAfterTax;
            }
        }

        private void ValidateResource(PurchaseReceiptResource res)
        {
            if (res.DocumentDate == default)
                throw new BadRequestException("Document date cannot be empty.");

            if (res.SupplierId.IsGuidEmpty())
                throw new BadRequestException("Supplier cannot be empty.");

            if (res.LocationId.IsGuidEmpty())
                throw new BadRequestException("Location cannot be empty.");
        }

        public string GetNewDocumentNumber(DateTime date)
        {
            var prefix = $"PR-{date:yyMM}";

            var lastNumber = db.GetQuery<PurchaseReceipt>()
                .Where(x => x.DocumentNumber.StartsWith(prefix))
                .OrderByDescending(x => x.DocumentNumber)
                .Select(x => x.DocumentNumber)
                .FirstOrDefault();

            if (lastNumber == null) return prefix + "0001";

            var newNumber = lastNumber.Substring(prefix.Length, 4).ToInteger() + 1;
            return prefix + newNumber.ToString("0000");
        }

        public Guid Create(PurchaseReceiptResource res, Guid? userId)
        {
            ValidateResource(res);

            var transaction = db.Database.BeginTransaction();

            try
            {
                Calculate(res);

                var receipt = new PurchaseReceipt();
                receipt.MapFrom(res);
                receipt.DocumentNumber = GetNewDocumentNumber(res.DocumentDate);
                receipt.Balance = receipt.TotalAfterTax;
                receipt.Status = PaymentStatus.Unpaid;
                db.Create(receipt, userId);

                // detail
                var sequence = 0;
                foreach (var detail in res.Details)
                {
                    if (detail.ItemId.IsGuidEmpty()) continue;
                    if (detail.Quantity == 0 || detail.Ratio == 0) continue;

                    var receiptDetail = new PurchaseReceiptDetail();
                    receiptDetail.MapFrom(detail);
                    receiptDetail.PurchaseReceiptId = receipt.Id;
                    receiptDetail.Sequence = ++sequence;
                    db.Create(receiptDetail, userId);

                    var item = db.Get<Item>(detail.ItemId);
                    item.PurchasePrice = receiptDetail.SubtotalBeforeTax / receiptDetail.Quantity / receiptDetail.Ratio;

                    if (item.Stockable)
                    {
                        itemService.AddStockMovementIn(
                            transactionId: receipt.Id,
                            documentNumber: receipt.DocumentNumber,
                            documentDate: receipt.DocumentDate,
                            transactionType: TransactionType.PurchaseReceipt,
                            itemId: receiptDetail.ItemId,
                            locationId: receipt.LocationId,
                            quantity: receiptDetail.Quantity * receiptDetail.Ratio,
                            cost: receiptDetail.SubtotalBeforeTax / receiptDetail.Quantity / receiptDetail.Ratio);
                    }
                }

                db.SaveChanges();

                // direct payment
                foreach (var directPayment in res.Payments)
                {
                    if (directPayment.Amount == 0) continue;

                    var payment = new PurchasePaymentResource();
                    payment.Direct = true;
                    payment.DocumentDate = receipt.DocumentDate;
                    payment.LocationId = receipt.LocationId;
                    payment.SupplierId = receipt.SupplierId;
                    payment.PaymentMethodId = directPayment.PaymentMethodId;
                    payment.ReferenceNumber = receipt.DocumentNumber;
                    payment.Remarks = directPayment.Remarks;

                    payment.Details = new List<PurchasePaymentDetailResource>();
                    payment.Details.Add(new PurchasePaymentDetailResource
                    {
                        PurchaseReceiptId = receipt.Id,
                        Payment = directPayment.Amount,
                        Balance = receipt.TotalAfterTax,
                        Remaining = receipt.TotalAfterTax - directPayment.Amount
                    });

                    paymentService.Create(payment, userId);
                }

                purchaseOrderService.CalculateQuantityReceived(receipt.PurchaseOrderId);

                transaction.Commit();

                return receipt.Id;
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
        }

        public Guid Update(PurchaseReceiptResource res, Guid? userId)
        {
            ValidateResource(res);

            var transaction = db.Database.BeginTransaction();

            try
            {
                // delete old
                itemService.RemoveStockMovement(res.Id);

                db.Database.ExecuteSqlRaw("DELETE FROM PurchaseReceiptDetail WHERE PurchaseReceiptId = {0} AND IsDeleted = 0", res.Id);

                var oldDirectPayments = db.GetQuery<PurchasePaymentDetail>()
                    .Where(x => x.PurchaseReceiptId == res.Id
                        && x.PurchasePayment.Direct == true)
                    .Select(x => new
                    {
                        PaymentId = x.PurchasePayment.Id,
                        Amount = x.Payment
                    }).ToList();

                foreach (var oldPayment in oldDirectPayments)
                {
                    db.Database.ExecuteSqlRaw("DELETE FROM PurchasePaymentDetail WHERE PurchasePaymentId = {0}", oldPayment.PaymentId);
                    db.Database.ExecuteSqlRaw("DELETE FROM PurchasePayment WHERE Id = {0}", oldPayment.PaymentId);
                }

                var totalOldPayment = oldDirectPayments.Sum(a => a.Amount);
                var oldReceipt = db.Get<PurchaseReceipt>(res.Id);
                oldReceipt.Balance += totalOldPayment;
                db.SaveChanges();

                var oldPurchaseOrderId = db.GetQuery<PurchaseReceipt>()
                    .Where(x => x.Id == res.Id)
                    .Select(x => x.PurchaseOrderId)
                    .FirstOrDefault();
                purchaseOrderService.CalculateQuantityReceived(oldPurchaseOrderId);


                //update
                Calculate(res);

                var receipt = db.Get<PurchaseReceipt>(res.Id);
                receipt.MapFrom(res);
                db.Update(receipt, userId);

                var sequence = 0;
                foreach (var detail in res.Details)
                {
                    if (detail.ItemId.IsGuidEmpty()) continue;
                    if (detail.Quantity == 0) continue;

                    var receiptDetail = new PurchaseReceiptDetail();
                    receiptDetail.MapFrom(detail);
                    receiptDetail.PurchaseReceiptId = receipt.Id;
                    receiptDetail.Sequence = ++sequence;
                    db.Create(receiptDetail, userId);

                    var item = db.Get<Item>(detail.ItemId);

                    var lastPurchaseDate = db.GetQuery<PurchaseReceiptDetail>()
                        .Where(x => x.ItemId == receiptDetail.ItemId)
                        .OrderByDescending(x => x.PurchaseReceipt.DocumentDate)
                        .Select(x => x.PurchaseReceipt.DocumentDate)
                        .FirstOrDefault();

                    if (receipt.DocumentDate >= lastPurchaseDate)
                        item.PurchasePrice = receiptDetail.SubtotalBeforeTax / receiptDetail.Quantity / receiptDetail.Ratio;

                    if (item.Stockable)
                    {
                        itemService.AddStockMovementIn(
                            transactionId: receipt.Id,
                            documentNumber: receipt.DocumentNumber,
                            documentDate: receipt.DocumentDate,
                            transactionType: TransactionType.PurchaseReceipt,
                            itemId: receiptDetail.ItemId,
                            locationId: receipt.LocationId,
                            quantity: receiptDetail.Quantity * receiptDetail.Ratio,
                            cost: receiptDetail.SubtotalBeforeTax / receiptDetail.Quantity / receiptDetail.Ratio);
                    }
                }

                // direct payment
                foreach (var directPayment in res.Payments)
                {
                    if (directPayment.Amount == 0) continue;

                    var payment = new PurchasePaymentResource();
                    payment.Direct = true;
                    payment.DocumentDate = receipt.DocumentDate;
                    payment.LocationId = receipt.LocationId;
                    payment.SupplierId = receipt.SupplierId;
                    payment.PaymentMethodId = directPayment.PaymentMethodId;
                    payment.ReferenceNumber = receipt.DocumentNumber;
                    payment.Remarks = directPayment.Remarks;

                    payment.Details = new List<PurchasePaymentDetailResource>();
                    payment.Details.Add(new PurchasePaymentDetailResource
                    {
                        PurchaseReceiptId = receipt.Id,
                        Payment = directPayment.Amount,
                        Balance = receipt.TotalAfterTax,
                        Remaining = receipt.TotalAfterTax - directPayment.Amount
                    });

                    paymentService.Create(payment, userId);
                }

                db.SaveChanges();

                purchaseOrderService.CalculateQuantityReceived(receipt.PurchaseOrderId);
                CalculateBalance(receipt.Id);

                transaction.Commit();

                return res.Id;
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
        }

        public void Delete(Guid? id, Guid? userId)
        {
            var transaction = db.Database.BeginTransaction();

            try
            {
                var paymentExist = db.GetQuery<PurchasePaymentDetail>()
                    .Any(x => x.PurchaseReceiptId == id);

                if (paymentExist)
                    throw new BadRequestException("Unable to delete Purchase Receipt because it is already paid.");

                // delete old
                itemService.RemoveStockMovement(id);
                db.Delete<PurchaseReceiptDetail>(x => x.PurchaseReceiptId == id, userId);
                db.Delete<PurchaseReceipt>(id, userId);

                db.SaveChanges();

                var oldPurchaseOrderId = db.GetQuery<PurchaseReceipt>()
                    .Where(x => x.Id == id)
                    .Select(x => x.PurchaseOrderId)
                    .FirstOrDefault();
                purchaseOrderService.CalculateQuantityReceived(oldPurchaseOrderId);

                transaction.Commit();
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
        }

        private void CalculateBalance(Guid? purchaseReceiptId)
        {
            var receipt = db.Get<PurchaseReceipt>(purchaseReceiptId);
            receipt.TotalPayment = db.GetQuery<PurchasePaymentDetail>()
                .Where(x => x.PurchaseReceiptId == purchaseReceiptId)
                .Sum(x => x.Payment);

            receipt.Balance = receipt.TotalAfterTax - receipt.TotalPayment;
            receipt.Status = receipt.Balance > 0 ? PaymentStatus.Unpaid : PaymentStatus.Paid;

            db.SaveChanges();
        }

        public List<PurchasePaymentDetailResource> GetListPayment(Guid? id)
        {
            return db.GetQuery<PurchasePaymentDetail>()
                .Where(x => x.PurchaseReceiptId == id)
                .OrderBy(x => x.PurchasePayment.DocumentDate)
                .Project().To<PurchasePaymentDetailResource>()
                .ToList();
        }
    }
}
