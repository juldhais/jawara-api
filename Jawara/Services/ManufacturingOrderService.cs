﻿using Jawara.Enums;
using Jawara.Helpers;
using Jawara.Models;
using Jawara.Repositories;
using Jawara.Resources;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Jawara.Services
{
    public class ManufacturingOrderService
    {
        private readonly DataContext db;
        private readonly ItemService itemService;

        public ManufacturingOrderService(DataContext db,
            ItemService itemService)
        {
            this.db = db;
            this.itemService = itemService;
        }

        public ManufacturingOrderResource Get(Guid? id)
        {
            var order = db.Get<ManufacturingOrder, ManufacturingOrderResource>(id);
            if (order != null)
            {
                order.Details = db.ManufacturingOrderDetail
                    .Where(x => x.IsDeleted == false
                                && x.ManufacturingOrderId == id)
                    .OrderBy(x => x.Sequence)
                    .Project().To<ManufacturingOrderDetailResource>()
                    .ToList();

                if (order.Details == null)
                    order.Details = new List<ManufacturingOrderDetailResource>();

                order.CreatedBy = db.GetQuery<User>()
                    .Where(x => x.Id == order.CreatedById)
                    .Select(x => x.Description)
                    .FirstOrDefault();

                order.UpdatedBy = db.GetQuery<User>()
                    .Where(x => x.Id == order.UpdatedById)
                    .Select(x => x.Description)
                    .FirstOrDefault();
            }

            return order;
        }

        public PagingResult<ManufacturingOrderResource> GetList(
            string keyword,
            string status,
            Guid? sectionId,
            Guid? bomId,
            Guid? parentId,
            Guid? locationId,
            DateTime? startDate,
            DateTime? endDate,
            int page,
            int size)
        {
            if (keyword.IsEmpty()
                && status.IsEmpty()
                && sectionId.IsGuidEmpty()
                && bomId.IsGuidEmpty()
                && parentId.IsGuidEmpty()
                && locationId.IsGuidEmpty()
                && startDate == null && endDate == null
                && page == 0 && size == 0)
                return new PagingResult<ManufacturingOrderResource>();

            var query = db.GetQuery<ManufacturingOrder>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.DocumentNumber.Contains(keyword)
                    || x.Remarks.Contains(keyword));

            if (status.IsNotEmpty())
                query = query.Where(x => x.Status == status);

            if (sectionId.IsGuidNotEmpty())
                query = query.Where(x => x.SectionId == sectionId);

            if (bomId.IsGuidNotEmpty())
                query = query.Where(x => x.BillOfMaterialId == bomId);

            if (parentId.IsGuidNotEmpty())
                query = query.Where(x => x.ParentId == parentId);

            if (locationId.IsGuidNotEmpty())
                query = query.Where(x => x.LocationId == locationId);

            if (startDate != null)
                query = query.Where(x => x.DocumentDate.Date >= startDate);

            if (endDate != null)
                query = query.Where(x => x.DocumentDate.Date <= endDate);

            query = query.OrderByDescending(x => x.DocumentNumber);

            var result = query.PaginateTo<ManufacturingOrder, ManufacturingOrderResource>(page, size);

            return result;
        }

        public List<LookupResource> GetLookup(
            string keyword,
            string status,
            Guid? sectionId,
            Guid? bomId,
            Guid? parentId,
            Guid? locationId)
        {
            if (keyword.IsEmpty()
                && status.IsEmpty()
                && sectionId.IsGuidEmpty()
                && bomId.IsGuidEmpty()
                && parentId.IsGuidEmpty()
                && locationId.IsGuidEmpty())
                return new List<LookupResource>();

            var query = db.GetQuery<ManufacturingOrder>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.DocumentNumber.Contains(keyword)
                    || x.Remarks.Contains(keyword));

            if (status.IsNotEmpty())
                query = query.Where(x => x.Status == status);

            if (sectionId.IsGuidNotEmpty())
                query = query.Where(x => x.SectionId == sectionId);

            if (bomId.IsGuidNotEmpty())
                query = query.Where(x => x.BillOfMaterialId == bomId);

            if (parentId.IsGuidNotEmpty())
                query = query.Where(x => x.ParentId == parentId);

            if (locationId.IsGuidNotEmpty())
                query = query.Where(x => x.LocationId == locationId);

            var result = query.OrderByDescending(x => x.DocumentNumber)
                .Select(x => new LookupResource
                {
                    Id = x.Id,
                    Code = x.DocumentNumber + " ─ " + x.Parent.Code
                }).ToList();

            return result;
        }

        public LookupResource GetSingleLookup(Guid? id)
        {
            var result = db.GetQuery<ManufacturingOrder>()
                .Where(x => x.Id == id)
                .Select(x => new LookupResource
                {
                    Id = x.Id,
                    Code = x.DocumentNumber + " ─ " + x.Parent.Code
                }).FirstOrDefault();

            return result;
        }

        private void ValidateResource(ManufacturingOrderResource res)
        {
            if (res.DocumentDate == default)
                throw new BadRequestException("Document date cannot be empty.");

            if (res.ParentId.IsGuidEmpty())
                throw new BadRequestException("Parent Item cannot be empty.");

            if (res.LocationId.IsGuidEmpty())
                throw new BadRequestException("Location cannot be empty.");

            if (res.Ratio == 0)
                throw new BadRequestException("Ratio cannot be zero.");
        }

        public string GetNewDocumentNumber(DateTime date)
        {
            var prefix = $"MO-{date:yyMM}";

            var lastNumber = db.GetQuery<ManufacturingOrder>()
                .Where(x => x.DocumentNumber.StartsWith(prefix))
                .OrderByDescending(x => x.DocumentNumber)
                .Select(x => x.DocumentNumber)
                .FirstOrDefault();

            if (lastNumber == null) return prefix + "0001";

            var newNumber = lastNumber.Substring(prefix.Length, 4).ToInteger() + 1;
            return prefix + newNumber.ToString("0000");
        }

        public Guid Create(ManufacturingOrderResource res, Guid? userId)
        {
            ValidateResource(res);

            var order = new ManufacturingOrder();
            order.MapFrom(res);
            order.DocumentNumber = GetNewDocumentNumber(res.DocumentDate);
            order.Status = ManufacturingOrderStatus.Open;
            db.Create(order, userId);

            var sequence = 0;
            foreach (var detail in res.Details)
            {
                if (detail.ComponentId.IsGuidEmpty()) continue;
                if (detail.Quantity == 0) continue;

                var orderDetail = new ManufacturingOrderDetail();
                orderDetail.MapFrom(detail);
                orderDetail.ManufacturingOrderId = order.Id;
                orderDetail.Sequence = ++sequence;
                db.Create(orderDetail, userId);
            }

            db.SaveChanges();

            return order.Id;
        }

        public Guid Update(ManufacturingOrderResource res, Guid? userId)
        {
            ValidateResource(res);

            var transaction = db.Database.BeginTransaction();

            try
            {
                //delete old
                db.Database.ExecuteSqlRaw("DELETE FROM ManufacturingOrderDetail WHERE ManufacturingOrderId = {0} AND IsDeleted = 0", res.Id);

                db.SaveChanges();


                //update
                var order = db.Get<ManufacturingOrder>(res.Id);

                order.MapFrom(res);
                db.Update(order, userId);

                var sequence = 0;
                foreach (var detail in res.Details)
                {
                    if (detail.ComponentId.IsGuidEmpty()) continue;
                    if (detail.Quantity == 0) continue;

                    var orderDetail = new ManufacturingOrderDetail();
                    orderDetail.MapFrom(detail);
                    orderDetail.ManufacturingOrderId = order.Id;
                    orderDetail.Sequence = ++sequence;
                    db.Create(orderDetail, userId);
                }

                db.SaveChanges();

                CalculateQuantityCompleted(order.Id);

                transaction.Commit();

                return res.Id;
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
        }

        public void CalculateQuantityCompleted(Guid? id)
        {
            if (id.IsGuidEmpty()) return;

            var qtyCompleted = db.GetQuery<ManufacturingCompletion>()
                .Where(x => x.ManufacturingOrderId == id)
                .Sum(x => x.Quantity);

            var order = db.Get<ManufacturingOrder>(id);
            order.QuantityCompleted = qtyCompleted;
            if (order.QuantityCompleted >= order.Quantity)
                order.Status = ManufacturingOrderStatus.Closed;

            var detailIds = db.GetQuery<ManufacturingOrderDetail>()
                .Where(x => x.ManufacturingOrderId == id)
                .Select(x => x.Id);

            foreach (var detailId in detailIds)
            {
                var orderDetail = db.Get<ManufacturingOrderDetail>(detailId);

                var qtyIssued = db.GetQuery<ManufacturingCompletionDetail>()
                    .Where(x => x.ManufacturingCompletion.ManufacturingOrderId == id
                        && x.ComponentId == orderDetail.ComponentId)
                    .Sum(x => x.Quantity * x.Ratio);

                orderDetail.QuantityIssued = qtyIssued / orderDetail.Ratio;
            }

            db.SaveChanges();
        }

        public void Delete(Guid? id, Guid? userId)
        {
            var status = db.GetQuery<ManufacturingOrder>()
                .Where(x => x.Id == id)
                .Select(x => x.Status)
                .FirstOrDefault();

            if (status == ManufacturingOrderStatus.Closed)
                throw new BadRequestException("Unable to delete Manufacturing Order because it is already Closed");

            var anyCompletion = db.GetQuery<ManufacturingCompletion>()
                .Any(x => x.ManufacturingOrderId == id);

            if (anyCompletion)
                throw new BadRequestException("Unable to delete Manufacturing Order because it is already has a Manufacturing Completion.");

            db.Delete<ManufacturingOrderDetail>(x => x.ManufacturingOrderId == id, userId);
            db.Delete<ManufacturingOrder>(id, userId);
            db.SaveChanges();
        }

        public void Open(Guid? id, Guid? userId)
        {
            var order = db.Get<ManufacturingOrder>(id);
            order.Status = ManufacturingOrderStatus.Open;
            db.Update(order, userId);
            db.SaveChanges();
        }

        public void Close(Guid? id, Guid? userId)
        {
            var order = db.Get<ManufacturingOrder>(id);
            order.Status = ManufacturingOrderStatus.Closed;
            db.Update(order, userId);
            db.SaveChanges();
        }
    }
}
