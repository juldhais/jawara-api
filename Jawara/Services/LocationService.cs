﻿using Jawara.Helpers;
using Jawara.Models;
using Jawara.Repositories;
using Jawara.Resources;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Jawara.Services
{
    public class LocationService
    {
        private readonly DataContext db;

        public LocationService(DataContext db)
        {
            this.db = db;
        }

        public LocationResource Get(Guid? id)
        {
            return db.Get<Location, LocationResource>(id);
        }

        public LocationResource Get(string code)
        {
            return db.GetQuery<Location>()
                .Where(x => x.Code == code)
                .Project().To<LocationResource>()
                .FirstOrDefault();
        }

        public PagingResult<LocationResource> GetList(string keyword, int page, int size)
        {
            var query = db.GetQuery<Location>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.Code == keyword
                    || x.Description.Contains(keyword));

            var result = query.OrderBy(x => x.Description)
                .PaginateTo<Location, LocationResource>(page, size);

            return result;
        }

        public List<LookupResource> GetLookup(string keyword)
        {
            if (keyword.IsEmpty() || keyword.Length < 2)
                return new List<LookupResource>();

            var query = db.GetQuery<Location>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.Code == keyword
                    || x.Description.Contains(keyword));

            var result = query.OrderBy(x => x.Code)
                .Select(x => new LookupResource
                {
                    Id = x.Id,
                    Code = x.Code + " ─ " + x.Description
                }).ToList();

            return result;
        }

        public LookupResource GetSingleLookup(Guid? id)
        {
            var result = db.GetQuery<Location>()
                .Where(x => x.Id == id)
                .Select(x => new LookupResource
                {
                    Id = x.Id,
                    Code = x.Code + " ─ " + x.Description
                }).FirstOrDefault();

            return result;
        }

        public void ValidateResource(LocationResource res)
        {
            if (res.Code.IsEmpty())
                throw new BadRequestException("Code cannot be empty.");

            if (res.Description.IsEmpty())
                throw new BadRequestException("Code cannot be empty.");

            if (db.GetQuery<Location>().Any(x => x.Code == res.Code && x.Id != res.Id))
                throw new BadRequestException("Code is already used.");
        }

        public Guid Create(LocationResource res, Guid? userId)
        {
            ValidateResource(res);

            var location = new Location();
            location.MapFrom(res);
            db.Create(location, userId);
            db.SaveChanges();

            return location.Id;
        }

        public Guid Update(LocationResource res, Guid? userId)
        {
            ValidateResource(res);

            var location = db.Get<Location>(res.Id);
            location.MapFrom(res);
            db.Update(location, userId);
            db.SaveChanges();

            return location.Id;
        }

        public void Delete(Guid? id, Guid? userId)
        {
            db.Delete<Location>(id, userId);
            db.SaveChanges();
        }
    }
}
