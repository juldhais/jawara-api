﻿using Jawara.Helpers;
using Jawara.Models;
using Jawara.Repositories;
using Jawara.Resources;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Jawara.Services
{
    public class PaymentMethodService
    {
        private readonly DataContext db;

        public PaymentMethodService(DataContext db)
        {
            this.db = db;
        }

        public PaymentMethodResource Get(Guid? id)
        {
            return db.Get<PaymentMethod, PaymentMethodResource>(id);
        }

        public PagingResult<PaymentMethodResource> GetList(string keyword, int page, int size)
        {
            var query = db.GetQuery<PaymentMethod>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.Code.Contains(keyword));

            var result = query.OrderBy(x => x.Code)
                .PaginateTo<PaymentMethod, PaymentMethodResource>(page, size);

            return result;
        }

        public PagingResult<PaymentMethodResource> GetListPurchase(string keyword, int page, int size)
        {
            var query = db.GetQuery<PaymentMethod>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.Code.Contains(keyword));

            query = query.Where(x => x.Purchase == true);

            var result = query.OrderBy(x => x.Code)
                .PaginateTo<PaymentMethod, PaymentMethodResource>(page, size);

            return result;
        }

        public PagingResult<PaymentMethodResource> GetListSales(string keyword, int page, int size)
        {
            var query = db.GetQuery<PaymentMethod>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.Code.Contains(keyword));

            query = query.Where(x => x.Sales == true);

            var result = query.OrderBy(x => x.Code)
                .PaginateTo<PaymentMethod, PaymentMethodResource>(page, size);

            return result;
        }

        public List<LookupResource> GetLookup(string keyword)
        {
            if (keyword.IsEmpty() || keyword.Length < 2)
                return new List<LookupResource>();

            var query = db.GetQuery<PaymentMethod>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.Code.Contains(keyword));

            var result = query.OrderBy(x => x.Code)
                .Select(x => new LookupResource
                {
                    Id = x.Id,
                    Code = x.Code
                }).ToList();

            return result;
        }

        public LookupResource GetSingleLookup(Guid? id)
        {
            var result = db.GetQuery<PaymentMethod>()
                .Where(x => x.Id == id)
                .Select(x => new LookupResource
                {
                    Id = x.Id,
                    Code = x.Code
                }).FirstOrDefault();

            return result;
        }

        public List<LookupResource> GetLookupPurchase(string keyword)
        {
            if (keyword.IsEmpty() || keyword.Length < 2)
                return new List<LookupResource>();

            var query = db.GetQuery<PaymentMethod>();
            query = query.Where(x => x.Purchase == true);

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.Code.Contains(keyword));

            var result = query.OrderBy(x => x.Code)
                .Select(x => new LookupResource
                {
                    Id = x.Id,
                    Code = x.Code
                }).ToList();

            return result;
        }

        public List<LookupResource> GetLookupSales(string keyword)
        {
            if (keyword.IsEmpty() || keyword.Length < 2)
                return new List<LookupResource>();

            var query = db.GetQuery<PaymentMethod>();
            query = query.Where(x => x.Sales == true);

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.Code.Contains(keyword));

            var result = query.OrderBy(x => x.Code)
                .Select(x => new LookupResource
                {
                    Id = x.Id,
                    Code = x.Code
                }).ToList();

            return result;
        }

        public void ValidateResource(PaymentMethodResource res)
        {
            if (res.Code.IsEmpty())
                throw new BadRequestException("Code cannot be empty.");

            if (db.GetQuery<PaymentMethod>().Any(x => x.Code == res.Code && x.Id != res.Id))
                throw new BadRequestException("Code is already used.");
        }

        public Guid Create(PaymentMethodResource res, Guid? userId)
        {
            ValidateResource(res);

            var method = new PaymentMethod();
            method.MapFrom(res);
            db.Create(method, userId);
            db.SaveChanges();

            return method.Id;
        }

        public Guid Update(PaymentMethodResource res, Guid? userId)
        {
            ValidateResource(res);

            var method = db.Get<PaymentMethod>(res.Id);
            method.MapFrom(res);
            db.Update(method, userId);
            db.SaveChanges();

            return method.Id;
        }

        public void Delete(Guid? id, Guid? userId)
        {
            db.Delete<PaymentMethod>(id, userId);
            db.SaveChanges();
        }
    }
}
