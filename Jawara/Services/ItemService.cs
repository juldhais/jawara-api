﻿using Jawara.Helpers;
using Jawara.Models;
using Jawara.Repositories;
using Jawara.Resources;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Jawara.Services
{
    public class ItemService
    {
        private readonly DataContext db;

        public ItemService(DataContext db)
        {
            this.db = db;
        }

        public ItemResource Get(Guid? id)
        {
            var item = db.Get<Item, ItemResource>(id);

            if (item != null)
                item.Units = db.GetQuery<ItemUnit>()
                    .Where(x => x.ItemId == id)
                    .OrderBy(x => x.Ratio)
                    .Project().To<ItemUnitResource>()
                    .ToList();

            return item;
        }

        public ItemResource GetByCode(string code)
        {
            if (code.IsEmpty()) return null;

            var item = db.GetQuery<Item>()
                .Where(x => x.Code == code || x.Barcode == code)
                .Project().To<ItemResource>()
                .FirstOrDefault();

            if (item != null)
                item.Units = db.GetQuery<ItemUnit>()
                    .Where(x => x.ItemId == item.Id)
                    .OrderBy(x => x.Ratio)
                    .Project().To<ItemUnitResource>()
                    .ToList();

            return item;
        }

        public ItemResource Get(string code)
        {
            var item = db.GetQuery<Item>()
                .Where(x => x.Code == code || x.Barcode == code)
                .Project().To<ItemResource>()
                .FirstOrDefault();

            if (item != null)
                item.Units = db.GetQuery<ItemUnit>()
                    .Where(x => x.ItemId == item.Id)
                    .OrderBy(x => x.Ratio)
                    .Project().To<ItemUnitResource>()
                    .ToList();

            return item;
        }

        public decimal GetSalesPrice(SalesPriceResource resource)
        {
            decimal result = 0;

            Guid? priceControlCategoryId = null;
            if (resource.CustomerId.IsGuidNotEmpty())
            {
                priceControlCategoryId = db.GetQuery<Customer>()
                    .Where(x => x.Id == resource.CustomerId)
                    .Select(x => x.PriceControlCategoryId)
                    .FirstOrDefault();
            }

            if (priceControlCategoryId.IsGuidEmpty() && resource.LocationId.IsGuidNotEmpty())
            {
                priceControlCategoryId = db.GetQuery<Location>()
                    .Where(x => x.Id == resource.LocationId)
                    .Select(x => x.PriceControlCategoryId)
                    .FirstOrDefault();
            }

            var priceControl = db.GetQuery<PriceControl>()
                .Where(x => x.ItemId == resource.ItemId
                    && x.PriceControlCategoryId == priceControlCategoryId
                    && x.Unit == resource.Unit
                    && x.Ratio == resource.Ratio
                    && x.MinimumQuantity <= resource.Quantity)
                .OrderByDescending(x => x.MinimumQuantity)
                .FirstOrDefault();

            if (priceControl == null)
            {
                var salesPrice = db.GetQuery<Item>()
                    .Where(x => x.Id == resource.ItemId)
                    .Select(x => x.SalesPrice)
                    .FirstOrDefault();

                result = salesPrice * resource.Ratio;
            }
            else result = priceControl.Price;

            return result;
        }

        public PagingResult<ItemResource> GetList(string keyword, int page, int size)
        {
            var query = db.GetQuery<Item>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.Code == keyword
                    || x.Description.Contains(keyword)
                    || x.Barcode == keyword
                    || x.Tag.Contains(keyword)
                    || x.ItemCategory.Code.Contains(keyword)
                    || x.ItemSubcategory.Code.Contains(keyword)
                    || x.Supplier.Description.Contains(keyword));

            var result = query.OrderBy(x => x.Description)
                .PaginateTo<Item, ItemResource>(page, size);

            return result;
        }

        public PagingResult<ItemResource> GetListPurchasable(string keyword, int page, int size)
        {
            var query = db.GetQuery<Item>();

            query = query.Where(x => x.Purchasable == true);

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.Code == keyword
                    || x.Description.Contains(keyword)
                    || x.Barcode == keyword
                    || x.Tag.Contains(keyword)
                    || x.ItemCategory.Code.Contains(keyword)
                    || x.ItemSubcategory.Code.Contains(keyword)
                    || x.Supplier.Description.Contains(keyword));

            var result = query.OrderBy(x => x.Description)
                .PaginateTo<Item, ItemResource>(page, size);

            return result;
        }

        public PagingResult<ItemResource> GetListStockable(string keyword, int page, int size)
        {
            var query = db.GetQuery<Item>();

            query = query.Where(x => x.Stockable == true);

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.Code == keyword
                    || x.Description.Contains(keyword)
                    || x.Barcode == keyword
                    || x.Tag.Contains(keyword)
                    || x.ItemCategory.Code.Contains(keyword)
                    || x.ItemSubcategory.Code.Contains(keyword)
                    || x.Supplier.Description.Contains(keyword));

            var result = query.OrderBy(x => x.Description)
                .PaginateTo<Item, ItemResource>(page, size);

            return result;
        }

        public PagingResult<ItemResource> GetListManufacturable(string keyword, int page, int size)
        {
            var query = db.GetQuery<Item>();

            query = query.Where(x => x.Manufacturable == true);

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.Code == keyword
                    || x.Description.Contains(keyword)
                    || x.Barcode == keyword
                    || x.Tag.Contains(keyword)
                    || x.ItemCategory.Code.Contains(keyword)
                    || x.ItemSubcategory.Code.Contains(keyword)
                    || x.Supplier.Description.Contains(keyword));

            var result = query.OrderBy(x => x.Description)
                .PaginateTo<Item, ItemResource>(page, size);

            return result;
        }

        public PagingResult<ItemResource> GetListSellable(string keyword, int page, int size)
        {
            var query = db.GetQuery<Item>();

            query = query.Where(x => x.Sellable == true);

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.Code == keyword
                    || x.Description.Contains(keyword)
                    || x.Barcode == keyword
                    || x.Tag.Contains(keyword)
                    || x.ItemCategory.Code.Contains(keyword)
                    || x.ItemSubcategory.Code.Contains(keyword)
                    || x.Supplier.Description.Contains(keyword));

            var result = query.OrderBy(x => x.Description)
                .PaginateTo<Item, ItemResource>(page, size);

            return result;
        }

        public List<LookupResource> GetLookup(string keyword)
        {
            if (keyword.IsEmpty() || keyword.Length < 2)
                return new List<LookupResource>();

            var query = db.GetQuery<Item>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.Code == keyword
                    || x.Description.Contains(keyword)
                    || x.Barcode == keyword);

            var result = query.OrderBy(x => x.Code)
                .Select(x => new LookupResource
                {
                    Id = x.Id,
                    Code = x.Code + " ─ " + x.Description
                }).ToList();

            return result;
        }

        public LookupResource GetSingleLookup(Guid? id)
        {
            var result = db.GetQuery<Item>()
                .Where(x => x.Id == id)
                .Select(x => new LookupResource
                {
                    Id = x.Id,
                    Code = x.Code + " ─ " + x.Description
                }).FirstOrDefault();

            return result;
        }

        public List<LookupResource> GetLookupStockable(string keyword)
        {
            if (keyword.IsEmpty() || keyword.Length < 2)
                return new List<LookupResource>();

            var query = db.GetQuery<Item>();
            query = query.Where(x => x.Stockable == true);

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.Code == keyword
                    || x.Description.Contains(keyword)
                    || x.Barcode == keyword);

            var result = query.OrderBy(x => x.Code)
                .Select(x => new LookupResource
                {
                    Id = x.Id,
                    Code = x.Code + " ─ " + x.Description
                }).ToList();

            return result;
        }

        public List<LookupResource> GetLookupPurchasable(string keyword)
        {
            if (keyword.IsEmpty() || keyword.Length < 2)
                return new List<LookupResource>();

            var query = db.GetQuery<Item>();
            query = query.Where(x => x.Purchasable == true);

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.Code == keyword
                    || x.Description.Contains(keyword)
                    || x.Barcode == keyword);

            var result = query.OrderBy(x => x.Code)
                .Select(x => new LookupResource
                {
                    Id = x.Id,
                    Code = x.Code + " ─ " + x.Description
                }).ToList();

            return result;
        }

        public List<LookupResource> GetLookupManufacturable(string keyword)
        {
            if (keyword.IsEmpty() || keyword.Length < 2)
                return new List<LookupResource>();

            var query = db.GetQuery<Item>();
            query = query.Where(x => x.Manufacturable == true);

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.Code == keyword
                    || x.Description.Contains(keyword)
                    || x.Barcode == keyword);

            var result = query.OrderBy(x => x.Code)
                .Select(x => new LookupResource
                {
                    Id = x.Id,
                    Code = x.Code + " ─ " + x.Description
                }).ToList();

            return result;
        }

        public List<LookupResource> GetLookupSellable(string keyword)
        {
            if (keyword.IsEmpty() || keyword.Length < 2)
                return new List<LookupResource>();

            var query = db.GetQuery<Item>();
            query = query.Where(x => x.Sellable == true);

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.Code == keyword
                    || x.Description.Contains(keyword)
                    || x.Barcode == keyword);

            var result = query.OrderBy(x => x.Code)
                .Select(x => new LookupResource
                {
                    Id = x.Id,
                    Code = x.Code + " ─ " + x.Description
                }).ToList();

            return result;
        }

        public void ValidateResource(ItemResource res)
        {
            if (res.Code.IsEmpty())
                throw new BadRequestException("Code cannot be empty.");

            if (res.Description.IsEmpty())
                throw new BadRequestException("Code cannot be empty.");

            if (db.GetQuery<Item>().Any(x => x.Code == res.Code && x.Id != res.Id))
                throw new BadRequestException("Code is already used.");

            foreach (var unitRes in res.Units)
            {
                if (unitRes.Unit.IsEmpty())
                    throw new BadRequestException("Unit name cannot be empty.");

                if (unitRes.Ratio <= 0)
                    throw new BadRequestException("Ratio must be greater than zero.");
            }
        }

        public Guid Create(ItemResource res, Guid? userId)
        {
            ValidateResource(res);

            var item = new Item();
            item.MapFrom(res);
            db.Create(item, userId);

            foreach (var unitRes in res.Units)
            {
                var unit = new ItemUnit();
                unit.MapFrom(unitRes);
                unit.ItemId = item.Id;
                db.Create(unit, userId);
            }

            db.SaveChanges();

            return item.Id;
        }

        public Guid Update(ItemResource res, Guid? userId)
        {
            ValidateResource(res);

            db.Remove<ItemUnit>(x => x.ItemId == res.Id);

            var item = db.Get<Item>(res.Id);
            item.MapFrom(res);
            db.Update(item, userId);

            foreach (var unitRes in res.Units)
            {
                var unit = new ItemUnit();
                unit.MapFrom(unitRes);
                unit.ItemId = item.Id;
                db.Create(unit, userId);
            }

            db.SaveChanges();

            return item.Id;
        }

        public void Delete(Guid? id, Guid? userId)
        {
            db.Delete<ItemUnit>(x => x.ItemId == id, userId);
            db.Delete<Item>(id, userId);
            db.SaveChanges();
        }

        public void AddStockMovementIn(
            Guid transactionId,
            string documentNumber,
            DateTime documentDate,
            string transactionType,
            Guid? itemId,
            Guid? locationId,
            decimal quantity,
            decimal cost)
        {
            var itemStock = db.GetQuery<ItemStock>()
                .Where(x => x.ItemId == itemId
                        && x.LocationId == locationId
                        && x.Date == documentDate
                        && x.Cost == cost)
                .FirstOrDefault();

            if (itemStock == null)
            {
                itemStock = new ItemStock();
                itemStock.ItemId = itemId;
                itemStock.LocationId = locationId;
                itemStock.Date = documentDate;
                itemStock.Stock = quantity;
                itemStock.Cost = cost;
                db.Create(itemStock, null);
            }
            else itemStock.Stock += quantity;

            var movement = new StockMovement();
            movement.TransactionId = transactionId;
            movement.DocumentNumber = documentNumber;
            movement.DocumentDate = documentDate;
            movement.TransactionType = transactionType;
            movement.ItemStockId = itemStock.Id;
            movement.Quantity = quantity;
            db.Create(movement, null);

            db.SaveChanges();
        }

        public void AddStockMovementOut(
            Guid transactionId,
            string documentNumber,
            DateTime documentDate,
            string transactionType,
            Guid? itemId,
            Guid? locationId,
            decimal quantity)
        {
            var itemStocks = db.GetQuery<ItemStock>()
            .Where(x => x.ItemId == itemId
                && x.LocationId == locationId
                && x.Stock > 0)
            .OrderBy(x => x.Date)
            .ThenBy(x => x.Stock)
            .ThenBy(x => x.Cost)
            .Select(x => new
            {
                x.Id,
                x.Stock
            }).ToList();

            if (itemStocks.Sum(x => x.Stock) < quantity)
                throw new BadRequestException("Stock is not sufficient.");

            var remaining = quantity;
            foreach (var stock in itemStocks)
            {
                decimal used = 0;
                if (stock.Stock >= remaining)
                    used = remaining;
                else used = stock.Stock;

                var itemStock = db.Get<ItemStock>(stock.Id);
                itemStock.Stock -= used;

                var movement = new StockMovement();
                movement.TransactionId = transactionId;
                movement.DocumentNumber = documentNumber;
                movement.DocumentDate = documentDate;
                movement.TransactionType = transactionType;
                movement.ItemStockId = itemStock.Id;
                movement.Quantity = used * -1;
                db.Create(movement, null);

                remaining = remaining - used;
                if (remaining == 0) break;
            }

            db.SaveChanges();
        }

        public void RemoveStockMovement(Guid? transactionId)
        {
            var stockMovements = db.GetQuery<StockMovement>()
                .Where(x => x.TransactionId == transactionId)
                .Select(x => new
                {
                    x.Id,
                    x.ItemStockId,
                    x.DocumentDate,
                    x.Quantity
                }).ToList();

            foreach (var movement in stockMovements)
            {
                var itemStock = db.Get<ItemStock>(movement.ItemStockId);
                itemStock.Stock = itemStock.Stock - movement.Quantity;
            }

            db.Remove<StockMovement>(x => x.TransactionId == transactionId);

            db.SaveChanges();
        }

        public PagingResult<ItemStockResource> GetStock(Guid? itemId, Guid? locationId, int page, int size)
        {
            if (itemId.IsGuidEmpty()
                && locationId.IsGuidEmpty()
                && page == 0
                && size == 0)
                return new PagingResult<ItemStockResource>();

            var query = db.GetQuery<ItemStock>();

            if (itemId.IsGuidNotEmpty())
                query = query.Where(x => x.ItemId == itemId);

            if (locationId.IsGuidNotEmpty())
                query = query.Where(x => x.LocationId == locationId);

            var result = new PagingResult<ItemStockResource>();
            result.TotalData = query.Count();
            result.Page = page == 0 ? 1 : page;
            result.Size = size == 0 ? result.TotalData : size;
            result.TotalPage = (result.TotalData - 1) / (result.Size + 1);

            result.Data = query.OrderBy(x => x.Item.Description)
                .ThenBy(x => x.Location.Description)
                .Paginate(page, size)
                .Select(x => new ItemStockResource
                {
                    ItemId = x.ItemId,
                    LocationId = x.LocationId,
                    Date = x.Date,
                    Code = x.Item.Code,
                    Description = x.Item.Description,
                    Category = x.Item.ItemCategory.Code,
                    Subcategory = x.Item.ItemSubcategory.Code,
                    Tag = x.Item.Tag,
                    Unit = x.Item.Unit,
                    Location = x.Location.Code,
                    Stock = x.Stock,
                    Cost = x.Cost
                }).ToList();

            foreach (var item in result.Data)
            {
                var itemUnits = db.GetQuery<ItemUnit>()
                    .Where(x => x.ItemId == item.ItemId)
                    .OrderByDescending(x => x.Ratio)
                    .ToList();

                var stockDetail = new List<string>();
                bool isNegative = item.Stock < 0;
                decimal remainder = Math.Abs(item.Stock);

                foreach (var itemUnit in itemUnits)
                {
                    if (remainder == 0) break;

                    var stock = remainder / itemUnit.Ratio;
                    remainder = remainder % itemUnit.Ratio;

                    if (stock != 0)
                        stockDetail.Add($"{stock:#,#0.##} {itemUnit.Unit}");
                }

                if (remainder != 0)
                    stockDetail.Add($"{remainder} {item.Unit}");

                item.StockDetail = string.Join("   ", stockDetail);
                item.Subtotal = item.Cost * item.Stock;
            }

            return result;
        }

        public ImportResultResource Import(List<ItemImportResource> list, Guid? userId)
        {
            var transaction = db.Database.BeginTransaction();

            try
            {
                var result = new ImportResultResource();

                var line = 0;
                foreach (var itemRes in list)
                {
                    line++;

                    if (itemRes.Code.IsEmpty() && itemRes.Description.IsEmpty()) continue;

                    if (itemRes.Code.IsEmpty())
                        result.AddErrorMessage($"[{line}] Item code cannot be empty.");

                    if (itemRes.Description.IsEmpty())
                        result.AddErrorMessage($"[{line}] Item name cannot be empty.");

                    if (result.IsError) continue;

                    bool isNew = false;

                    var item = db.GetQuery<Item>().FirstOrDefault(x => x.Code == itemRes.Code);

                    if (item == null)
                    {
                        item = new Item();
                        isNew = true;
                    }

                    item.MapFrom(itemRes);

                    if (itemRes.Category.IsNotEmpty())
                    {
                        var category = db.GetQuery<ItemCategory>()
                            .Where(x => x.Code == itemRes.Category)
                            .FirstOrDefault();

                        if (category == null)
                        {
                            category = new ItemCategory();
                            category.Code = itemRes.Category;
                            db.Create(category, userId);
                            db.SaveChanges();
                        }

                        item.ItemCategoryId = category?.Id;
                    }

                    if (itemRes.Subcategory.IsNotEmpty())
                    {
                        var subcategory = db.GetQuery<ItemSubcategory>()
                            .Where(x => x.Code == itemRes.Subcategory)
                            .FirstOrDefault();

                        if (subcategory == null)
                        {
                            subcategory = new ItemSubcategory();
                            subcategory.Code = itemRes.Subcategory;
                            db.Create(subcategory, userId);
                            db.SaveChanges();
                        }

                        item.ItemSubcategoryId = subcategory?.Id;
                    }

                    if (itemRes.Supplier.IsNotEmpty())
                    {
                        item.SupplierId = db.GetQuery<Supplier>()
                            .Where(x => x.Description == itemRes.Supplier)
                            .Select(x => x.Id)
                            .FirstOrDefault();

                        if (item.SupplierId.IsGuidEmpty())
                            throw new BadRequestException($"[{line}] Supplier {itemRes.Supplier} not found.");
                    }


                    if (isNew) db.Create(item, userId);
                    else db.Update(item, userId);
                }

                db.SaveChanges();
                transaction.Commit();

                return result;
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
        }
    }
}
