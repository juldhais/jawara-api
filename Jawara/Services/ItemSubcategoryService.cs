﻿using Jawara.Helpers;
using Jawara.Models;
using Jawara.Repositories;
using Jawara.Resources;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Jawara.Services
{
    public class ItemSubcategoryService
    {
        private readonly DataContext db;

        public ItemSubcategoryService(DataContext db)
        {
            this.db = db;
        }

        public ItemSubcategoryResource Get(Guid? id)
        {
            return db.Get<ItemSubcategory, ItemSubcategoryResource>(id);
        }

        public PagingResult<ItemSubcategoryResource> GetList(string keyword, int page, int size)
        {
            var query = db.GetQuery<ItemSubcategory>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.Code.Contains(keyword));

            var result = query.OrderBy(x => x.Code)
                .PaginateTo<ItemSubcategory, ItemSubcategoryResource>(page, size);

            return result;
        }

        public List<LookupResource> GetLookup(string keyword)
        {
            if (keyword.IsEmpty() || keyword.Length < 2)
                return new List<LookupResource>();

            var query = db.GetQuery<ItemSubcategory>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.Code.Contains(keyword));

            var result = query.OrderBy(x => x.Code)
                .Select(x => new LookupResource
                {
                    Id = x.Id,
                    Code = x.Code
                }).ToList();

            return result;
        }

        public LookupResource GetSingleLookup(Guid? id)
        {
            var result = db.GetQuery<ItemSubcategory>()
                .Where(x => x.Id == id)
                .Select(x => new LookupResource
                {
                    Id = x.Id,
                    Code = x.Code
                }).FirstOrDefault();

            return result;
        }

        public void ValidateResource(ItemSubcategoryResource res)
        {
            if (res.Code.IsEmpty())
                throw new BadRequestException("Code cannot be empty.");

            if (db.GetQuery<ItemSubcategory>().Any(x => x.Code == res.Code && x.Id != res.Id))
                throw new BadRequestException("Code is already used.");
        }

        public Guid Create(ItemSubcategoryResource res, Guid? userId)
        {
            ValidateResource(res);

            var category = new ItemSubcategory();
            category.MapFrom(res);
            db.Create(category, userId);
            db.SaveChanges();

            return category.Id;
        }

        public Guid Update(ItemSubcategoryResource res, Guid? userId)
        {
            ValidateResource(res);

            var category = db.Get<ItemSubcategory>(res.Id);
            category.MapFrom(res);
            db.Update(category, userId);
            db.SaveChanges();

            return category.Id;
        }

        public void Delete(Guid? id, Guid? userId)
        {
            db.Delete<ItemSubcategory>(id, userId);
            db.SaveChanges();
        }
    }
}
