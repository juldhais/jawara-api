﻿using Jawara.Enums;
using Jawara.Helpers;
using Jawara.Models;
using Jawara.Repositories;
using Jawara.Resources;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Jawara.Services
{
    public class ConfigurationService
    {
        private readonly DataContext db;

        public ConfigurationService(DataContext db)
        {
            this.db = db;
        }

        public List<ConfigurationResource> GetList()
        {
            return db.GetQuery<Configuration>()
                .OrderBy(x => x.Key)
                .Project().To<ConfigurationResource>()
                .ToList();
        }

        public void Update(List<ConfigurationResource> list)
        {
            foreach (var config in list)
                db.SetConfiguration(config.Key, config.Value);

            db.SaveChanges();
        }

        public ConfigurationResource Get(string key)
        {
            return db.GetQuery<Configuration>()
                .Where(x => x.Key == key)
                .Project().To<ConfigurationResource>()
                .FirstOrDefault();
        }

        public ConfigurationResource[] GetAll()
        {
            return db.GetQuery<Configuration>()
                .Project().To<ConfigurationResource>()
                .ToArray();
        }

        public void Set(ConfigurationResource res, Guid? userId)
        {
            var config = db.GetQuery<Configuration>()
                .Where(x => x.Key == res.Key)
                .FirstOrDefault();

            if (config == null)
            {
                config = new Configuration();
                config.Key = res.Key;
                config.Value = res.Value;
                db.Create(config, userId);
            }
            else
            {
                config.Value = res.Value;
                db.Update(config, userId);
            }

            db.SaveChanges();
        }

        public void SetAll(ConfigurationResource[] res, Guid? userId)
        {
            foreach (var item in res)
            {
                var config = db.GetQuery<Configuration>()
                    .Where(x => x.Key == item.Key)
                    .FirstOrDefault();

                if (config == null)
                {
                    config = new Configuration();
                    config.Key = item.Key;
                    config.Value = item.Value;
                    db.Create(config, userId);
                }
                else
                {
                    config.Value = item.Value;
                    db.Update(config, userId);
                }
            }

            db.SaveChanges();
        }

        public ConfigurationEditResource GetEdit()
        {
            var list = db.GetQuery<Configuration>().ToList();

            string getValue(string key)
            {
                return list.FirstOrDefault(x => x.Key == key)?.Value;
            }

            var resource = new ConfigurationEditResource();
            resource.CompanyName = getValue(Config.CompanyName);
            resource.PrintHeader = getValue(Config.PrintHeader);
            resource.PrintSubheader = getValue(Config.PrintSubheader);
            resource.PrintFooter = getValue(Config.PrintFooter);
            resource.AutoCutter = getValue(Config.AutoCutter);
            resource.PrintType = getValue(Config.PrintType);
            resource.UseDiscount = getValue(Config.UseDiscount);
            resource.UseTax = getValue(Config.UseTax);

            return resource;
        }

        public void SetEdit(ConfigurationEditResource resource, Guid? userId)
        {
            void setConfig(string key, string value)
            {
                var config = db.GetQuery<Configuration>()
                           .FirstOrDefault(x => x.Key == key);

                if (config == null)
                {
                    config = new Configuration();
                    config.Key = key;
                    config.Value = value;
                    db.Create(config, userId);
                }
                else
                {
                    config.Value = value;
                    db.Update(config, userId);
                }
            }

            setConfig(Config.CompanyName, resource.CompanyName);
            setConfig(Config.PrintHeader, resource.PrintHeader);
            setConfig(Config.PrintSubheader, resource.PrintSubheader);
            setConfig(Config.PrintFooter, resource.PrintFooter);
            setConfig(Config.AutoCutter, resource.AutoCutter);
            setConfig(Config.PrintType, resource.PrintType);
            setConfig(Config.UseDiscount, resource.UseDiscount);
            setConfig(Config.UseTax, resource.UseTax);

            db.SaveChanges();
        }
    }
}
