﻿using Jawara.Helpers;
using Jawara.Models;
using Jawara.Repositories;
using Jawara.Resources;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Jawara.Services
{
    public class ItemTagService
    {
        private readonly DataContext db;

        public ItemTagService(DataContext db)
        {
            this.db = db;
        }

        public ItemTagResource Get(Guid? id)
        {
            return db.Get<ItemTag, ItemTagResource>(id);
        }

        public PagingResult<ItemTagResource> GetList(string keyword, int page, int size)
        {
            var query = db.GetQuery<ItemTag>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.Code.Contains(keyword));

            var result = query.OrderBy(x => x.Code)
                .PaginateTo<ItemTag, ItemTagResource>(page, size);

            return result;
        }

        public List<LookupResource> GetLookup(string keyword)
        {
            if (keyword.IsEmpty() || keyword.Length < 2)
                return new List<LookupResource>();

            var query = db.GetQuery<ItemTag>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.Code.Contains(keyword));

            var result = query.OrderBy(x => x.Code)
                .Select(x => new LookupResource
                {
                    Id = x.Id,
                    Code = x.Code
                }).ToList();

            return result;
        }

        public LookupResource GetSingleLookup(Guid? id)
        {
            var result = db.GetQuery<ItemTag>()
                .Where(x => x.Id == id)
                .Select(x => new LookupResource
                {
                    Id = x.Id,
                    Code = x.Code
                }).FirstOrDefault();

            return result;
        }

        public void ValidateResource(ItemTagResource res)
        {
            if (res.Code.IsEmpty())
                throw new BadRequestException("Code cannot be empty.");

            if (db.GetQuery<ItemTag>().Any(x => x.Code == res.Code && x.Id != res.Id))
                throw new BadRequestException("Code is already used.");
        }

        public Guid Create(ItemTagResource res, Guid? userId)
        {
            ValidateResource(res);

            var tag = new ItemTag();
            tag.MapFrom(res);
            db.Create(tag, userId);
            db.SaveChanges();

            return tag.Id;
        }

        public Guid Update(ItemTagResource res, Guid? userId)
        {
            ValidateResource(res);

            var tag = db.Get<ItemTag>(res.Id);
            tag.MapFrom(res);
            db.Update(tag, userId);
            db.SaveChanges();

            return tag.Id;
        }

        public void Delete(Guid? id, Guid? userId)
        {
            db.Delete<ItemTag>(id, userId);
            db.SaveChanges();
        }
    }
}
