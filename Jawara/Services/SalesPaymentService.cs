﻿using Jawara.Enums;
using Jawara.Helpers;
using Jawara.Models;
using Jawara.Repositories;
using Jawara.Resources;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Jawara.Services
{
    public class SalesPaymentService
    {
        private readonly DataContext db;

        public SalesPaymentService(DataContext db)
        {
            this.db = db;
        }

        public SalesPaymentResource Get(Guid? id)
        {
            var payment = db.Get<SalesPayment, SalesPaymentResource>(id);
            if (payment != null)
            {
                payment.Details = db.SalesPaymentDetail
                    .Where(x => x.IsDeleted == false
                                && x.SalesPaymentId == id)
                    .OrderBy(x => x.SalesDelivery.DocumentNumber)
                    .Project().To<SalesPaymentDetailResource>()
                    .ToList();

                if (payment.Details == null)
                    payment.Details = new List<SalesPaymentDetailResource>();

                payment.Returns = db.SalesReturn
                    .Where(x => x.IsDeleted == false
                            && x.SalesPaymentId == id)
                    .OrderBy(x => x.DocumentNumber)
                    .Project().To<SalesPaymentReturnResource>()
                    .ToList();

                if (payment.Returns == null)
                    payment.Returns = new List<SalesPaymentReturnResource>();
                else
                    payment.Returns.ForEach(x => x.Check = true);

                payment.CreatedBy = db.GetQuery<User>()
                    .Where(x => x.Id == payment.CreatedById)
                    .Select(x => x.Description)
                    .FirstOrDefault();

                payment.UpdatedBy = db.GetQuery<User>()
                    .Where(x => x.Id == payment.UpdatedById)
                    .Select(x => x.Description)
                    .FirstOrDefault();
            }

            return payment;
        }

        public PagingResult<SalesPaymentResource> GetList(string keyword, Guid? customerId, Guid? locationId, DateTime? startDate, DateTime? endDate, int page, int size)
        {
            if (keyword.IsEmpty()
                && customerId.IsGuidEmpty()
                && locationId.IsGuidEmpty()
                && startDate == null && endDate == null
                && page == 0 && size == 0)
                return new PagingResult<SalesPaymentResource>();

            var query = db.GetQuery<SalesPayment>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.DocumentNumber.Contains(keyword)
                    || x.ReferenceNumber.Contains(keyword)
                    || x.Remarks.Contains(keyword));

            if (customerId.IsGuidNotEmpty())
                query = query.Where(x => x.CustomerId == customerId);

            if (locationId.IsGuidNotEmpty())
                query = query.Where(x => x.LocationId == locationId);

            if (startDate != null)
                query = query.Where(x => x.DocumentDate.Date >= startDate);

            if (endDate != null)
                query = query.Where(x => x.DocumentDate.Date <= endDate);

            query = query.OrderByDescending(x => x.DocumentNumber);

            var result = query.PaginateTo<SalesPayment, SalesPaymentResource>(page, size);

            return result;
        }

        private void Calculate(SalesPaymentResource res)
        {
            res.TotalPayment = res.Details.Sum(x => x.Payment);
            res.TotalReturn = res.Returns.Sum(x => x.TotalAfterTax);
            res.AmountPaid = res.TotalPayment - res.TotalReturn;
        }

        private void ValidateResource(SalesPaymentResource res)
        {
            if (res.DocumentDate == default)
                throw new BadRequestException("Document date cannot be empty.");

            if (res.CustomerId.IsGuidEmpty())
                throw new BadRequestException("Customer cannot be empty.");

            if (res.LocationId.IsGuidEmpty())
                throw new BadRequestException("Location cannot be empty.");
        }

        public string GetNewDocumentNumber(DateTime date)
        {
            var prefix = $"PP-{date:yyMM}";

            var lastNumber = db.GetQuery<SalesPayment>()
                .Where(x => x.DocumentNumber.StartsWith(prefix))
                .OrderByDescending(x => x.DocumentNumber)
                .Select(x => x.DocumentNumber)
                .FirstOrDefault();

            if (lastNumber == null) return prefix + "0001";

            var newNumber = lastNumber.Substring(prefix.Length, 4).ToInteger() + 1;
            return prefix + newNumber.ToString("0000");
        }

        public Guid Create(SalesPaymentResource res, Guid? userId)
        {
            ValidateResource(res);

            Calculate(res);

            var payment = new SalesPayment();
            payment.MapFrom(res);
            payment.DocumentNumber = GetNewDocumentNumber(res.DocumentDate);
            db.Create(payment, userId);

            foreach (var detail in res.Details)
            {
                if (detail.Payment == 0) continue;
                if (detail.SalesDeliveryId.IsGuidEmpty()) continue;

                var paymentDetail = new SalesPaymentDetail();
                paymentDetail.MapFrom(detail);
                paymentDetail.SalesPaymentId = payment.Id;
                db.Create(paymentDetail, userId);

                var delivery = db.Get<SalesDelivery>(detail.SalesDeliveryId);
                delivery.TotalPayment += detail.Payment;
                delivery.Balance -= detail.Payment;

                if (delivery.Balance < 0)
                    throw new BadRequestException($"Payment amount for {delivery.DocumentNumber} exceeds the remaining balance.");

                delivery.Status = delivery.Balance == 0 ? PaymentStatus.Paid : PaymentStatus.Unpaid;
            }

            foreach (var paymentRet in res.Returns)
            {
                if (paymentRet.Check)
                {
                    var ret = db.Get<SalesReturn>(paymentRet.Id);
                    ret.Status = ReturnStatus.Used;
                    ret.SalesPaymentId = payment.Id;
                }
            }

            db.SaveChanges();

            return payment.Id;
        }

        public Guid Update(SalesPaymentResource res, Guid? userId)
        {
            ValidateResource(res);

            var transaction = db.Database.BeginTransaction();

            try
            {
                // delete old
                var listOldDetail = db.GetQuery<SalesPaymentDetail>()
                    .Where(x => x.SalesPaymentId == res.Id)
                    .ToList();

                foreach (var oldDetail in listOldDetail)
                {
                    var delivery = db.Get<SalesDelivery>(oldDetail.SalesDeliveryId);
                    delivery.TotalPayment -= oldDetail.Payment;
                    delivery.Balance += oldDetail.Payment;
                    delivery.Status = PaymentStatus.Unpaid;
                }

                db.SaveChanges();

                db.Database.ExecuteSqlRaw("UPDATE SalesReturn SET Status = 'Unused' WHERE SalesPaymentId = {0} AND IsDeleted = 0", res.Id);
                db.Database.ExecuteSqlRaw("DELETE FROM SalesPaymentDetail WHERE SalesPaymentId = {0} AND IsDeleted = 0", res.Id);


                //update
                Calculate(res);

                var payment = db.Get<SalesPayment>(res.Id);
                payment.MapFrom(res);
                db.Update(payment, userId);

                foreach (var detail in res.Details)
                {
                    if (detail.Payment == 0) continue;
                    if (detail.SalesDeliveryId.IsGuidEmpty()) continue;

                    var paymentDetail = new SalesPaymentDetail();
                    paymentDetail.MapFrom(detail);
                    paymentDetail.SalesPaymentId = payment.Id;
                    db.Create(paymentDetail, userId);

                    var delivery = db.Get<SalesDelivery>(detail.SalesDeliveryId);
                    delivery.TotalPayment += detail.Payment;
                    delivery.Balance -= detail.Payment;

                    if (delivery.Balance < 0)
                        throw new BadRequestException($"Payment amount for {delivery.DocumentNumber} exceeds the remaining balance.");

                    delivery.Status = delivery.Balance == 0 ? PaymentStatus.Paid : PaymentStatus.Unpaid;
                }

                foreach (var paymentRet in res.Returns)
                {
                    if (paymentRet.Check)
                    {
                        var ret = db.Get<SalesReturn>(paymentRet.Id);
                        ret.Status = ReturnStatus.Used;
                        ret.SalesPaymentId = payment.Id;
                    }
                }

                db.SaveChanges();

                transaction.Commit();

                return res.Id;
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
        }

        public void Delete(Guid? id, Guid? userId)
        {
            var transaction = db.Database.BeginTransaction();

            try
            {
                // delete old
                var listOldDetail = db.GetQuery<SalesPaymentDetail>()
                    .Where(x => x.SalesPaymentId == id)
                    .ToList();

                foreach (var oldDetail in listOldDetail)
                {
                    var delivery = db.Get<SalesDelivery>(oldDetail.SalesDeliveryId);
                    delivery.TotalPayment -= oldDetail.Payment;
                    delivery.Balance += oldDetail.Payment;
                    delivery.Status = PaymentStatus.Unpaid;
                }

                db.Database.ExecuteSqlRaw("UPDATE SalesReturn SET Status = 'Unused' WHERE SalesPaymentId = {0} AND IsDeleted = 0", id);

                db.Delete<SalesPaymentDetail>(x => x.SalesPaymentId == id, userId);
                db.Delete<SalesPayment>(id, userId);

                db.SaveChanges();

                transaction.Commit();
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
        }
    }
}
