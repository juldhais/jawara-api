﻿using Jawara.Helpers;
using Jawara.Models;
using Jawara.Repositories;
using Jawara.Resources;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Jawara.Services
{
    public class SectionService
    {
        private readonly DataContext db;

        public SectionService(DataContext db)
        {
            this.db = db;
        }

        public SectionResource Get(Guid? id)
        {
            return db.Get<Section, SectionResource>(id);
        }

        public PagingResult<SectionResource> GetList(string keyword, int page, int size)
        {
            var query = db.GetQuery<Section>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.Description.Contains(keyword));

            var result = query.OrderBy(x => x.Description)
                .PaginateTo<Section, SectionResource>(page, size);

            return result;
        }

        public List<LookupResource> GetLookup(string keyword)
        {
            if (keyword.IsEmpty() || keyword.Length < 2)
                return new List<LookupResource>();

            var query = db.GetQuery<Section>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.Description.Contains(keyword));

            var result = query.OrderBy(x => x.Code)
                .Select(x => new LookupResource
                {
                    Id = x.Id,
                    Code = x.Code + " ─ " + x.Description
                }).ToList();

            return result;
        }

        public LookupResource GetSingleLookup(Guid? id)
        {
            var result = db.GetQuery<Section>()
                .Where(x => x.Id == id)
                .Select(x => new LookupResource
                {
                    Id = x.Id,
                    Code = x.Code + " ─ " + x.Description
                }).FirstOrDefault();

            return result;
        }

        public void ValidateResource(SectionResource res)
        {
            if (res.Code.IsEmpty())
                throw new BadRequestException("Code cannot be empty.");

            if (db.GetQuery<Section>().Any(x => x.Code == res.Code && x.Id != res.Id))
                throw new BadRequestException("Code is already used.");
        }

        public Guid Create(SectionResource res, Guid? userId)
        {
            ValidateResource(res);

            var unit = new Section();
            unit.MapFrom(res);
            db.Create(unit, userId);
            db.SaveChanges();

            return unit.Id;
        }

        public Guid Update(SectionResource res, Guid? userId)
        {
            ValidateResource(res);

            var unit = db.Get<Section>(res.Id);
            unit.MapFrom(res);
            db.Update(unit, userId);
            db.SaveChanges();

            return unit.Id;
        }

        public void Delete(Guid? id, Guid? userId)
        {
            db.Delete<Section>(id, userId);
            db.SaveChanges();
        }
    }
}