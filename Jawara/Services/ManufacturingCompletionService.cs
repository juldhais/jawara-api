﻿using Jawara.Enums;
using Jawara.Helpers;
using Jawara.Models;
using Jawara.Repositories;
using Jawara.Resources;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Jawara.Services
{
    public class ManufacturingCompletionService
    {
        private readonly DataContext db;
        private readonly ItemService itemService;
        private readonly ManufacturingOrderService manufacturingOrderService;

        public ManufacturingCompletionService(DataContext db,
            ItemService itemService,
            ManufacturingOrderService manufacturingOrderService)
        {
            this.db = db;
            this.itemService = itemService;
            this.manufacturingOrderService = manufacturingOrderService;
        }

        public ManufacturingCompletionResource Get(Guid? id)
        {
            var completion = db.Get<ManufacturingCompletion, ManufacturingCompletionResource>(id);
            if (completion != null)
            {
                completion.Details = db.ManufacturingCompletionDetail
                    .Where(x => x.IsDeleted == false
                                && x.ManufacturingCompletionId == id)
                    .OrderBy(x => x.Sequence)
                    .Project().To<ManufacturingCompletionDetailResource>()
                    .ToList();

                if (completion.Details == null)
                    completion.Details = new List<ManufacturingCompletionDetailResource>();

                completion.CreatedBy = db.GetQuery<User>()
                    .Where(x => x.Id == completion.CreatedById)
                    .Select(x => x.Description)
                    .FirstOrDefault();

                completion.UpdatedBy = db.GetQuery<User>()
                    .Where(x => x.Id == completion.UpdatedById)
                    .Select(x => x.Description)
                    .FirstOrDefault();
            }

            return completion;
        }

        public PagingResult<ManufacturingCompletionResource> GetList(
            string keyword,
            Guid? sectionId,
            Guid? bomId,
            Guid? parentId,
            Guid? locationId,
            DateTime? startDate,
            DateTime? endDate,
            int page,
            int size)
        {
            if (keyword.IsEmpty()
                && sectionId.IsGuidEmpty()
                && bomId.IsGuidEmpty()
                && parentId.IsGuidEmpty()
                && locationId.IsGuidEmpty()
                && startDate == null && endDate == null
                && page == 0 && size == 0)
                return new PagingResult<ManufacturingCompletionResource>();

            var query = db.GetQuery<ManufacturingCompletion>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.DocumentNumber.Contains(keyword)
                    || x.Remarks.Contains(keyword));

            if (sectionId.IsGuidNotEmpty())
                query = query.Where(x => x.SectionId == sectionId);

            if (bomId.IsGuidNotEmpty())
                query = query.Where(x => x.BillOfMaterialId == bomId);

            if (parentId.IsGuidNotEmpty())
                query = query.Where(x => x.ParentId == parentId);

            if (locationId.IsGuidNotEmpty())
                query = query.Where(x => x.LocationId == locationId);

            if (startDate != null)
                query = query.Where(x => x.DocumentDate.Date >= startDate);

            if (endDate != null)
                query = query.Where(x => x.DocumentDate.Date <= endDate);

            query = query.OrderByDescending(x => x.DocumentNumber);

            var result = query.PaginateTo<ManufacturingCompletion, ManufacturingCompletionResource>(page, size);

            return result;
        }

        private void ValidateResource(ManufacturingCompletionResource res)
        {
            if (res.DocumentDate == default)
                throw new BadRequestException("Document date cannot be empty.");

            if (res.BillOfMaterialId.IsGuidEmpty())
                throw new BadRequestException("Bill of Material cannot be empty.");

            if (res.ParentId.IsGuidEmpty())
                throw new BadRequestException("Parent Item cannot be empty.");

            if (res.LocationId.IsGuidEmpty())
                throw new BadRequestException("Location cannot be empty.");

            if (res.Quantity == 0)
                throw new BadRequestException("Quantity cannot be empty.");

            if (res.Ratio == 0)
                throw new BadRequestException("Ratio cannot be empty.");
        }

        public string GetNewDocumentNumber(DateTime date)
        {
            var prefix = $"MC-{date:yyMM}";

            var lastNumber = db.GetQuery<ManufacturingCompletion>()
                .Where(x => x.DocumentNumber.StartsWith(prefix))
                .OrderByDescending(x => x.DocumentNumber)
                .Select(x => x.DocumentNumber)
                .FirstOrDefault();

            if (lastNumber == null) return prefix + "0001";

            var newNumber = lastNumber.Substring(prefix.Length, 4).ToInteger() + 1;
            return prefix + newNumber.ToString("0000");
        }

        public Guid Create(ManufacturingCompletionResource res, Guid? userId)
        {
            ValidateResource(res);

            var transaction = db.Database.BeginTransaction();

            try
            {
                var completion = new ManufacturingCompletion();
                completion.MapFrom(res);
                completion.DocumentNumber = GetNewDocumentNumber(res.DocumentDate);
                db.Create(completion, userId);

                var parentStockable = db.GetQuery<Item>()
                    .Where(x => x.Id == completion.ParentId)
                    .Select(x => x.Stockable)
                    .FirstOrDefault();
                if (parentStockable)
                {
                    itemService.AddStockMovementIn(
                        transactionId: completion.Id,
                        documentNumber: completion.DocumentNumber,
                        documentDate: completion.DocumentDate,
                        transactionType: TransactionType.ManufacturingCompletion,
                        itemId: completion.ParentId,
                        locationId: completion.LocationId,
                        quantity: completion.Quantity * completion.Ratio,
                        cost: completion.Cost / completion.Ratio);
                }

                var sequence = 0;
                foreach (var detail in res.Details)
                {
                    if (detail.ComponentId.IsGuidEmpty()) continue;
                    if (detail.Quantity == 0) continue;

                    var componentStockable = db.GetQuery<Item>()
                        .Where(x => x.Id == detail.ComponentId)
                        .Select(x => x.Stockable)
                        .FirstOrDefault();
                    if (componentStockable)
                    {
                        itemService.AddStockMovementOut(
                        transactionId: completion.Id,
                        documentNumber: completion.DocumentNumber,
                        documentDate: completion.DocumentDate,
                        transactionType: TransactionType.ManufacturingCompletion,
                        itemId: detail.ComponentId,
                        locationId: completion.LocationId,
                        quantity: detail.Quantity * detail.Ratio);
                    }

                    var completionDetail = new ManufacturingCompletionDetail();
                    completionDetail.MapFrom(detail);
                    completionDetail.ManufacturingCompletionId = completion.Id;
                    completionDetail.Sequence = ++sequence;
                    db.Create(completionDetail, userId);
                }

                db.SaveChanges();

                manufacturingOrderService.CalculateQuantityCompleted(completion.ManufacturingOrderId);

                transaction.Commit();

                return completion.Id;
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
        }

        public Guid Update(ManufacturingCompletionResource res, Guid? userId)
        {
            ValidateResource(res);

            var transaction = db.Database.BeginTransaction();

            try
            {
                //delete old
                var oldCompletion = Get(res.Id);
                itemService.RemoveStockMovement(oldCompletion.Id);
                db.Database.ExecuteSqlRaw("DELETE FROM ManufacturingCompletionDetail WHERE ManufacturingCompletionId = {0} AND IsDeleted = 0", res.Id);

                //update
                var completion = db.Get<ManufacturingCompletion>(res.Id);

                completion.MapFrom(res);
                db.Update(completion, userId);

                var parentStockable = db.GetQuery<Item>()
                   .Where(x => x.Id == completion.ParentId)
                   .Select(x => x.Stockable)
                   .FirstOrDefault();
                if (parentStockable)
                {
                    itemService.AddStockMovementIn(
                        transactionId: completion.Id,
                        documentNumber: completion.DocumentNumber,
                        documentDate: completion.DocumentDate,
                        transactionType: TransactionType.ManufacturingCompletion,
                        itemId: completion.ParentId,
                        locationId: completion.LocationId,
                        quantity: completion.Quantity * completion.Ratio,
                        cost: completion.Cost / completion.Ratio);
                }

                var sequence = 0;
                foreach (var detail in res.Details)
                {
                    if (detail.ComponentId.IsGuidEmpty()) continue;
                    if (detail.Quantity == 0) continue;

                    var componentStockable = db.GetQuery<Item>()
                        .Where(x => x.Id == detail.ComponentId)
                        .Select(x => x.Stockable)
                        .FirstOrDefault();
                    if (componentStockable)
                    {
                        itemService.AddStockMovementOut(
                        transactionId: completion.Id,
                        documentNumber: completion.DocumentNumber,
                        documentDate: completion.DocumentDate,
                        transactionType: TransactionType.ManufacturingCompletion,
                        itemId: detail.ComponentId,
                        locationId: completion.LocationId,
                        quantity: detail.Quantity * detail.Ratio);
                    }

                    var completionDetail = new ManufacturingCompletionDetail();
                    completionDetail.MapFrom(detail);
                    completionDetail.ManufacturingCompletionId = completion.Id;
                    completionDetail.Sequence = ++sequence;
                    db.Create(completionDetail, userId);
                }

                db.SaveChanges();

                manufacturingOrderService.CalculateQuantityCompleted(completion.ManufacturingOrderId);

                transaction.Commit();

                return res.Id;
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
        }

        public void Delete(Guid? id, Guid? userId)
        {
            var transaction = db.Database.BeginTransaction();

            try
            {
                itemService.RemoveStockMovement(id);
                db.Delete<ManufacturingCompletionDetail>(x => x.ManufacturingCompletionId == id, userId);
                db.Delete<ManufacturingCompletion>(id, userId);
                db.SaveChanges();

                var manufacturingOrderId = db.GetQuery<ManufacturingCompletion>()
                    .Where(x => x.Id == id)
                    .Select(x => x.ManufacturingOrderId)
                    .FirstOrDefault();
                manufacturingOrderService.CalculateQuantityCompleted(manufacturingOrderId);

                transaction.Commit();
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
        }
    }
}
