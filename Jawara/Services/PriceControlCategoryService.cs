﻿using Jawara.Helpers;
using Jawara.Models;
using Jawara.Repositories;
using Jawara.Resources;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Jawara.Services
{
    public class PriceControlCategoryService
    {
        private readonly DataContext db;

        public PriceControlCategoryService(DataContext db)
        {
            this.db = db;
        }

        public PriceControlCategoryResource Get(Guid? id)
        {
            return db.Get<PriceControlCategory, PriceControlCategoryResource>(id);
        }

        public PagingResult<PriceControlCategoryResource> GetList(string keyword, int page, int size)
        {
            var query = db.GetQuery<PriceControlCategory>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.Code.Contains(keyword));

            var result = query.OrderBy(x => x.Code)
                .PaginateTo<PriceControlCategory, PriceControlCategoryResource>(page, size);

            return result;
        }

        public List<LookupResource> GetLookup(string keyword)
        {
            if (keyword.IsEmpty() || keyword.Length < 2)
                return new List<LookupResource>();

            var query = db.GetQuery<PriceControlCategory>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.Code.Contains(keyword));

            var result = query.OrderBy(x => x.Code)
                .Select(x => new LookupResource
                {
                    Id = x.Id,
                    Code = x.Code
                }).ToList();

            return result;
        }

        public LookupResource GetSingleLookup(Guid? id)
        {
            var result = db.GetQuery<PriceControlCategory>()
                .Where(x => x.Id == id)
                .Select(x => new LookupResource
                {
                    Id = x.Id,
                    Code = x.Code
                }).FirstOrDefault();

            return result;
        }

        public void ValidateResource(PriceControlCategoryResource res)
        {
            if (res.Code.IsEmpty())
                throw new BadRequestException("Code cannot be empty.");

            if (db.GetQuery<PriceControlCategory>().Any(x => x.Code == res.Code && x.Id != res.Id))
                throw new BadRequestException("Code is already used.");
        }

        public Guid Create(PriceControlCategoryResource res, Guid? userId)
        {
            ValidateResource(res);

            var category = new PriceControlCategory();
            category.MapFrom(res);
            db.Create(category, userId);
            db.SaveChanges();

            return category.Id;
        }

        public Guid Update(PriceControlCategoryResource res, Guid? userId)
        {
            ValidateResource(res);

            var category = db.Get<PriceControlCategory>(res.Id);
            category.MapFrom(res);
            db.Update(category, userId);
            db.SaveChanges();

            return category.Id;
        }

        public void Delete(Guid? id, Guid? userId)
        {
            db.Delete<PriceControlCategory>(id, userId);
            db.SaveChanges();
        }
    }
}
