﻿using Jawara.Helpers;
using Jawara.Models;
using Jawara.Repositories;
using Jawara.Resources;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Jawara.Services
{
    public class RoleService
    {
        private readonly DataContext db;

        public RoleService(DataContext db)
        {
            this.db = db;
        }

        public RoleResource Get(Guid? id)
        {
            var role = db.Get<Role, RoleResource>(id);
            role.Privileges = db.GetQuery<RolePrivilege>()
                .Where(x => x.RoleId == id)
                .OrderBy(x => x.Privilege)
                .Select(x => x.Privilege)
                .ToList();

            return role;
        }

        public PagingResult<RoleResource> GetList(string keyword, int page, int size)
        {
            var query = db.GetQuery<Role>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.Code.Contains(keyword));

            var result = query.OrderBy(x => x.Code)
                .PaginateTo<Role, RoleResource>(page, size);

            return result;
        }

        public List<LookupResource> GetLookup(string keyword)
        {
            if (keyword.IsEmpty() || keyword.Length < 2)
                return new List<LookupResource>();

            var query = db.GetQuery<Role>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.Code.Contains(keyword));

            var result = query.OrderBy(x => x.Code)
                .Select(x => new LookupResource
                {
                    Id = x.Id,
                    Code = x.Code
                }).ToList();

            return result;
        }

        public LookupResource GetSingleLookup(Guid? id)
        {
            var result = db.GetQuery<Role>()
                .Where(x => x.Id == id)
                .Select(x => new LookupResource
                {
                    Id = x.Id,
                    Code = x.Code
                }).FirstOrDefault();

            return result;
        }

        public void ValidateResource(RoleResource res)
        {
            if (res.Code.IsEmpty())
                throw new BadRequestException("Code cannot be empty.");

            if (db.GetQuery<Role>().Any(x => x.Code == res.Code && x.Id != res.Id))
                throw new BadRequestException("Code is already used.");
        }

        public Guid Create(RoleResource res, Guid? userId)
        {
            ValidateResource(res);

            var role = new Role();
            role.MapFrom(res);
            db.Create(role, userId);

            foreach (var priv in res.Privileges)
            {
                var privilege = new RolePrivilege();
                privilege.Privilege = priv;
                privilege.RoleId = role.Id;
                db.Create(privilege, userId);
            }

            db.SaveChanges();

            return role.Id;
        }

        public Guid Update(RoleResource res, Guid? userId)
        {
            ValidateResource(res);

            db.Remove<RolePrivilege>(x => x.RoleId == res.Id);

            var role = db.Get<Role>(res.Id);
            role.MapFrom(res);
            db.Update(role, userId);

            foreach (var priv in res.Privileges)
            {
                var privilege = new RolePrivilege();
                privilege.Privilege = priv;
                privilege.RoleId = role.Id;
                db.Create(privilege, userId);
            }

            db.SaveChanges();

            return role.Id;
        }

        public void Delete(Guid? id, Guid? userId)
        {
            db.Delete<RolePrivilege>(x => x.RoleId == id, userId);
            db.Delete<Role>(id, userId);
            db.SaveChanges();
        }

        public void CheckPrivilege(Guid? roleId, string privilege)
        {
            var isAdministrator = db.GetQuery<Role>()
                    .Where(x => x.Id == roleId)
                    .Select(x => x.IsAdministrator)
                    .FirstOrDefault();

            if (isAdministrator) return;

            var hasPrivilege = db.GetQuery<RolePrivilege>()
                .Any(x => x.RoleId == roleId
                    && x.Privilege == privilege);

            if (!hasPrivilege) throw new UnauthorizedException();
        }
    }
}
