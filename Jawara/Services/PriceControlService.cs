﻿using Jawara.Helpers;
using Jawara.Models;
using Jawara.Repositories;
using Jawara.Resources;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Jawara.Services
{
    public class PriceControlService
    {
        private readonly DataContext db;

        public PriceControlService(DataContext db)
        {
            this.db = db;
        }

        public PriceControlResource Get(Guid? id)
        {
            return db.Get<PriceControl, PriceControlResource>(id);
        }

        public PagingResult<PriceControlResource> GetList(Guid? priceControlCategoryId, Guid? itemId, int page, int size)
        {
            var query = db.GetQuery<PriceControl>();

            if (priceControlCategoryId.IsGuidNotEmpty())
                query = query.Where(x => x.PriceControlCategoryId == priceControlCategoryId);

            if (itemId.IsGuidNotEmpty())
                query = query.Where(x => x.ItemId == itemId);

            var result = query.OrderBy(x => x.Item.Description)
                .ThenBy(x => x.PriceControlCategory.Code)
                .PaginateTo<PriceControl, PriceControlResource>(page, size);

            return result;
        }

        public void ValidateResource(PriceControlResource res)
        {
            if (res.PriceControlCategoryId.IsGuidEmpty())
                throw new BadRequestException("Sales Price Category cannot be empty.");

            if (res.ItemId.IsEmpty())
                throw new BadRequestException("Item cannot be empty.");

            if (db.GetQuery<PriceControl>()
                  .Any(x => x.PriceControlCategoryId == res.PriceControlCategoryId
                         && x.ItemId == res.ItemId
                         && x.MinimumQuantity == res.MinimumQuantity
                         && x.Unit == res.Unit
                         && x.Ratio == res.Ratio
                         && x.Id != res.Id))
                throw new BadRequestException("Item Sales Price is already exist.");
        }

        public Guid Create(PriceControlResource res, Guid? userId)
        {
            ValidateResource(res);

            var price = new PriceControl();
            price.MapFrom(res);
            db.Create(price, userId);
            db.SaveChanges();

            return price.Id;
        }

        public Guid Update(PriceControlResource res, Guid? userId)
        {
            ValidateResource(res);

            var price = db.Get<PriceControl>(res.Id);
            price.MapFrom(res);
            db.Update(price, userId);
            db.SaveChanges();

            return price.Id;
        }

        public void Delete(Guid? id, Guid? userId)
        {
            db.Delete<PriceControl>(id, userId);
            db.SaveChanges();
        }

        public void BatchUpdate(BatchUpdatePriceControlResource resource, Guid? userId)
        {
            if (resource.PriceControlCategoryId.IsGuidEmpty())
                throw new BadRequestException("Sales Price Category cannot be empty.");

            if (resource.ItemCategoryId.IsGuidEmpty()
                && resource.ItemSubcategoryId.IsGuidEmpty()
                && resource.ItemTag.IsEmpty())
                throw new BadRequestException("Item category/subcategory/tag cannot be empty.");

            var listItem = new List<Item>();
            var queryItem = db.GetQuery<Item>();

            if (resource.ItemCategoryId.IsGuidNotEmpty())
                queryItem = queryItem.Where(x => x.ItemCategoryId == resource.ItemCategoryId);

            if (resource.ItemSubcategoryId.IsGuidNotEmpty())
                queryItem = queryItem.Where(x => x.ItemSubcategoryId == resource.ItemSubcategoryId);

            if (resource.ItemTag.IsNotEmpty())
                queryItem = queryItem.Where(x => x.Tag.Contains(resource.ItemTag));

            listItem = queryItem.ToList();

            if (resource.ItemTag.IsNotEmpty())
                listItem = listItem.Where(x => x.Tag.Split(", ").Contains(resource.ItemTag)).ToList();


            foreach (var item in listItem)
            {
                var priceControl = db.GetQuery<PriceControl>()
                    .Where(x => x.ItemId == item.Id
                        && x.PriceControlCategoryId == resource.PriceControlCategoryId
                        && x.Unit == resource.Unit
                        && x.Ratio == resource.Ratio
                        && x.MinimumQuantity == resource.MinimumQuantity)
                    .FirstOrDefault();

                if (priceControl == null)
                {
                    priceControl = new PriceControl();
                    priceControl.PriceControlCategoryId = resource.PriceControlCategoryId;
                    priceControl.ItemId = item.Id;
                    priceControl.Unit = resource.Unit;
                    priceControl.Ratio = resource.Ratio;
                    priceControl.MinimumQuantity = resource.MinimumQuantity;
                    priceControl.Price = resource.Price;
                    db.Create(priceControl, userId);
                    continue;
                }

                priceControl.Price = resource.Price;
                db.Update(priceControl, userId);
            }

            db.SaveChanges();
        }

        public ImportResultResource Import(List<PriceControlImportResource> list, Guid? userId)
        {
            var transaction = db.Database.BeginTransaction();

            try
            {
                var result = new ImportResultResource();

                var line = 0;
                foreach (var res in list)
                {
                    line++;

                    if (res.Item.IsEmpty())
                        throw new BadRequestException($"[{line}] Item cannot be empty.");

                    if (res.PriceControlCategory.IsEmpty())
                        throw new BadRequestException($"[{line}] Price Control Category cannot be empty.");

                    var item = db.GetQuery<Item>()
                        .Where(x => x.Code == res.Item || x.Description == res.Item)
                        .FirstOrDefault();

                    if (item == null)
                        throw new BadRequestException($"[{line}] Item {res.Item} not found.");

                    var category = db.GetQuery<PriceControlCategory>()
                        .Where(x => x.Code == res.PriceControlCategory)
                        .FirstOrDefault();

                    if (category == null)
                        throw new BadRequestException($"[{line}] Sales Price Category {res.PriceControlCategory} not found.");

                    var unit = res.Unit;
                    var ratio = res.Ratio;
                    if (unit.IsEmpty() || ratio == 0)
                    {
                        unit = item.Unit;
                        ratio = 1;
                    }


                    bool isNew = false;

                    var priceControl = db.GetQuery<PriceControl>()
                        .Where(x => x.ItemId == item.Id
                            && x.PriceControlCategoryId == category.Id
                            && x.Unit == unit && x.Ratio == ratio)
                        .FirstOrDefault();

                    if (priceControl == null)
                    {
                        priceControl = new PriceControl();
                        isNew = true;
                    }

                    priceControl.ItemId = item?.Id;
                    priceControl.PriceControlCategoryId = category?.Id;
                    priceControl.Price = res.Price;
                    priceControl.MinimumQuantity = res.MinimumQuantity;
                    priceControl.Unit = unit;
                    priceControl.Ratio = ratio;

                    if (isNew) db.Create(priceControl, userId);
                    else db.Update(priceControl, userId);
                }

                db.SaveChanges();
                transaction.Commit();

                return result;
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
        }
    }
}
