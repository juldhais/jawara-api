﻿using Jawara.Enums;
using Jawara.Helpers;
using Jawara.Models;
using Jawara.Repositories;
using Jawara.Resources;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace Jawara.Services
{
    public class StockAdjustmentService
    {
        private readonly DataContext db;
        private readonly ItemService itemService;

        public StockAdjustmentService(DataContext db,
            ItemService itemService)
        {
            this.db = db;
            this.itemService = itemService;
        }

        public StockAdjustmentResource Get(Guid? id)
        {
            var adjustment = db.Get<StockAdjustment, StockAdjustmentResource>(id);
            if (adjustment != null)
            {
                var listDetail = db.GetQuery<StockAdjustmentDetail>()
                    .Where(x => x.StockAdjustmentId == id)
                    .OrderBy(x => x.Sequence)
                    .Project().To<StockAdjustmentDetailResource>()
                    .ToList();

                if (listDetail != null) adjustment.Details = listDetail;

                adjustment.CreatedBy = db.GetQuery<User>()
                    .Where(x => x.Id == adjustment.CreatedById)
                    .Select(x => x.Description)
                    .FirstOrDefault();

                adjustment.UpdatedBy = db.GetQuery<User>()
                    .Where(x => x.Id == adjustment.UpdatedById)
                    .Select(x => x.Description)
                    .FirstOrDefault();
            }

            return adjustment;
        }

        public PagingResult<StockAdjustmentResource> GetList(string keyword, Guid? locationId, DateTime? startDate, DateTime? endDate, int page, int size)
        {
            if (keyword.IsEmpty()
                && locationId.IsGuidEmpty()
                && startDate == null && endDate == null
                && page == 0 && size == 0)
                return new PagingResult<StockAdjustmentResource>();

            var query = db.GetQuery<StockAdjustment>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.DocumentNumber.Contains(keyword)
                    || x.Remarks.Contains(keyword));

            if (locationId.IsGuidNotEmpty())
                query = query.Where(x => x.LocationId == locationId);

            if (startDate != null)
                query = query.Where(x => x.DocumentDate.Date >= startDate);

            if (endDate != null)
                query = query.Where(x => x.DocumentDate.Date <= endDate);

            query = query.OrderByDescending(x => x.DocumentNumber);

            var result = query.PaginateTo<StockAdjustment, StockAdjustmentResource>(page, size);

            return result;
        }

        private void ValidateResource(StockAdjustmentResource res)
        {
            if (res.DocumentDate == default)
                throw new BadRequestException("Document date cannot be empty.");

            if (res.LocationId.IsGuidEmpty())
                throw new BadRequestException("Location cannot be empty.");
        }

        public string GetNewDocumentNumber(DateTime date)
        {
            var prefix = $"IA-{date:yyMM}";

            var lastNumber = db.GetQuery<StockAdjustment>()
                .Where(x => x.DocumentNumber.StartsWith(prefix))
                .OrderByDescending(x => x.DocumentNumber)
                .Select(x => x.DocumentNumber)
                .FirstOrDefault();

            if (lastNumber == null) return prefix + "0001";

            var newNumber = lastNumber.Substring(prefix.Length, 4).ToInteger() + 1;
            return prefix + newNumber.ToString("0000");
        }

        public Guid Create(StockAdjustmentResource res, Guid? userId)
        {
            ValidateResource(res);

            var transaction = db.Database.BeginTransaction();

            try
            {
                var adjustment = new StockAdjustment();
                adjustment.MapFrom(res);
                adjustment.DocumentNumber = GetNewDocumentNumber(res.DocumentDate);
                db.Create(adjustment, userId);

                // detail
                var sequence = 0;
                foreach (var detail in res.Details)
                {
                    if (detail.ItemId.IsGuidEmpty()) continue;
                    if (detail.Quantity == 0 || detail.Ratio == 0) continue;

                    var adjustmentDetail = new StockAdjustmentDetail();
                    adjustmentDetail.MapFrom(detail);
                    adjustmentDetail.StockAdjustmentId = adjustment.Id;
                    adjustmentDetail.Sequence = ++sequence;
                    db.Create(adjustmentDetail, userId);

                    var itemStockable = db.GetQuery<Item>()
                        .Where(x => x.Id == detail.ItemId)
                        .Select(x => x.Stockable)
                        .FirstOrDefault();
                    if (itemStockable)
                    {
                        if (detail.Quantity > 0)
                        {
                            itemService.AddStockMovementIn(
                                transactionId: adjustment.Id,
                                documentNumber: adjustment.DocumentNumber,
                                documentDate: adjustment.DocumentDate,
                                transactionType: TransactionType.StockAdjustment,
                                itemId: adjustmentDetail.ItemId,
                                locationId: adjustment.LocationId,
                                quantity: adjustmentDetail.Quantity * adjustmentDetail.Ratio,
                                cost: adjustmentDetail.Cost / adjustmentDetail.Ratio);
                        }
                        else
                        {
                            itemService.AddStockMovementOut(
                                transactionId: adjustment.Id,
                                documentNumber: adjustment.DocumentNumber,
                                documentDate: adjustment.DocumentDate,
                                transactionType: TransactionType.StockAdjustment,
                                itemId: adjustmentDetail.ItemId,
                                locationId: adjustment.LocationId,
                                quantity: adjustmentDetail.Quantity * adjustmentDetail.Ratio);
                        }
                    }
                }

                db.SaveChanges();

                transaction.Commit();

                return adjustment.Id;
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
        }

        public Guid Update(StockAdjustmentResource res, Guid? userId)
        {
            ValidateResource(res);

            var transaction = db.Database.BeginTransaction();

            try
            {
                // delete old
                itemService.RemoveStockMovement(res.Id);
                db.Database.ExecuteSqlRaw("DELETE FROM StockAdjustmentDetail WHERE StockAdjustmentId = {0} AND IsDeleted = 0", res.Id);

                //update
                var adjustment = db.Get<StockAdjustment>(res.Id);
                adjustment.MapFrom(res);
                db.Update(adjustment, userId);

                var sequence = 0;
                foreach (var detail in res.Details)
                {
                    if (detail.ItemId.IsGuidEmpty()) continue;
                    if (detail.Quantity == 0) continue;

                    var adjustmentDetail = new StockAdjustmentDetail();
                    adjustmentDetail.MapFrom(detail);
                    adjustmentDetail.StockAdjustmentId = adjustment.Id;
                    adjustmentDetail.Sequence = ++sequence;
                    db.Create(adjustmentDetail, userId);

                    var itemStockable = db.GetQuery<Item>()
                        .Where(x => x.Id == detail.ItemId)
                        .Select(x => x.Stockable)
                        .FirstOrDefault();
                    if (itemStockable)
                    {
                        if (detail.Quantity > 0)
                        {
                            itemService.AddStockMovementIn(
                                transactionId: adjustment.Id,
                                documentNumber: adjustment.DocumentNumber,
                                documentDate: adjustment.DocumentDate,
                                transactionType: TransactionType.StockAdjustment,
                                itemId: adjustmentDetail.ItemId,
                                locationId: adjustment.LocationId,
                                quantity: adjustmentDetail.Quantity * adjustmentDetail.Ratio,
                                cost: adjustmentDetail.Cost / adjustmentDetail.Ratio);
                        }
                        else
                        {
                            itemService.AddStockMovementOut(
                                transactionId: adjustment.Id,
                                documentNumber: adjustment.DocumentNumber,
                                documentDate: adjustment.DocumentDate,
                                transactionType: TransactionType.StockAdjustment,
                                itemId: adjustmentDetail.ItemId,
                                locationId: adjustment.LocationId,
                                quantity: adjustmentDetail.Quantity * adjustmentDetail.Ratio);
                        }
                    }
                }

                db.SaveChanges();

                transaction.Commit();

                return res.Id;
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
        }

        public void Delete(Guid? id, Guid? userId)
        {
            var transaction = db.Database.BeginTransaction();

            try
            {
                // delete old
                itemService.RemoveStockMovement(id);
                db.Delete<StockAdjustmentDetail>(x => x.StockAdjustmentId == id, userId);
                db.Delete<StockAdjustment>(id, userId);
                db.SaveChanges();

                transaction.Commit();
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
        }
    }
}
