﻿using Jawara.Helpers;
using Jawara.Models;
using Jawara.Resources;
using Jawara.Repositories;
using System.Data;
using System.Linq;

namespace Jawara.Services
{
    public class SalesReportService
    {
        private readonly DataContext db;

        public SalesReportService(DataContext db)
        {
            this.db = db;
        }

        public SalesReportResource GetReport(SalesReportParam param)
        {
            if (param == null) return new SalesReportResource();

            var result = new SalesReportResource();
            result.StartDate = param.StartDate;
            result.EndDate = param.EndDate;
            result.Status = param.Status;

            var query = db.GetQuery<SalesDelivery>()
                .Where(x => x.DocumentDate.Date >= param.StartDate
                        && x.DocumentDate.Date <= param.EndDate);

            if (param.LocationId.IsGuidNotEmpty())
            {
                query = query.Where(x => x.LocationId == param.LocationId);
                result.Location = db.GetQuery<Location>()
                    .Where(x => x.Id == param.LocationId)
                    .Select(x => x.Description)
                    .FirstOrDefault();
            }

            if (param.CustomerCategoryId.IsGuidNotEmpty())
            {
                query = query.Where(x => x.Customer.CustomerCategoryId == param.CustomerCategoryId);
                result.CustomerCategory = db.GetQuery<CustomerCategory>()
                    .Where(x => x.Id == param.CustomerCategoryId)
                    .Select(x => x.Code)
                    .FirstOrDefault();
            }

            if (param.CustomerId.IsGuidNotEmpty())
            {
                query = query.Where(x => x.CustomerId == param.CustomerId);
                result.Customer = db.GetQuery<Customer>()
                    .Where(x => x.Id == param.CustomerId)
                    .Select(x => x.Description)
                    .FirstOrDefault();
            }

            if (param.UserId.IsGuidNotEmpty())
            {
                query = query.Where(x => x.CreatedById == param.UserId);
                result.User = db.GetQuery<User>()
                    .Where(x => x.Id == param.UserId)
                    .Select(x => x.Description)
                    .FirstOrDefault();
            }

            if (param.Status.IsNotEmpty())
                query = query.Where(x => x.Status == param.Status);

            result.Data = query.OrderBy(x => x.DocumentNumber)
                .Project().To<SalesReportData>()
                .ToList();

            result.TotalBeforeTax = result.Data.Sum(x => x.TotalBeforeTax);
            result.TotalTax = result.Data.Sum(x => x.TotalTax);
            result.TotalAfterTax = result.Data.Sum(x => x.TotalAfterTax);
            result.TotalPayment = result.Data.Sum(x => x.TotalPayment);
            result.Balance = result.Data.Sum(x => x.Balance);

            return result;
        }
    }
}
