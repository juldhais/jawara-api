﻿using Jawara.Enums;
using Jawara.Helpers;
using Jawara.Models;
using Jawara.Repositories;
using Jawara.Resources;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Jawara.Services
{
    public class PurchaseOrderService
    {
        private readonly DataContext db;

        public PurchaseOrderService(DataContext db)
        {
            this.db = db;
        }

        public PurchaseOrderResource Get(Guid? id)
        {
            var order = db.Get<PurchaseOrder, PurchaseOrderResource>(id);
            if (order != null)
            {
                order.Details = db.GetQuery<PurchaseOrderDetail>()
                    .Where(x => x.PurchaseOrderId == id)
                    .OrderBy(x => x.Sequence)
                    .Project().To<PurchaseOrderDetailResource>()
                    .ToList();

                if (order.Details == null)
                    order.Details = new List<PurchaseOrderDetailResource>();

                order.CreatedBy = db.GetQuery<User>()
                    .Where(x => x.Id == order.CreatedById)
                    .Select(x => x.Description)
                    .FirstOrDefault();

                order.UpdatedBy = db.GetQuery<User>()
                    .Where(x => x.Id == order.UpdatedById)
                    .Select(x => x.Description)
                    .FirstOrDefault();
            }

            return order;
        }

        public PagingResult<PurchaseOrderResource> GetList(string keyword, string status, Guid? supplierId, Guid? locationId, DateTime? startDate, DateTime? endDate, int page, int size)
        {
            if (keyword.IsEmpty()
                && status.IsEmpty()
                && supplierId.IsGuidEmpty()
                && locationId.IsGuidEmpty()
                && startDate == null && endDate == null
                && page == 0 && size == 0)
                return new PagingResult<PurchaseOrderResource>();

            var query = db.GetQuery<PurchaseOrder>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.DocumentNumber.Contains(keyword)
                    || x.ReferenceNumber.Contains(keyword)
                    || x.Remarks.Contains(keyword));

            if (status.IsNotEmpty())
                query = query.Where(x => x.Status == status);

            if (supplierId.IsGuidNotEmpty())
                query = query.Where(x => x.SupplierId == supplierId);

            if (locationId.IsGuidNotEmpty())
                query = query.Where(x => x.LocationId == locationId);

            if (startDate != null)
                query = query.Where(x => x.DocumentDate.Date >= startDate);

            if (endDate != null)
                query = query.Where(x => x.DocumentDate.Date <= endDate);

            query = query.OrderByDescending(x => x.DocumentNumber);

            var result = query.PaginateTo<PurchaseOrder, PurchaseOrderResource>(page, size);

            return result;
        }

        public List<LookupResource> GetLookup(string keyword, string status, Guid? supplierId, Guid? locationId)
        {
            if (keyword.IsEmpty()
                && status.IsEmpty()
                && supplierId.IsGuidEmpty()
                && locationId.IsGuidEmpty())
                return new List<LookupResource>();

            var query = db.GetQuery<PurchaseOrder>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.DocumentNumber.Contains(keyword)
                    || x.ReferenceNumber.Contains(keyword)
                    || x.Remarks.Contains(keyword));

            if (status.IsNotEmpty())
                query = query.Where(x => x.Status == status);

            if (supplierId.IsGuidNotEmpty())
                query = query.Where(x => x.SupplierId == supplierId);

            if (locationId.IsGuidNotEmpty())
                query = query.Where(x => x.LocationId == locationId);

            var result = query.OrderByDescending(x => x.DocumentNumber)
                .Select(x => new LookupResource
                {
                    Id = x.Id,
                    Code = x.DocumentNumber + " ─ " + x.Supplier.Description
                }).ToList();

            return result;
        }

        public LookupResource GetSingleLookup(Guid? id)
        {
            var result = db.GetQuery<PurchaseOrder>()
                .Where(x => x.Id == id)
                .Select(x => new LookupResource
                {
                    Id = x.Id,
                    Code = x.DocumentNumber + " ─ " + x.Supplier.Description
                }).FirstOrDefault();

            return result;
        }

        public PagingResult<PurchaseReceiptResource> GetListReceipt(Guid? id, int page, int size)
        {
            if (id.IsGuidEmpty() && page == 0 && size == 0)
                return new PagingResult<PurchaseReceiptResource>();

            var query = db.GetQuery<PurchaseReceipt>()
                .Where(x => x.PurchaseOrderId == id)
                .OrderByDescending(x => x.DocumentNumber);

            var result = query.PaginateTo<PurchaseReceipt, PurchaseReceiptResource>(page, size);

            return result;
        }

        private void Calculate(PurchaseOrderResource res)
        {
            res.TotalBeforeTax = 0;
            res.TotalValueAddedTax = 0;
            res.TotalIncomeTax = 0;
            res.TotalOtherTax = 0;
            res.TotalAfterTax = 0;

            foreach (var detail in res.Details)
            {
                var subtotal = detail.Quantity * detail.Price;
                var subtotalAfterDiscount1 = subtotal * (100 - detail.DiscountPercent1) / 100;
                var subtotalAfterDiscount2 = subtotalAfterDiscount1 * (100 - detail.DiscountPercent2) / 100;
                var subtotalAfterDiscountAmount = subtotalAfterDiscount2 - detail.DiscountAmount;
                detail.SubtotalBeforeTax = subtotalAfterDiscountAmount;

                if (detail.ValueAddedTaxId.IsGuidNotEmpty())
                {
                    var vat = db.Get<Tax>(detail.ValueAddedTaxId);
                    detail.ValueAddedTaxAmount = detail.SubtotalBeforeTax * vat.Rate / 100 + vat.Amount;
                }
                if (detail.IncomeTaxId.IsGuidNotEmpty())
                {
                    var ict = db.Get<Tax>(detail.IncomeTaxId);
                    detail.IncomeTaxAmount = detail.SubtotalBeforeTax * ict.Rate / 100 + ict.Amount;
                }
                if (detail.OtherTaxId.IsGuidNotEmpty())
                {
                    var oth = db.Get<Tax>(detail.OtherTaxId);
                    detail.OtherTaxAmount = detail.SubtotalBeforeTax * oth.Rate / 100 + oth.Amount;
                }

                detail.SubtotalAfterTax = detail.SubtotalBeforeTax + detail.ValueAddedTaxAmount + detail.IncomeTaxAmount + detail.OtherTaxAmount;

                res.TotalBeforeTax += detail.SubtotalBeforeTax;
                res.TotalValueAddedTax += detail.ValueAddedTaxAmount;
                res.TotalIncomeTax += detail.IncomeTaxAmount;
                res.TotalOtherTax += detail.OtherTaxAmount;
                res.TotalAfterTax += detail.SubtotalAfterTax;
            }
        }

        private void ValidateResource(PurchaseOrderResource res)
        {
            if (res.DocumentDate == default)
                throw new BadRequestException("Document date cannot be empty.");

            if (res.SupplierId.IsGuidEmpty())
                throw new BadRequestException("Supplier cannot be empty.");

            if (res.LocationId.IsGuidEmpty())
                throw new BadRequestException("Location cannot be empty.");
        }

        public string GetNewDocumentNumber(DateTime date)
        {
            var prefix = $"PO-{date:yyMM}";

            var lastNumber = db.GetQuery<PurchaseOrder>()
                .Where(x => x.DocumentNumber.StartsWith(prefix))
                .OrderByDescending(x => x.DocumentNumber)
                .Select(x => x.DocumentNumber)
                .FirstOrDefault();

            if (lastNumber == null) return prefix + "0001";

            var newNumber = lastNumber.Substring(prefix.Length, 4).ToInteger() + 1;
            return prefix + newNumber.ToString("0000");
        }

        public Guid Create(PurchaseOrderResource res, Guid? userId)
        {
            ValidateResource(res);

            Calculate(res);

            var order = new PurchaseOrder();
            order.MapFrom(res);
            order.DocumentNumber = GetNewDocumentNumber(res.DocumentDate);
            order.Status = PurchaseOrderStatus.Open;
            db.Create(order, userId);

            var sequence = 0;
            foreach (var detail in res.Details)
            {
                if (detail.ItemId.IsGuidEmpty()) continue;
                if (detail.Quantity == 0) continue;

                var orderDetail = new PurchaseOrderDetail();
                orderDetail.MapFrom(detail);
                orderDetail.PurchaseOrderId = order.Id;
                orderDetail.Sequence = ++sequence;
                db.Create(orderDetail, userId);
            }

            db.SaveChanges();

            return order.Id;
        }

        public Guid Update(PurchaseOrderResource res, Guid? userId)
        {
            ValidateResource(res);

            var transaction = db.Database.BeginTransaction();

            try
            {
                db.Database.ExecuteSqlRaw("DELETE FROM PurchaseOrderDetail WHERE PurchaseOrderId = {0} AND IsDeleted = 0", res.Id);

                //update
                Calculate(res);

                var order = db.Get<PurchaseOrder>(res.Id);

                if (order == null)
                    throw new BadRequestException("Purchase Order not found.");

                order.MapFrom(res);
                db.Update(order, userId);

                var sequence = 0;
                foreach (var detail in res.Details)
                {
                    if (detail.ItemId.IsGuidEmpty()) continue;
                    if (detail.Quantity == 0) continue;

                    var orderDetail = new PurchaseOrderDetail();
                    orderDetail.MapFrom(detail);
                    orderDetail.PurchaseOrderId = order.Id;
                    orderDetail.Sequence = ++sequence;
                    db.Create(orderDetail, userId);
                }

                db.SaveChanges();
                transaction.Commit();

                return res.Id;
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
        }

        public void Delete(Guid? id, Guid? userId)
        {
            var order = db.Get<PurchaseOrder>(id);

            if (order.Status == PurchaseOrderStatus.Closed)
                throw new BadRequestException($"Unable to delete Purchase Order because it is already {order.Status}.");

            db.Delete<PurchaseOrderDetail>(x => x.PurchaseOrderId == id, userId);
            db.Delete<PurchaseOrder>(id, userId);
            db.SaveChanges();
        }

        public void Open(Guid? id, Guid? userId)
        {
            var order = db.Get<PurchaseOrder>(id);
            order.Status = PurchaseOrderStatus.Open;
            db.Update(order, userId);

            db.SaveChanges();
        }

        public void Close(Guid? id, Guid? userId)
        {
            var order = db.Get<PurchaseOrder>(id);
            order.Status = PurchaseOrderStatus.Closed;
            db.Update(order, userId);

            db.SaveChanges();
        }

        public void CalculateQuantityReceived(Guid? id)
        {
            if (id.IsGuidEmpty()) return;

            var order = db.Get<PurchaseOrder>(id);

            var orderDetailIds = db.GetQuery<PurchaseOrderDetail>()
                .Where(x => x.PurchaseOrderId == id)
                .Select(x => x.Id)
                .ToList();

            var isCompleted = true;
            foreach (var detailId in orderDetailIds)
            {
                var orderDetail = db.Get<PurchaseOrderDetail>(detailId);

                var qtyReceived = db.GetQuery<PurchaseReceiptDetail>()
                    .Where(x => x.PurchaseReceipt.PurchaseOrderId == id
                            && x.ItemId == orderDetail.ItemId)
                    .Sum(x => x.Quantity * x.Ratio);

                orderDetail.QuantityReceived = qtyReceived / orderDetail.Ratio;

                if (orderDetail.Quantity > orderDetail.QuantityReceived)
                    isCompleted = false;
            }

            if (isCompleted)
                order.Status = PurchaseOrderStatus.Closed;

            db.SaveChanges();
        }
    }
}
