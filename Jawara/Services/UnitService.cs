﻿using Jawara.Helpers;
using Jawara.Models;
using Jawara.Repositories;
using Jawara.Resources;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Jawara.Services
{
    public class UnitService
    {
        private readonly DataContext db;

        public UnitService(DataContext db)
        {
            this.db = db;
        }

        public UnitResource Get(Guid? id)
        {
            return db.Get<Unit, UnitResource>(id);
        }

        public PagingResult<UnitResource> GetList(string keyword, int page, int size)
        {
            var query = db.GetQuery<Unit>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.Code.Contains(keyword));

            var result = query.OrderBy(x => x.Code)
                .PaginateTo<Unit, UnitResource>(page, size);

            return result;
        }

        public List<LookupResource> GetLookup(string keyword)
        {
            var query = db.GetQuery<Unit>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.Code.Contains(keyword));

            var result = query.OrderBy(x => x.Sequence)
                .Select(x => new LookupResource
                {
                    Id = x.Id,
                    Code = x.Code
                }).ToList();

            return result;
        }

        public LookupResource GetSingleLookup(Guid? id)
        {
            var result = db.GetQuery<Unit>()
                .Where(x => x.Id == id)
                .Select(x => new LookupResource
                {
                    Id = x.Id,
                    Code = x.Code
                }).FirstOrDefault();

            return result;
        }

        public void ValidateResource(UnitResource res)
        {
            if (res.Code.IsEmpty())
                throw new BadRequestException("Code cannot be empty.");

            if (db.GetQuery<Unit>().Any(x => x.Code == res.Code && x.Id != res.Id))
                throw new BadRequestException("Code is already used.");
        }

        public Guid Create(UnitResource res, Guid? userId)
        {
            ValidateResource(res);

            var unit = new Unit();
            unit.MapFrom(res);
            db.Create(unit, userId);
            db.SaveChanges();

            return unit.Id;
        }

        public Guid Update(UnitResource res, Guid? userId)
        {
            ValidateResource(res);

            var unit = db.Get<Unit>(res.Id);
            unit.MapFrom(res);
            db.Update(unit, userId);
            db.SaveChanges();

            return unit.Id;
        }

        public void Delete(Guid? id, Guid? userId)
        {
            db.Delete<Unit>(id, userId);
            db.SaveChanges();
        }
    }
}
