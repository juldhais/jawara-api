﻿using Jawara.Enums;
using Jawara.Helpers;
using Jawara.Repositories;
using Jawara.Resources;
using System;
using System.Linq;
using System.Text;

namespace Jawara.Services
{
    public class SalesDeliveryPrintService
    {
        private readonly DataContext db;
        private readonly ConfigurationService configService;
        private readonly SalesDeliveryService salesDeliveryService;

        private ConfigurationEditResource configEdit;

        public SalesDeliveryPrintService(DataContext db,
            ConfigurationService configService,
            SalesDeliveryService salesDeliveryService)
        {
            this.db = db;
            this.configService = configService;
            this.salesDeliveryService = salesDeliveryService;
            this.configEdit = configService.GetEdit();
        }

        public PrintResource Print(Guid? id)
        {
            var result = new PrintResource();
            result.Type = configEdit.PrintType;
            result.Module = "Sales Delivery";

            var res = salesDeliveryService.Get(id);

            if (configEdit.PrintType == PrintType.Receipt30)
                result.Data = PrintReceipt30(res);
            else if (configEdit.PrintType == PrintType.Receipt40)
                result.Data = PrintReceipt40(res);
            else if (configEdit.PrintType == PrintType.Receipt50)
                result.Data = PrintReceipt50(res);
            else result.Data = null;

            return result;
        }

        private object PrintReceipt30(SalesDeliveryResource res)
        {
            var builder = new StringBuilder();

            builder.AppendLine(configEdit.PrintHeader);
            builder.AppendLine(configEdit.PrintSubheader);

            builder.AppendLine($"Sales No : {res.DocumentNumber}");
            builder.AppendLine($"Date     : {res.DocumentDate:dd/MM/yyyy - HH:mm}");
            builder.AppendLine($"Customer : {res.CustomerCode}");
            builder.AppendLine("-".Repeat(28));

            foreach (var detail in res.Details)
            {
                builder.AppendLine($"{detail.ItemDescription.Left(28)}");
                builder.AppendLine($"{detail.Quantity.NumRight(5)} {detail.Price.NumRight(10)} {detail.SubtotalAfterTax.NumRight(11)}");
            }

            builder.AppendLine("-".Repeat(28));

            builder.AppendLine($"{"TOTAL".Right(13)} : {res.TotalAfterTax.NumRight(12)}");

            foreach (var item in res.Payments)
                builder.AppendLine($"{item.PaymentMethodCode.Right(13)} : {res.TotalAfterTax.NumRight(12)}");

            builder.AppendLine(" ");

            builder.AppendLine($"ITEMS: {res.Details.Count.Format()}   QTY: {res.Details.Sum(x => x.Quantity).Format()}");

            builder.AppendLine(" ");

            builder.AppendLine(configEdit.PrintFooter);

            for (int i = 0; i < 10; i++)
                builder.AppendLine(" ");

            return builder.ToString();
        }

        private object PrintReceipt40(SalesDeliveryResource res)
        {
            var builder = new StringBuilder();

            builder.AppendLine(configEdit.PrintHeader);
            builder.AppendLine(configEdit.PrintSubheader);

            builder.AppendLine($"Sales No : {res.DocumentNumber}");
            builder.AppendLine($"Date     : {res.DocumentDate:dd/MM/yyyy - HH:mm}");
            builder.AppendLine($"Customer : {res.CustomerCode}");
            builder.AppendLine("-".Repeat(38));

            foreach (var detail in res.Details)
            {
                builder.AppendLine($"{detail.ItemDescription.Left(38)}");
                builder.AppendLine($"{detail.Quantity.NumRight(6)} {detail.Unit.Left(3)} {detail.Price.NumRight(12)} {detail.SubtotalAfterTax.NumRight(14)}");
            }

            builder.AppendLine("-".Repeat(38));

            builder.AppendLine($"{"TOTAL".Right(19)} : {res.TotalAfterTax.NumRight(16)}");

            foreach (var item in res.Payments)
                builder.AppendLine($"{item.PaymentMethodCode.Right(19)} : {res.TotalAfterTax.NumRight(16)}");

            builder.AppendLine(" ");

            builder.AppendLine($"ITEMS: {res.Details.Count.Format()}   QTY: {res.Details.Sum(x => x.Quantity).Format()}");

            builder.AppendLine(" ");

            builder.AppendLine(configEdit.PrintFooter);

            for (int i = 0; i < 10; i++)
                builder.AppendLine(" ");

            return builder.ToString();
        }

        private object PrintReceipt50(SalesDeliveryResource res)
        {
            var builder = new StringBuilder();

            builder.AppendLine(configEdit.PrintHeader);
            builder.AppendLine(configEdit.PrintSubheader);

            builder.AppendLine($"Sales No : {res.DocumentNumber}");
            builder.AppendLine($"Date     : {res.DocumentDate:dd/MM/yyyy - HH:mm}");
            builder.AppendLine($"Customer : {res.CustomerCode}");
            builder.AppendLine("-".Repeat(48));

            foreach (var detail in res.Details)
            {
                builder.AppendLine($"{detail.ItemDescription.Left(28)} {detail.Quantity.NumRight(4)} {detail.SubtotalAfterTax.NumRight(14)}");
            }

            builder.AppendLine("-".Repeat(48));

            builder.AppendLine($"{"TOTAL".Right(29)} : {res.TotalAfterTax.NumRight(16)}");

            foreach (var item in res.Payments)
                builder.AppendLine($"{item.PaymentMethodCode.Right(29)} : {res.TotalAfterTax.NumRight(16)}");

            builder.AppendLine(" ");

            builder.AppendLine($"ITEMS: {res.Details.Count.Format()}   QTY: {res.Details.Sum(x => x.Quantity).Format()}");

            builder.AppendLine(" ");

            builder.AppendLine(configEdit.PrintFooter);

            for (int i = 0; i < 10; i++)
                builder.AppendLine(" ");

            return builder.ToString();
        }
    }
}
