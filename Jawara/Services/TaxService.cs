﻿using Jawara.Helpers;
using Jawara.Models;
using Jawara.Repositories;
using Jawara.Resources;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Jawara.Services
{
    public class TaxService
    {
        private readonly DataContext db;

        public TaxService(DataContext db)
        {
            this.db = db;
        }

        public TaxResource Get(Guid? id)
        {
            return db.Get<Tax, TaxResource>(id);
        }

        public PagingResult<TaxResource> GetList(string keyword, string type, int page, int size)
        {
            var query = db.GetQuery<Tax>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.Description.Contains(keyword));

            if (type.IsNotEmpty())
                query = query.Where(x => x.Type == type);

            var result = query.OrderBy(x => x.Description)
                .PaginateTo<Tax, TaxResource>(page, size);

            return result;
        }

        public List<LookupResource> GetLookup(string keyword, string type)
        {
            var query = db.GetQuery<Tax>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.Description.Contains(keyword));

            if (type.IsNotEmpty())
                query = query.Where(x => x.Type == type);

            var result = query.OrderBy(x => x.Code)
                .Select(x => new LookupResource
                {
                    Id = x.Id,
                    Code = x.Code + " ─ " + x.Description
                }).ToList();

            return result;
        }

        public LookupResource GetSingleLookup(Guid? id)
        {
            var result = db.GetQuery<Tax>()
                .Where(x => x.Id == id)
                .Select(x => new LookupResource
                {
                    Id = x.Id,
                    Code = x.Code + " ─ " + x.Description
                }).FirstOrDefault();

            return result;
        }

        public void ValidateResource(TaxResource res)
        {
            if (res.Code.IsEmpty())
                throw new BadRequestException("Code cannot be empty.");

            if (db.GetQuery<Tax>().Any(x => x.Code == res.Code && x.Id != res.Id))
                throw new BadRequestException("Code is already used.");
        }

        public Guid Create(TaxResource res, Guid? userId)
        {
            ValidateResource(res);

            var unit = new Tax();
            unit.MapFrom(res);
            db.Create(unit, userId);
            db.SaveChanges();

            return unit.Id;
        }

        public Guid Update(TaxResource res, Guid? userId)
        {
            ValidateResource(res);

            var unit = db.Get<Tax>(res.Id);
            unit.MapFrom(res);
            db.Update(unit, userId);
            db.SaveChanges();

            return unit.Id;
        }

        public void Delete(Guid? id, Guid? userId)
        {
            db.Delete<Tax>(id, userId);
            db.SaveChanges();
        }
    }
}