﻿using Jawara.Enums;
using Jawara.Helpers;
using Jawara.Models;
using Jawara.Repositories;
using Jawara.Resources;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Jawara.Services
{
    public class StockOrderService
    {
        private readonly DataContext db;

        public StockOrderService(DataContext db)
        {
            this.db = db;
        }

        public StockOrderResource Get(Guid? id)
        {
            var order = db.Get<StockOrder, StockOrderResource>(id);
            if (order != null)
            {
                order.Details = db.StockOrderDetail
                    .Where(x => x.IsDeleted == false
                                && x.StockOrderId == id)
                    .OrderBy(x => x.Sequence)
                    .Project().To<StockOrderDetailResource>()
                    .ToList();

                if (order.Details == null)
                    order.Details = new List<StockOrderDetailResource>();

                order.CreatedBy = db.GetQuery<User>()
                    .Where(x => x.Id == order.CreatedById)
                    .Select(x => x.Description)
                    .FirstOrDefault();

                order.UpdatedBy = db.GetQuery<User>()
                    .Where(x => x.Id == order.UpdatedById)
                    .Select(x => x.Description)
                    .FirstOrDefault();
            }

            return order;
        }

        public PagingResult<StockOrderResource> GetList(string keyword, string status, Guid? destinationId, Guid? sourceId, DateTime? startDate, DateTime? endDate, int page, int size)
        {
            if (keyword.IsEmpty()
                && status.IsEmpty()
                && destinationId.IsGuidEmpty()
                && sourceId.IsGuidEmpty()
                && startDate == null && endDate == null
                && page == 0 && size == 0)
                return new PagingResult<StockOrderResource>();

            var query = db.GetQuery<StockOrder>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.DocumentNumber.Contains(keyword)
                    || x.Remarks.Contains(keyword));

            if (status.IsNotEmpty())
                query = query.Where(x => x.Status == status);

            if (destinationId.IsGuidNotEmpty())
                query = query.Where(x => x.DestinationLocationId == destinationId);

            if (sourceId.IsGuidNotEmpty())
                query = query.Where(x => x.SourceLocationId == sourceId);

            if (startDate != null)
                query = query.Where(x => x.DocumentDate.Date >= startDate);

            if (endDate != null)
                query = query.Where(x => x.DocumentDate.Date <= endDate);

            query = query.OrderByDescending(x => x.DocumentNumber);

            var result = query.PaginateTo<StockOrder, StockOrderResource>(page, size);

            return result;
        }

        public List<LookupResource> GetLookup(string keyword, string status, Guid? sourceId, Guid? destinationId)
        {
            if (keyword.IsEmpty()
                && status.IsEmpty()
                && sourceId.IsGuidEmpty()
                && destinationId.IsGuidEmpty())
                return new List<LookupResource>();

            var query = db.GetQuery<StockOrder>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.DocumentNumber.Contains(keyword)
                    || x.Remarks.Contains(keyword));

            if (status.IsNotEmpty())
                query = query.Where(x => x.Status == status);

            if (sourceId.IsGuidNotEmpty())
                query = query.Where(x => x.SourceLocationId == sourceId);

            if (destinationId.IsGuidNotEmpty())
                query = query.Where(x => x.DestinationLocationId == sourceId);

            var result = query.OrderByDescending(x => x.DocumentNumber)
                .Select(x => new LookupResource
                {
                    Id = x.Id,
                    Code = x.DocumentNumber + " ─ " + x.DestinationLocation.Description
                }).ToList();

            return result;
        }

        public LookupResource GetSingleLookup(Guid? id)
        {
            var result = db.GetQuery<StockOrder>()
                .Where(x => x.Id == id)
                .Select(x => new LookupResource
                {
                    Id = x.Id,
                    Code = x.DocumentNumber + " ─ " + x.DestinationLocation.Description
                }).FirstOrDefault();

            return result;
        }

        public PagingResult<StockDeliveryResource> GetListDelivery(Guid? id, int page, int size)
        {
            if (id.IsGuidEmpty() && page == 0 && size == 0)
                return new PagingResult<StockDeliveryResource>();

            var query = db.GetQuery<StockDelivery>()
                .Where(x => x.StockOrderId == id)
                .OrderByDescending(x => x.DocumentNumber);

            var result = query.PaginateTo<StockDelivery, StockDeliveryResource>(page, size);

            return result;
        }

        public PagingResult<StockReceiptResource> GetListReceipt(Guid? id, int page, int size)
        {
            if (id.IsGuidEmpty() && page == 0 && size == 0)
                return new PagingResult<StockReceiptResource>();

            var query = db.GetQuery<StockReceipt>()
                .Where(x => x.StockDelivery.StockOrderId == id)
                .OrderByDescending(x => x.DocumentNumber);

            var result = query.PaginateTo<StockReceipt, StockReceiptResource>(page, size);

            return result;
        }

        private void ValidateResource(StockOrderResource res)
        {
            if (res.DocumentDate == default)
                throw new BadRequestException("Document date cannot be empty.");

            if (res.DestinationLocationId.IsGuidEmpty())
                throw new BadRequestException("Destination Location cannot be empty.");

            if (res.SourceLocationId.IsGuidEmpty())
                throw new BadRequestException("Source Location cannot be empty.");
        }

        public string GetNewDocumentNumber(DateTime date)
        {
            var prefix = $"IO-{date:yyMM}";

            var lastNumber = db.GetQuery<StockOrder>()
                .Where(x => x.DocumentNumber.StartsWith(prefix))
                .OrderByDescending(x => x.DocumentNumber)
                .Select(x => x.DocumentNumber)
                .FirstOrDefault();

            if (lastNumber == null) return prefix + "0001";

            var newNumber = lastNumber.Substring(prefix.Length, 4).ToInteger() + 1;
            return prefix + newNumber.ToString("0000");
        }

        public Guid Create(StockOrderResource res, Guid? userId)
        {
            ValidateResource(res);

            var order = new StockOrder();
            order.MapFrom(res);
            order.DocumentNumber = GetNewDocumentNumber(res.DocumentDate);
            order.Status = StockOrderStatus.Open;
            db.Create(order, userId);

            var sequence = 0;
            foreach (var detail in res.Details)
            {
                if (detail.ItemId.IsGuidEmpty()) continue;
                if (detail.Quantity == 0) continue;

                var orderDetail = new StockOrderDetail();
                orderDetail.MapFrom(detail);
                orderDetail.StockOrderId = order.Id;
                orderDetail.Sequence = ++sequence;
                db.Create(orderDetail, userId);
            }

            db.SaveChanges();

            return order.Id;
        }

        public Guid Update(StockOrderResource res, Guid? userId)
        {
            ValidateResource(res);

            var transaction = db.Database.BeginTransaction();

            try
            {
                db.Database.ExecuteSqlRaw("DELETE FROM StockOrderDetail WHERE StockOrderId = {0} AND IsDeleted = 0", res.Id);

                var order = db.Get<StockOrder>(res.Id);

                if (order == null)
                    throw new BadRequestException("Stock Order not found.");

                order.MapFrom(res);
                db.Update(order, userId);

                var sequence = 0;
                foreach (var detail in res.Details)
                {
                    if (detail.ItemId.IsGuidEmpty()) continue;
                    if (detail.Quantity == 0) continue;

                    var orderDetail = new StockOrderDetail();
                    orderDetail.MapFrom(detail);
                    orderDetail.StockOrderId = order.Id;
                    orderDetail.Sequence = ++sequence;
                    db.Create(orderDetail, userId);
                }

                db.SaveChanges();
                transaction.Commit();

                return res.Id;
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
        }

        public void Delete(Guid? id, Guid? userId)
        {
            var order = db.Get<StockOrder>(id);

            if (order.Status == StockOrderStatus.Closed)
                throw new BadRequestException($"Unable to delete Stock Order because it is already {order.Status}.");

            db.Delete<StockOrderDetail>(x => x.StockOrderId == id, userId);
            db.Delete<StockOrder>(id, userId);
            db.SaveChanges();
        }

        public void Open(Guid? id, Guid? userId)
        {
            var order = db.Get<StockOrder>(id);
            order.Status = StockOrderStatus.Open;
            db.Update(order, userId);

            db.SaveChanges();
        }

        public void Close(Guid? id, Guid? userId)
        {
            var order = db.Get<StockOrder>(id);
            order.Status = StockOrderStatus.Closed;
            db.Update(order, userId);

            db.SaveChanges();
        }

        public void CalculateQuantityDelivered(Guid? id)
        {
            var order = db.Get<StockOrder>(id);

            var orderDetailIds = db.GetQuery<StockOrderDetail>()
                .Where(x => x.StockOrderId == id)
                .Select(x => x.Id)
                .ToList();

            var isCompleted = true;
            foreach (var detailId in orderDetailIds)
            {
                var orderDetail = db.Get<StockOrderDetail>(detailId);

                var qtyDelivered = db.GetQuery<StockDeliveryDetail>()
                    .Where(x => x.StockDelivery.StockOrderId == id
                        && x.ItemId == orderDetail.ItemId)
                    .Sum(x => x.Quantity * x.Ratio);

                orderDetail.QuantityDelivered = qtyDelivered / orderDetail.Ratio;

                if (orderDetail.Quantity > orderDetail.QuantityDelivered)
                    isCompleted = false;
            }

            if (isCompleted)
                order.Status = StockOrderStatus.Closed;

            db.SaveChanges();
        }

        public void CalculateQuantityReceived(Guid? id)
        {
            var order = db.Get<StockOrder>(id);

            var orderDetailIds = db.GetQuery<StockOrderDetail>()
                .Where(x => x.StockOrderId == id)
                .Select(x => x.Id)
                .ToList();

            foreach (var detailId in orderDetailIds)
            {
                var orderDetail = db.Get<StockOrderDetail>(detailId);

                var qtyReceived = db.GetQuery<StockReceiptDetail>()
                    .Where(x => x.StockReceipt.StockDelivery.StockOrderId == id
                        && x.ItemId == orderDetail.ItemId)
                    .Sum(x => x.Quantity * x.Ratio);

                orderDetail.QuantityReceived = qtyReceived / orderDetail.Ratio;
            }

            db.SaveChanges();
        }
    }
}
