﻿using Jawara.Helpers;
using Jawara.Models;
using Jawara.Repositories;
using Jawara.Resources;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Jawara.Services
{
    public class CustomerCategoryService
    {
        private readonly DataContext db;

        public CustomerCategoryService(DataContext db)
        {
            this.db = db;
        }

        public CustomerCategoryResource Get(Guid? id)
        {
            return db.Get<CustomerCategory, CustomerCategoryResource>(id);
        }

        public PagingResult<CustomerCategoryResource> GetList(string keyword, int page, int size)
        {
            var query = db.GetQuery<CustomerCategory>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.Code.Contains(keyword));

            var result = query.OrderBy(x => x.Code)
                .PaginateTo<CustomerCategory, CustomerCategoryResource>(page, size);

            return result;
        }

        public List<LookupResource> GetLookup(string keyword)
        {
            if (keyword.IsEmpty() || keyword.Length < 2)
                return new List<LookupResource>();

            var query = db.GetQuery<CustomerCategory>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.Code.Contains(keyword));

            var result = query.OrderBy(x => x.Code)
                .Select(x => new LookupResource
                {
                    Id = x.Id,
                    Code = x.Code
                }).ToList();

            return result;
        }

        public LookupResource GetSingleLookup(Guid? id)
        {
            var result = db.GetQuery<CustomerCategory>()
                .Where(x => x.Id == id)
                .Select(x => new LookupResource
                {
                    Id = x.Id,
                    Code = x.Code
                }).FirstOrDefault();

            return result;
        }

        public void ValidateResource(CustomerCategoryResource res)
        {
            if (res.Code.IsEmpty())
                throw new BadRequestException("Code cannot be empty.");

            if (db.GetQuery<CustomerCategory>().Any(x => x.Code == res.Code && x.Id != res.Id))
                throw new BadRequestException("Code is already used.");
        }

        public Guid Create(CustomerCategoryResource res, Guid? userId)
        {
            ValidateResource(res);

            var category = new CustomerCategory();
            category.MapFrom(res);
            db.Create(category, userId);
            db.SaveChanges();

            return category.Id;
        }

        public Guid Update(CustomerCategoryResource res, Guid? userId)
        {
            ValidateResource(res);

            var category = db.Get<CustomerCategory>(res.Id);
            category.MapFrom(res);
            db.Update(category, userId);
            db.SaveChanges();

            return category.Id;
        }

        public void Delete(Guid? id, Guid? userId)
        {
            db.Delete<CustomerCategory>(id, userId);
            db.SaveChanges();
        }
    }
}
