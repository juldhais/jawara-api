﻿using Jawara.Enums;
using Jawara.Helpers;
using Jawara.Models;
using Jawara.Repositories;
using Jawara.Resources;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Jawara.Services
{
    public class SalesOrderService
    {
        private readonly DataContext db;

        public SalesOrderService(DataContext db)
        {
            this.db = db;
        }

        public SalesOrderResource Get(Guid? id)
        {
            var order = db.Get<SalesOrder, SalesOrderResource>(id);
            if (order != null)
            {
                order.Details = db.SalesOrderDetail
                    .Where(x => x.IsDeleted == false
                                && x.SalesOrderId == id)
                    .OrderBy(x => x.Sequence)
                    .Project().To<SalesOrderDetailResource>()
                    .ToList();

                if (order.Details == null)
                    order.Details = new List<SalesOrderDetailResource>();

                order.CreatedBy = db.GetQuery<User>()
                    .Where(x => x.Id == order.CreatedById)
                    .Select(x => x.Description)
                    .FirstOrDefault();

                order.UpdatedBy = db.GetQuery<User>()
                    .Where(x => x.Id == order.UpdatedById)
                    .Select(x => x.Description)
                    .FirstOrDefault();
            }

            return order;
        }

        public PagingResult<SalesOrderResource> GetList(string keyword, string status, Guid? customerId, Guid? locationId, DateTime? startDate, DateTime? endDate, int page, int size)
        {
            if (keyword.IsEmpty()
                && status.IsEmpty()
                && customerId.IsGuidEmpty()
                && locationId.IsGuidEmpty()
                && startDate == null && endDate == null
                && page == 0 && size == 0)
                return new PagingResult<SalesOrderResource>();

            var query = db.GetQuery<SalesOrder>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.DocumentNumber.Contains(keyword)
                    || x.ReferenceNumber.Contains(keyword)
                    || x.Remarks.Contains(keyword));

            if (status.IsNotEmpty())
                query = query.Where(x => x.Status == status);

            if (customerId.IsGuidNotEmpty())
                query = query.Where(x => x.CustomerId == customerId);

            if (locationId.IsGuidNotEmpty())
                query = query.Where(x => x.LocationId == locationId);

            if (startDate != null)
                query = query.Where(x => x.DocumentDate.Date >= startDate);

            if (endDate != null)
                query = query.Where(x => x.DocumentDate.Date <= endDate);

            query = query.OrderByDescending(x => x.DocumentNumber);

            var result = query.PaginateTo<SalesOrder, SalesOrderResource>(page, size);

            return result;
        }

        public List<LookupResource> GetLookup(string keyword, string status, Guid? customerId, Guid? locationId)
        {
            if (keyword.IsEmpty()
                && status.IsEmpty()
                && customerId.IsGuidEmpty()
                && locationId.IsGuidEmpty())
                return new List<LookupResource>();

            var query = db.GetQuery<SalesOrder>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.DocumentNumber.Contains(keyword)
                    || x.ReferenceNumber.Contains(keyword)
                    || x.Remarks.Contains(keyword));

            if (status.IsNotEmpty())
                query = query.Where(x => x.Status == status);

            if (customerId.IsGuidNotEmpty())
                query = query.Where(x => x.CustomerId == customerId);

            if (locationId.IsGuidNotEmpty())
                query = query.Where(x => x.LocationId == locationId);

            var result = query.OrderByDescending(x => x.DocumentNumber)
                .Select(x => new LookupResource
                {
                    Id = x.Id,
                    Code = x.DocumentNumber + " ─ " + x.Customer.Description
                }).ToList();

            return result;
        }

        public LookupResource GetSingleLookup(Guid? id)
        {
            var result = db.GetQuery<SalesOrder>()
                .Where(x => x.Id == id)
                .Select(x => new LookupResource
                {
                    Id = x.Id,
                    Code = x.DocumentNumber + " ─ " + x.Customer.Description
                }).FirstOrDefault();

            return result;
        }

        public PagingResult<SalesDeliveryResource> GetListDelivery(Guid? id, int page, int size)
        {
            if (id.IsGuidEmpty() && page == 0 && size == 0)
                return new PagingResult<SalesDeliveryResource>();

            var query = db.GetQuery<SalesDelivery>()
                .Where(x => x.SalesOrderId == id)
                .OrderByDescending(x => x.DocumentNumber);

            var result = query.PaginateTo<SalesDelivery, SalesDeliveryResource>(page, size);

            return result;
        }
        private void Calculate(SalesOrderResource res)
        {
            res.TotalBeforeTax = 0;
            res.TotalValueAddedTax = 0;
            res.TotalIncomeTax = 0;
            res.TotalOtherTax = 0;
            res.TotalAfterTax = 0;

            foreach (var detail in res.Details)
            {
                var subtotal = detail.Quantity * detail.Price;
                var subtotalAfterDiscount1 = subtotal * (100 - detail.DiscountPercent1) / 100;
                var subtotalAfterDiscount2 = subtotalAfterDiscount1 * (100 - detail.DiscountPercent2) / 100;
                var subtotalAfterDiscountAmount = subtotalAfterDiscount2 - detail.DiscountAmount;
                detail.SubtotalBeforeTax = subtotalAfterDiscountAmount;

                if (detail.ValueAddedTaxId.IsGuidNotEmpty())
                {
                    var vat = db.Get<Tax>(detail.ValueAddedTaxId);
                    detail.ValueAddedTaxAmount = detail.SubtotalBeforeTax * vat.Rate / 100 + vat.Amount;
                }
                if (detail.IncomeTaxId.IsGuidNotEmpty())
                {
                    var ict = db.Get<Tax>(detail.IncomeTaxId);
                    detail.IncomeTaxAmount = detail.SubtotalBeforeTax * ict.Rate / 100 + ict.Amount;
                }
                if (detail.OtherTaxId.IsGuidNotEmpty())
                {
                    var oth = db.Get<Tax>(detail.OtherTaxId);
                    detail.OtherTaxAmount = detail.SubtotalBeforeTax * oth.Rate / 100 + oth.Amount;
                }

                detail.SubtotalAfterTax = detail.SubtotalBeforeTax + detail.ValueAddedTaxAmount + detail.IncomeTaxAmount + detail.OtherTaxAmount;

                res.TotalBeforeTax += detail.SubtotalBeforeTax;
                res.TotalValueAddedTax += detail.ValueAddedTaxAmount;
                res.TotalIncomeTax += detail.IncomeTaxAmount;
                res.TotalOtherTax += detail.OtherTaxAmount;
                res.TotalAfterTax += detail.SubtotalAfterTax;
            }
        }

        private void ValidateResource(SalesOrderResource res)
        {
            if (res.DocumentDate == default)
                throw new BadRequestException("Document date cannot be empty.");

            if (res.CustomerId.IsGuidEmpty())
                throw new BadRequestException("Customer cannot be empty.");

            if (res.LocationId.IsGuidEmpty())
                throw new BadRequestException("Location cannot be empty.");
        }

        public string GetNewDocumentNumber(DateTime date)
        {
            var prefix = $"SO-{date:yyMM}";

            var lastNumber = db.GetQuery<SalesOrder>()
                .Where(x => x.DocumentNumber.StartsWith(prefix))
                .OrderByDescending(x => x.DocumentNumber)
                .Select(x => x.DocumentNumber)
                .FirstOrDefault();

            if (lastNumber == null) return prefix + "0001";

            var newNumber = lastNumber.Substring(prefix.Length, 4).ToInteger() + 1;
            return prefix + newNumber.ToString("0000");
        }

        public Guid Create(SalesOrderResource res, Guid? userId)
        {
            ValidateResource(res);

            Calculate(res);

            var order = new SalesOrder();
            order.MapFrom(res);
            order.DocumentNumber = GetNewDocumentNumber(res.DocumentDate);
            order.Status = SalesOrderStatus.Open;
            db.Create(order, userId);

            var sequence = 0;
            foreach (var detail in res.Details)
            {
                if (detail.ItemId.IsGuidEmpty()) continue;
                if (detail.Quantity == 0) continue;

                var orderDetail = new SalesOrderDetail();
                orderDetail.MapFrom(detail);
                orderDetail.SalesOrderId = order.Id;
                orderDetail.Sequence = ++sequence;
                db.Create(orderDetail, userId);
            }

            db.SaveChanges();

            return order.Id;
        }

        public Guid Update(SalesOrderResource res, Guid? userId)
        {
            ValidateResource(res);

            var transaction = db.Database.BeginTransaction();

            try
            {
                db.Database.ExecuteSqlRaw("DELETE FROM SalesOrderDetail WHERE SalesOrderId = {0} AND IsDeleted = 0", res.Id);

                //update
                Calculate(res);

                var order = db.Get<SalesOrder>(res.Id);

                if (order == null)
                    throw new BadRequestException("Sales Order not found.");

                order.MapFrom(res);
                db.Update(order, userId);

                var sequence = 0;
                foreach (var detail in res.Details)
                {
                    if (detail.ItemId.IsGuidEmpty()) continue;
                    if (detail.Quantity == 0) continue;

                    var orderDetail = new SalesOrderDetail();
                    orderDetail.MapFrom(detail);
                    orderDetail.SalesOrderId = order.Id;
                    orderDetail.Sequence = ++sequence;
                    db.Create(orderDetail, userId);
                }

                db.SaveChanges();
                transaction.Commit();

                return res.Id;
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
        }

        public void Delete(Guid? id, Guid? userId)
        {
            var order = db.Get<SalesOrder>(id);

            if (order.Status == SalesOrderStatus.Closed)
                throw new BadRequestException($"Unable to delete Sales Order because it is already {order.Status}.");

            db.Delete<SalesOrderDetail>(x => x.SalesOrderId == id, userId);
            db.Delete<SalesOrder>(id, userId);
            db.SaveChanges();
        }

        public void Open(Guid? id, Guid? userId)
        {
            var order = db.Get<SalesOrder>(id);
            order.Status = SalesOrderStatus.Open;
            db.Update(order, userId);

            db.SaveChanges();
        }

        public void Close(Guid? id, Guid? userId)
        {
            var order = db.Get<SalesOrder>(id);
            order.Status = SalesOrderStatus.Closed;
            db.Update(order, userId);

            db.SaveChanges();
        }

        public void CalculateQuantityDelivered(Guid? id)
        {
            var order = db.Get<SalesOrder>(id);

            var orderDetailIds = db.GetQuery<SalesOrderDetail>()
                .Where(x => x.SalesOrderId == id)
                .Select(x => x.Id)
                .ToList();

            var isCompleted = true;
            foreach (var detailId in orderDetailIds)
            {
                var orderDetail = db.Get<SalesOrderDetail>(detailId);

                var qtyDelivered = db.GetQuery<SalesDeliveryDetail>()
                    .Where(x => x.SalesDelivery.SalesOrderId == id
                            && x.ItemId == orderDetail.ItemId)
                    .Sum(x => x.Quantity * x.Ratio);

                orderDetail.QuantityDelivered = qtyDelivered / orderDetail.Ratio;

                if (orderDetail.Quantity > orderDetail.QuantityDelivered)
                    isCompleted = false;
            }

            if (isCompleted)
                order.Status = SalesOrderStatus.Closed;

            db.SaveChanges();
        }
    }
}
