﻿using Jawara.Helpers;
using Jawara.Models;
using Jawara.Repositories;
using Jawara.Resources;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Jawara.Services
{
    public class BillOfMaterialService
    {
        private readonly DataContext db;

        public BillOfMaterialService(DataContext db)
        {
            this.db = db;
        }

        public BillOfMaterialResource Get(Guid? id)
        {
            var bom = db.Get<BillOfMaterial, BillOfMaterialResource>(id);

            bom.Details = db.GetQuery<BillOfMaterialDetail>()
                .Where(x => x.BillOfMaterialId == id)
                .OrderBy(x => x.Sequence)
                .Project().To<BillOfMaterialDetailResource>()
                .ToList();

            if (bom.Details == null)
                bom.Details = new List<BillOfMaterialDetailResource>();

            return bom;
        }

        public PagingResult<BillOfMaterialResource> GetList(string keyword, int page, int size)
        {
            var query = db.GetQuery<BillOfMaterial>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.Code.Contains(keyword) || x.Description.Contains(keyword));

            var result = query.OrderBy(x => x.Parent.Code)
                .PaginateTo<BillOfMaterial, BillOfMaterialResource>(page, size);

            return result;
        }

        public LookupResource GetSingleLookup(Guid? id)
        {
            var result = db.GetQuery<BillOfMaterial>()
                .Where(x => x.Id == id)
                .Select(x => new LookupResource
                {
                    Id = x.Id,
                    Code = x.Code + " ─ " + x.Description
                }).FirstOrDefault();

            return result;
        }

        public List<LookupResource> GetLookup(string keyword)
        {
            if (keyword.IsEmpty() || keyword.Length < 2)
                return new List<LookupResource>();

            var query = db.GetQuery<BillOfMaterial>();

            if (keyword.IsNotEmpty())
                query = query.Where(x => x.Code == keyword
                    || x.Description.Contains(keyword));

            var result = query.OrderBy(x => x.Code)
                .Select(x => new LookupResource
                {
                    Id = x.Id,
                    Code = x.Code + " ─ " + x.Description
                }).ToList();

            return result;
        }

        public void ValidateResource(BillOfMaterialResource res)
        {
            if (res.Code.IsEmpty())
                throw new BadRequestException("Code cannot be empty.");

            if (res.Description.IsEmpty())
                throw new BadRequestException("Code cannot be empty.");

            if (res.ParentId.IsGuidEmpty())
                throw new BadRequestException("Parent cannot be empty.");

            if (!res.Details.Any())
                throw new BadRequestException("Component cannot be empty.");
        }

        public Guid Create(BillOfMaterialResource res, Guid? userId)
        {
            ValidateResource(res);

            var bom = new BillOfMaterial();
            bom.MapFrom(res);
            db.Create(bom, userId);

            var sequence = 0;
            foreach (var detail in res.Details)
            {
                var bomDetail = new BillOfMaterialDetail();
                bomDetail.MapFrom(detail);
                bomDetail.BillOfMaterialId = bom.Id;
                bomDetail.Sequence = ++sequence;
                db.Create(bomDetail, userId);
            }

            db.SaveChanges();

            return bom.Id;
        }

        public Guid Update(BillOfMaterialResource res, Guid? userId)
        {
            ValidateResource(res);

            db.Remove<BillOfMaterialDetail>(x => x.BillOfMaterialId == res.Id);

            var bom = db.Get<BillOfMaterial>(res.Id);
            bom.MapFrom(res);
            db.Update(bom, userId);

            var sequence = 0;
            foreach (var detail in res.Details)
            {
                var bomDetail = new BillOfMaterialDetail();
                bomDetail.MapFrom(detail);
                bomDetail.BillOfMaterialId = bom.Id;
                bomDetail.Sequence = ++sequence;
                db.Create(bomDetail, userId);
            }

            db.SaveChanges();

            return bom.Id;
        }

        public void Delete(Guid? id, Guid? userId)
        {
            db.Delete<BillOfMaterial>(id, userId);
            db.Delete<BillOfMaterialDetail>(x => x.BillOfMaterialId == id, userId);
            db.SaveChanges();
        }
    }
}
